# Code-Clan Frontend

This package builds the frontend assets for project Code-Clan

## Usage:

###Setup

`npm install`

###Develop

`npm start`

###Test

`npm test`

###Lint

`npm run lint`

###Build

`npm run dev`

###Publish

`npm run prod`
