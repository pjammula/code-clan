var path = require('path');
var webpackConfig = require('./tools/webpack/webpack.config.test');

module.exports = function (config) {
  var files = [
    'node_modules/jquery/dist/jquery.min.js',
    'node_modules/jasmine-jquery/lib/jasmine-jquery.js',
    'tools/karma/karma.flight.js',
    'tools/karma/modernizr.js',
    'src/scripts/**/*-test.js',
    'src/scripts/**/**/*.html',
    { pattern: 'src/scripts/**/**/*.json', watched: true, served: true, included: false },
  ];
  if (config.grep) {
    files.splice(files.indexOf('src/scripts/**/*-test.js'), 1);
    files.push('src/scripts/**/{{component}}/**/*-test.js'.replace(/{{component}}/, config.grep));
  }
  config.set({
    browsers: ['ChromeHeadless'],
    browserDisconnectTolerance: 3,
    browserDisconnectTimeout : 200000,
    browserNoActivityTimeout : 200000,
    captureTimeout: 200000,
    client: {
      clearContext: false
    },
    colors: true,
    coverageIstanbulReporter: {
      reports: [ 'text-summary' ],
      fixWebpackSourcePaths: true
    },
    coverageReporter: {
      dir: 'reports/coverage',
      reporters: [
        { type: 'html', subdir: 'html' },
        { type: 'lcovonly', subdir: '.' },
      ],
    },
    exclude: [
    ],
    failOnEmptyTestSuite: true,
    files: files,
    frameworks: [
      'jasmine',
      'es6-shim',
    ],
    logLevel: config.LOG_ERROR,
    preprocessors: {
      'tools/karma/karma.flight.js': ['webpack'],
      'src/scripts/**/*-test.js': ['webpack', 'sourcemap'],
    },
    reporters: ['progress', 'coverage', 'verbose'],
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true,
      quiet: true,
    },
  });
};
