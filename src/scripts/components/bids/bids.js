import { component } from 'flight';
import apiFetch from '../../globals/fetch/fetch';

export default component(function Bids() {
	this.attributes({
		//selectors
		root: '.js-bids',
		initiativeRadio: '.js-bids-radio'
	});

	this.broadcastEvent = function (e, data) {
		this.trigger(document, 'uiUpdatesFilter', { 'key': 'selectedType', 'value': data.el.value} );
		//console.log(data.el.value);
	};

	this.after('initialize', function () {
	    this.on('change', {
	    	initiativeRadio: this.broadcastEvent,
	    });
	});
});