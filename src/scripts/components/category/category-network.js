import { component } from 'flight';
import formatEndpoint from '../../globals/fetch/format-endpoint';
import apiFetch from 'globals/fetch/fetch';

export default component(function Network() {
  this.attributes({
    parent: null,

    interpolate: /\{\%([\s\S]+?)\%\}/g,
    url() {
      return this.node.getAttribute('data-categories-url');
    },
  });

  

  /**
   * Populates placehodlers in endpoint url
   * @param  {string} rawUrl - preprocessed endpoint url
   * @return {string} formatted endpoint url
   */
  this.formatEndPoint = function (rawUrl, data) {
    return rawUrl;
    // return formatEndpoint(rawUrl, {
    //   term: data.searchText,
    // }, this.attr.interpolate);
  };

  /**
   * Sends server response to search nav UI components
   * @param  {object} response
   * @return {void}
   */
  this.sendResponse = function (response) {
    if (!response.success) {
      this.$node.closest(this.attr.parent).trigger('networkResponseFailure');
    } else {
      this.$node.closest(this.attr.parent).trigger('networkResponseSuccess', { data : response.categories });
    }
  };

  /**
   * Makes GET request to server in response to search UI events
   * @param  {event} e    [description]
   * @param  {object} data - form data
   * @return {void}
   */
  this.getCategories = function () {
    // let smdata = [
    //   'RFP',
    //   'FSS',
    //   'POC',
    //   'Recruitment',
    //   'Lorem Ipsum',
    // ];


    // setTimeout(() => {
    //   this.sendResponse({ data : smdata});
    // }, 200);

    apiFetch(this.attr.url, {
      method: 'get',
      //credentials: 'include',
    }).then(response => this.sendResponse(response));
  };

  this.after('initialize', function () {

    this.getCategories();
  });
});
