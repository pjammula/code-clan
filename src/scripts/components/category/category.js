import { component } from 'flight';

import morphdom from 'morphdom';
import withChildComponents from 'flight-with-child-components';
import withTemplate from 'flight-handlebars-view';
import withTemplateHelpers from 'mixins/withTemplateHelpers';
import filter from 'lodash/filter';
import map from 'lodash/map';

import Network from './category-network';


export default component(function Category() {
  this.attributes({
    // selectors
    root: '.js-category',
    categoryContainer: '.js-category-container',
    categoryItem: '.js-category-item',
    categoryList: '.js-category-list',
    
    // children
    network: '.js-category-network',
    // values
  });

  this.renderCategories = function (e, response) {
    const categoryMarkup = this.render('categoryTemplate', response.data);
    this.select('categoryContainer').html(categoryMarkup.trim());
  };

  this.attachChildren = function () {
    this.attachChild(Network, this.select('network'), {
      parent: this.attr.root,
    });
  };

  this.onCategoryClick = function (e, data) {
    const selectedCategories = filter(this.select('categoryItem'), (item) => item.checked)
    .map(item => item.value);
    this.broadcastCategoriesUpdate(selectedCategories);
  };

  this.broadcastCategoriesUpdate = function (items) {
    this.trigger(document, 'uiUpdatesFilter', { 'key': 'category', 'value': items} );
    // this.trigger('categoriesFilterChange', {
    //   data: items,
    // });
  }

this.after('initialize', function () {
    this.templates({
      categoryTemplate: '#js-category-template',
    });
    this.on('click', {
      'categoryList': this.onCategoryClick,
    });
    this.on(document, 'uiAttachedComponents', this.attachChildren);
    this.on(this.$node, 'networkResponseSuccess', this.renderCategories);
  });
}, withChildComponents, withTemplate, withTemplateHelpers);