import { component } from 'flight';

//Components
import Header from './header/header';
import Footer from './footer/footer';
import Initiative from './initiative/initiative';
import SkillSearch from './skill-search/skill-search';
import Category from './category/category';
import Login from './user/login/login';
import PostInitiative from './post-initiative/post-initiative';
import Bids from './bids/bids';
import Logout from './user/logout/logout';

const Components = component(function Components() {
  /**
   * Attaches components to App
   * @return {void}
   */
  this.attachComponents = function () {

    Header.attachTo('.js-header');
    Footer.attachTo('.js-footer');
    Initiative.attachTo('.js-initiative');
    SkillSearch.attachTo('.js-skill-search');
    Category.attachTo('.js-category');
    Login.attachTo('.js-login');
    PostInitiative.attachTo('.js-post-initiative');
    Bids.attachTo('.js-bids');
    Logout.attachTo('.js-logout');

    this.trigger(this.$node.closest(document), 'uiAttachedComponents');
  };

  this.after('initialize', function () {
    this.attachComponents();
  });
});

export default Components;
