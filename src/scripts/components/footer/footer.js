import { component } from 'flight';
import morphdom from 'morphdom';
import withTemplate from 'flight-handlebars-view';
import withTemplateHelpers from 'mixins/withTemplateHelpers';

export default component(function Footer() {
	this.attributes({
		//selectors
		root: '.js-footer',
		footerContainer: '.js-footer-container',
	});

	this.initComponent = function (e) {
		const data = {title: "My New Post", body: "This is my first post!"};
    	const footer = this.render('footerTemplate', data);
		morphdom(this.select('footerContainer').get(0), footer.trim());
	};

	this.after('initialize', function () {
		this.addHelpers();
	    this.templates({
	      footerTemplate: '#js-footer-template',
	    });

	    this.on(document, 'uiAttachedComponents', this.initComponent);
	});
}, withTemplate, withTemplateHelpers);