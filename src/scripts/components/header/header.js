import { component } from 'flight';
import morphdom from 'morphdom';
import withTemplate from 'flight-handlebars-view';
import withTemplateHelpers from 'mixins/withTemplateHelpers';

export default component(function Header() {
	this.attributes({
		//selectors
		root: '.js-header',
		headerContainer: '.js-header-container'
	});

	this.initComponent = function (e) {
		const data = {title: "My New Post", body: "This is my first post!"};
    	const header = this.render('headerTemplate', data);
		morphdom(this.select('headerContainer').get(0), header.trim());
	};

	this.after('initialize', function () {
		this.addHelpers();
	    this.templates({
	      headerTemplate: '#js-header-template',
	    });

    	this.on(document, 'uiAttachedComponents', this.initComponent);
	});
}, withTemplate, withTemplateHelpers);