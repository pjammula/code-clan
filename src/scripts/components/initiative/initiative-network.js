import { component } from 'flight';
import withResources from 'flight-with-resources';

// mixins
import withStorage from '../../mixins/withStorage';
import withHistory from '../../mixins/withHistory';
// globals
import apiFetch from '../../globals/fetch/fetch';
import formatEndpoint from '../../globals/fetch/format-endpoint';

export default component(function InitiativeNetwork() {
	this.attributes({
		parent: null,
		initiateUrl: {
			'My Active Initiatives': '//codeclanapp.herokuapp.com/getActiveBiddedQuestions',
			'My Completed Initiatives': '//codeclanapp.herokuapp.com/getCompletedQuestions',
			'All Initiatives': '//codeclanapp.herokuapp.com/postQuestionsToFrontEnd',
		},
		formUrl: '',

		filters : {
			'selectedType': 'My Active Initiatives',
			'skills' : [],
			'category': [],
		},
	});

	this.initFilters = function (e, data) {
		this.updateFilters(e, { 'key': 'skills', 'value' : []});
	}

	this.updateFilters = function (e, data) {
		this.attr.filters[data.key] = JSON.parse(JSON.stringify(data.value));

		this.attr.filters.url = this.attr.initiateUrl[this.attr.filters['selectedType']];
		this.getInitiativeData();
	}

	this.getInitiativeData = function () {
		console.log(this.attr.filters);
		const data = this.attr.filters;
	    apiFetch(data.url, {
	      method: 'POST',
	      credentials: 'include',
	    }, JSON.stringify(data)).then(response => this.sendResponse(response));
	};

	this.getEndpoint = function () {
		return formatEndpoint(this.attr.initiativeUrl, this.attr.interpolate);
	};

	this.sendResponse = function (response) {
		if (!response.success) {
		  this.trigger('networkFailsInitiativeData');
		} else {
		  this.trigger('networkReturnsInitiativeData', response.data);
		}
	};

	this.sendFormData = function (e, data) {
	   /* apiFetch(this.getEndpoint(this.attr.formUrl), {
	      method: 'POST',
	      credentials: 'include',
	    }, JSON.stringify(data));*/
	};

	this.after('initialize', function () {
		this.on(document, 'uiRequestsFormSubmission', this.sendFormData);
		this.on(document, 'uiUpdatesFilter', this.updateFilters);
		this.on(document, 'uiAttachedComponents', this.initFilters);
		this.on(document, 'networkReturnsPostInitiativeSuccess', this.getInitiativeData);
	});
}, withResources, withHistory, withStorage);