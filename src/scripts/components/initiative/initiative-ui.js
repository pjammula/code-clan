import { component } from 'flight';
import morphdom from 'morphdom';
import withTemplate from 'flight-handlebars-view';
import withTemplateHelpers from 'mixins/withTemplateHelpers';
import withForm from 'mixins/withForm';
import withChildComponents from 'flight-with-child-components';
import withState from 'flight-with-state';

export default component(withState, function InitiativeUi() {
	this.attributes({
		//selectors
		parent: null,
		initiativeContainer: '.js-initiative-container',
		bidOverlayContainer: '.js-initiative-bid-overlay-container',
		openBid: '.js-initiative-bid',
		bidOverlay: '.js-initiative-bid-overlay',
		bidOverlayForm: '.js-initiative-bid-overlay-form',
		bidOverlayClose: '.js-initiative-bid-overlay-close',
		bidOverlaySubmit: '.js-initiative-bid-overlay-submit',
		bidOverlayAdd: '.js-initiative-bid-overlay-add',
		bidOverlayRemove: '.js-initiative-bid-overlay-remove',
		bidOverlayFormFields: '.js-initiative-bid-overlay-form-fields',
		bidOverlayBody: '.js-initiative-bid-overlay-body',
		bidOverlayReset: '.js-initiative-bid-overlay-reset',
		bidOverlayFormControl: '.js-form-control',
		initiativeContainer: '.js-initiative-container',
		resultsDiv: '.js-initiative-item',
		data: '',
	});

	this.renderInitiatives = function (e, data) {
		// this.attr.data = {
		// 	items: [
		// 		{title: "Initiative One", description: "RFP", skills: "Front End", daysLeft: 10, bidsCount: 5},
		// 		{title: "Initiative Two", description: "Interviews", skills: "Flight", daysLeft: 4, bidsCount: 2},
		// 		{title: "Initiative Three", description: "Megathon", skills: "Node", daysLeft: 18, bidsCount: 1}
		// 	]
		// };
		// data = this.attr.data;

    	const initiative = this.render('initiativeTemplate', data);
    	this.select('initiativeContainer').html(initiative.trim());
    	const bidTemplate = this.render('bidOverlayTemplate');
		morphdom(this.select('bidOverlayContainer').get(0), bidTemplate.trim());
		var size = 5, k = 0;
		this.renderResults(this.attr.data, k, size);
	};

	this.addMembers = function () {
    	const bidMemberTemplate = this.render('bidOverlayMemberTemplate');
    	this.select('bidOverlayBody').append(bidMemberTemplate.trim());
    	if(this.select('bidOverlayFormFields').length > 1) {
    		this.select('bidOverlayRemove').show();
    	}
    	this.select('bidOverlayForm').parsley().destroy();
		this.select('bidOverlayForm').parsley(window.ParsleyConfig);
	};

	this.removeMembers = function () {
		this.select('bidOverlayForm').parsley().destroy();
		this.select('bidOverlayForm').parsley(window.ParsleyConfig);
		this.select('bidOverlayFormFields').last().remove();
		if(this.select('bidOverlayFormFields').length === 1) {
			this.select('bidOverlayRemove').hide();
		}
	};

	this.showBidOverlay = function () {
		this.select('bidOverlayRemove').hide();
		this.select('bidOverlay').show();
		if(this.select('bidOverlayFormFields').length === 0) {
			this.addMembers();
		} else if (this.select('bidOverlayFormFields').length > 1) {
			this.select('bidOverlayRemove').show();
		}
	};

	this.hideBidOverlay = function () {
		this.select('bidOverlayReset').trigger('click');
		this.select('bidOverlayFormControl').removeClass('forms-control--filled');
		this.select('bidOverlayForm').parsley().reset();
		this.select('bidOverlay').hide();
	};

	this.handleFormSubmission = function (e) {
		e.preventDefault();
		const form = this.select('bidOverlayForm');
		const formData = this.serializeForm(form);
		this.trigger(document, 'uiRequestsFormSubmission', formData);
	};

	this.isElementInViewport = function(el) {
	    var rect = el.getBoundingClientRect();
	    return rect.bottom > 0 &&
	        rect.right > 0 &&
	        rect.left < (window.innerWidth || document.documentElement.clientWidth) /* or $(window).width() */ &&
	        rect.top < (window.innerHeight || document.documentElement.clientHeight) /* or $(window).height() */;
	};

	this.findEndOfResults = function(e) {
		var el = this.select('initiativeContainer');
		var count = this.select('resultsDiv').length;
		if(this.isElementInViewport(this.select('resultsDiv')[count - 1])) {
			if(this.attr.data.items.length - count > 0) {
				this.renderResults(this.attr.data, count, 5);
			} else {
				this.off(document, 'scroll');
				return false;
			}
		}
	};

	this.renderResults = function(data, ind, size) {
		var data1 = data.items.slice(ind, ind+size), data2 = new Object();
		data2.items = data1;
		const initiative = this.render('initiativeTemplate', data2);
		if(ind == 0) {
			this.select('initiativeContainer').html(initiative.trim());
		} else {
			this.select('initiativeContainer').append(initiative.trim());
		}
    	
	};

	this.after('initialize', function () {
		this.addHelpers();
	    this.templates({
	      initiativeTemplate: '#js-initiative-template',
	      bidOverlayTemplate: '#js-initiative-bid-overlay-template',
	      bidOverlayMemberTemplate: '#js-initiative-bid-overlay-member-template',
	    });

	    this.on('click', {
	    	openBid: this.showBidOverlay,
	    	bidOverlayClose: this.hideBidOverlay,
	    	bidOverlayAdd: this.addMembers,
	    	bidOverlayRemove: this.removeMembers,
	    });

	    this.on('submit', {
	    	bidOverlayForm: this.handleFormSubmission,
	    });

    	this.on(document, 'networkReturnsInitiativeData', this.renderInitiatives);
    	//this.renderInitiatives();
    	//this.on(document, 'scroll', this.findEndOfResults);
	});
}, withTemplate, withTemplateHelpers, withForm, withChildComponents);