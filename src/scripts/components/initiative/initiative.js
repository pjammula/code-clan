import { component } from 'flight';

import withChildComponents from 'flight-with-child-components';

import InitiativeNetwork from './initiative-network';
import InitiativeUi from './initiative-ui';

export default component(function Initiative() {
	this.attributes({
		//selectors
		root: '.js-initiative',
		initiativeUi: '.js-initiative-ui',
		initiativeNetwork: '.js-initiative-network',
	});

	this.after('initialize', function () {
	    this.attachChild(InitiativeUi, this.select('initiativeUi'), {
	      parent: this.attr.root,
	    });
	    this.attachChild(InitiativeNetwork, this.select('initiativeNetwork'), {
	      parent: this.attr.root,
	    });
	});
}, withChildComponents);