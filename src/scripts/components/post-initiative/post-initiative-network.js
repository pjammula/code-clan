import { component } from 'flight';
import withResources from 'flight-with-resources';

// mixins
import withStorage from '../../mixins/withStorage';
import withHistory from '../../mixins/withHistory';
// globals
import apiFetch from '../../globals/fetch/fetch';
import formatEndpoint from '../../globals/fetch/format-endpoint';

export default component(function PostInitiativeNetwork() {
	this.attributes({
		root: '.js-post-initiative-network',
		parent: null,
	});

	this.sendFormData = function (e, data) {
		const formObject = {
			title: data.body.title,
			description: data.body.description,
			skills: data.body.skills.split(','),
			startDate: data.body.startDate,
			endDate: data.body.endDate,
			wbsCode: data.body.endDate,
			resourcesCount: data.body.resourcesCount,
			firmHours: data.body.firmHours,
			category: data.body.category.split(',')
		};
	    apiFetch(data.url, {
	      method: data.method,
	      credentials: 'include',
	    }, JSON.stringify(formObject))
	    .then(response => this.sendResponse(response));
	};

	this.sendResponse = function () {
		this.$node.closest(document).trigger('networkReturnsPostInitiativeSuccess');
		//window.location.href = this.attr.redirectUrl;
	};

	this.after('initialize', function () {
		this.on(this.$node.closest(this.attr.parent), 'uiRequestsFormSubmission', this.sendFormData);
	});
}, withResources, withHistory, withStorage);