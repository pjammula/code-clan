import { component } from 'flight';
import morphdom from 'morphdom';
import withTemplate from 'flight-handlebars-view';
import withTemplateHelpers from 'mixins/withTemplateHelpers';
import withForm from 'mixins/withForm';
import withChildComponents from 'flight-with-child-components';
import withState from 'flight-with-state';

export default component(function PostInitiative() {
	this.attributes({
		//selectors
		root: '.js-post-initiative-ui',
		parent: null,
		postInitiativeCreate: '.js-post-initiative-create',
		postInitiativeOverlay: '.js-post-initiative-overlay',
		postInitiativeOverlayContainer: '.js-post-initiative-overlay-container',
		postInitiativeOverlayForm: '.js-post-initiative-overlay-form',
		postInitiativeClose: '.js-post-initiative-overlay-close',
		postInitiativeReset: '.js-post-initiative-overlay-reset',
		postInitiativeFormControl: '.js-form-control',
	});

	this.initComponent = function () {
    	const initiativeOverlayTemplate = this.render('postInitiativeOverlayTemplate');
		morphdom(this.select('postInitiativeOverlayContainer').get(0), initiativeOverlayTemplate.trim());
		this.select('postInitiativeOverlayForm').parsley(window.ParsleyConfig);
	};

	this.showOverlay = function (e) {
		this.select('postInitiativeOverlay').show();
	};

	this.hideOverlay = function (e) {
		this.select('postInitiativeReset').trigger('click');
		this.select('postInitiativeFormControl').removeClass('forms-control--filled');
		this.select('postInitiativeOverlayForm').parsley().reset();
		this.select('postInitiativeOverlay').hide();
	};

	this.handleFormSubmission = function (e) {
		e.preventDefault();
		const form = this.select('postInitiativeOverlayForm');
		const formData = {};
	    formData.url = this.getFormAction(form);
	    formData.method = this.getFormMethod(form);
	    formData.body = this.serializeForm(form);
		this.$node.closest(this.attr.parent).trigger('uiRequestsFormSubmission', formData);
	};

	this.after('initialize', function () {
		this.addHelpers();
	    this.templates({
	      postInitiativeOverlayTemplate: '#js-post-initiative-overlay-template',
	    });

	    this.on('click', {
	    	postInitiativeCreate: this.showOverlay,
	    	postInitiativeClose: this.hideOverlay,
	    });

	    this.on('submit', {
	    	postInitiativeOverlayForm: this.handleFormSubmission,
	    });
		this.on(document, 'uiAttachedComponents', this.initComponent);
		this.on(this.$node.closest(document), 'networkReturnsPostInitiativeSuccess', this.hideOverlay);
	});
}, withTemplate, withTemplateHelpers, withForm, withChildComponents);