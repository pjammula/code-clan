import { component } from 'flight';

import withChildComponents from 'flight-with-child-components';

import PostInitiativeNetwork from './post-initiative-network';
import PostInitiativeUi from './post-initiative-ui';

export default component(function PostInitiative() {
	this.attributes({
		//selectors
		root: '.js-post-initiative',
		postInitiativeUi: '.js-post-initiative-ui',
		postInitiativeNetwork: '.js-post-initiative-network',
	});

	this.after('initialize', function () {
	    this.attachChild(PostInitiativeUi, this.select('postInitiativeUi'), {
	      parent: this.attr.root,
	    });
	    this.attachChild(PostInitiativeNetwork, this.select('postInitiativeNetwork'), {
	      parent: this.attr.root,
	    });
	});
}, withChildComponents);