import { component } from 'flight';
import 'isomorphic-fetch';
import formatEndpoint from '../../globals/fetch/format-endpoint';
import apiFetch from 'globals/fetch/fetch';
import logger from 'globals/logger/logger';
import debounce from 'lodash/debounce';

export default component(function Network() {
  this.attributes({
    parent: null,
    delay: 100,

    interpolate: /\{\%([\s\S]+?)\%\}/g,
    url() {
      return this.node.getAttribute('data-suggestions-url');
    },
    cancelablePromise: '',
  });

  const makeCancelable = (promise) => {
    let hasCanceled_ = false;

    const wrappedPromise = new Promise((resolve, reject) => {
      promise.then((val) =>
        hasCanceled_ ? reject({ isCanceled: true }) : resolve(val)
      );
      promise.catch((error) =>
        hasCanceled_ ? reject({ isCanceled: true }) : reject(error)
      );
    });

    return {
      promise: wrappedPromise,
      cancel() {
        hasCanceled_ = true;
      },
    };
  };

  /**
   * Populates placehodlers in endpoint url
   * @param  {string} rawUrl - preprocessed endpoint url
   * @return {string} formatted endpoint url
   */
  this.formatSuggEndPoint = function (rawUrl, data) {
    return formatEndpoint(rawUrl, {
      term: data.searchText,
    }, this.attr.interpolate);
  };

  /**
   * Sends server response to search nav UI components
   * @param  {object} response
   * @return {void}
   */
  this.sendResponse = function (response) {
    if (!response.success) {
      this.$node.closest(this.attr.parent).trigger('networkResponseFailure');
    } else {
      this.$node.closest(this.attr.parent).trigger('networkResponseSuccess', response);
    }
  };

  /**
   * Makes GET request to server in response to search UI events
   * @param  {event} e    [description]
   * @param  {object} data - form data
   * @return {void}
   */
  this.getSearchSuggestions = function (data) {
    let endPoint = this.formatSuggEndPoint(this.attr.url, data);

    if (this.attr.cancelablePromise) {
      this.attr.cancelablePromise.cancel();
    }
    this.attr.cancelablePromise = makeCancelable(fetch(endPoint, {
      method: 'get',
      //credentials: 'include',
    }).then(function (response) {
      return response.json();
    }));

    this.attr.cancelablePromise
      .promise
      .then((response) => this.sendResponse(response))
      .catch((reason) => logger.log('isCanceled: ' + reason.isCanceled));
  };

  this.after('initialize', function () {
    this.on(this.$node.closest(this.attr.parent), 'uiRequestsSuggestions', debounce(function (e, data) {
      this.getSearchSuggestions(data);
    }, this.attr.delay));
  });
});
