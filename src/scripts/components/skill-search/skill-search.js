import { component } from 'flight';

import morphdom from 'morphdom';
import withChildComponents from 'flight-with-child-components';
import withState from 'flight-with-state';
import withTemplate from 'flight-handlebars-view';
import withTemplateHelpers from 'mixins/withTemplateHelpers';

import Network from './skill-search-network';
import filter from 'lodash/filter';
import debounce from 'lodash/debounce';

export default component(withState, function SkillSearch() {
  this.attributes({
    // selectors
    root: '.js-skill-search',
    skillsContainer: '.js-skills-container',
    skill: '.js-skill-select',
    searchForm: '.js-search-form',
    searchInput: '.js-search-input',
    skillCta: '.js-skill-btn',
    // children
    network: '.js-skill-search-network',
    suggestionsContainer: '.js-suggestions-container',
    // values
    delay: 200,
    perfectMatch: '',
  });

  this.initialState({
    skills: [],
    isDisabled: true,
    searchInputFilled: false,
  });

  this.handleComponentMount = function () {
    this.attachChildren();
    this.trigger('uiRequestsProductData', {
      codes: this.getProductCodes(),
    });
    this.mergeState({
      mode: this.getSelectedProductCodes().length ? 'contrast' : 'select',
    });
  };

  /**
   * Activates search on component mount
   * @return {void}
   */
  this.activateSearch = function () {
    this.select('searchInput').removeAttr('disabled');
  };

  /**
   * Updates input field ui in response to state hanve
   * @return {[type]} [description]
   */
  this.updateInputUi = function () {
    this.select('searchForm').toggleClass('filled', !this.state.searchInputFilled);
  };

  /**
   * Gets search api url
   * @return {string} search api url
   */
  this.getUrl = function () {
    const path = this.select('searchForm').attr('action');
    const parameters = this.select('searchInput').attr('name');
    const value = encodeURI(this.select('searchInput').val());
    const originParam = this.$node.data('origin') ? `&from=${this.$node.data('origin')}` : '';
    return `${path}?${parameters}=${value}${originParam}`;
  };

  this.handleStateChange = function (current, previous) {
    if (current.searchInputFilled !== previous.searchInputFilled) {
      this.updateInputUi();
    } else if (current.isDisabled !== previous.isDisabled) {
      this.activateSearch();
    }

    if (current.skills.length !== previous.skills.length) {
      this.renderSkills();
      this.broadcastSkillsUpdate();
    }
  };

  this.broadcastSkillsUpdate = function () {
    this.trigger(document, 'uiUpdatesFilter', { 'key': 'skills', 'value': this.state.skills} );
    // this.trigger('skillsFilterChange', {
    //   data: this.state.skills,
    // });
  }

  this.renderSkills = function () {
    const skillsMarkup = this.render('skillListTemplate', this.state.skills);
    //this.trigger('uiRemoveContrastItem');
    this.select('skillsContainer').html(skillsMarkup.trim());
    //morphdom(this.select('skillsContainer').get(0), skillsMarkup.trim());
    //svgUseIt();
    //this.trigger('uiCreateContrastItem');
    //this.equalizeItems();
  };

  this.attachChildren = function () {
    // this.mergeState({
    //   skills: ['JavaScript', 'CSS', 'Bootstrap'],
    // });
    this.attachChild(Network, this.select('network'), {
      parent: this.attr.root,
    });
  };

  this.removeSkill = function (e, data) {
    this.mergeState({
        skills: filter(this.state.skills, (skill) => skill !== data.el.innerHTML),
    });
  };

  this.selectSkill = function () {
    let copySkills = JSON.parse(JSON.stringify(this.state.skills));
    if (copySkills.indexOf(this.attr.perfectMatch) == -1) {
      copySkills.push(this.attr.perfectMatch);
      this.mergeState({
        skills: copySkills,
      });
    }
    this.clearPerfectMatch();
    this.select('searchInput').val('');
    this.clearSearchSuggestions();
  };

  /**
   * Sends request with delay to avoid multiple calls
   * @retrn {void}
   */
  this.initAutocomplete = function () {
    this.trigger('uiRequestsSuggestions', {
      searchText: this.select('searchInput').val(),
    });
  };

  /**
   * Update state of input field
   * @param  {boolean} value - filled state of input (true: filled, false: empty)
   * @return {void}
   */
  this.updateInputState = function (value) {
    this.mergeState({
      searchInputFilled: value,
    });
  };

  /**
   * Handles change of search input
   * @return {void}
   */
  this.handleInputChange = function () {
    if (this.select('searchInput').val() !== '' &&  this.select('searchInput').val().length >= 2) {
      this.updateInputState(false);
      this.initAutocomplete();
    } else {
      this.updateInputState(true);
      this.clearSearchSuggestions();
    }
  };

  this.handleComponentMount = function () {
    this.mergeState({
      isDisabled: false,
    });
  };

  /**
   * Populate search suggestion
   * @param  {array} response - search results
   * @return {void}
   */
  this.populateSearchSuggestions = function (e, response) {
    if (this.select('searchInput').val() !== '') {
      if (response.skills && response.skills.length == 1) {
        this.attr.perfectMatch = response.skills[0];
        this.select('searchInput').val(this.attr.perfectMatch);
        this.clearSearchSuggestions();
      } else {
        this.clearPerfectMatch();
        this.select('suggestionsContainer').html(this.render('suggestionsTemplate', response.skills).trim());
      }
    } else {
      this.clearPerfectMatch();
      this.clearSearchSuggestions();
    }
  };

  this.clearPerfectMatch = function () {
    this.attr.perfectMatch = '';
  };

  /**
   * Clears search suggestions list
   * @return {void}
   */
  this.clearSearchSuggestions = function () {
    this.select('suggestionsContainer').html('');
  };

  this.onFormSubmit = function (e) {
    e.preventDefault();
    if (this.attr.perfectMatch !== '') {
      this.selectSkill();
    }
  };

  this.onSkillBtnClick = function (e, data) {
    e.preventDefault();
    this.attr.perfectMatch = data.el.innerHTML;
    this.selectSkill();
  }

  this.after('initialize', function () {
    this.templates({
      skillListTemplate: '#js-skills-list-template',
      suggestionsTemplate: '#js-search-suggestions-template',
    });
    this.after('stateChanged', this.handleStateChange);
    this.on('input', {
      searchInput: debounce((e, data) => this.handleInputChange(e, data), this.attr.delay),
    });
    this.on('click', {
      'skill': this.removeSkill,
      'skillCta': this.onSkillBtnClick,
    });
    this.on('submit', {
      'searchForm': this.onFormSubmit,
    });
    this.on(document, 'uiAttachedComponents', this.attachChildren);
    this.on(document, 'uiAttachedComponents', this.handleComponentMount);
    this.on(this.$node, 'networkResponseSuccess', this.populateSearchSuggestions);
    this.on(this.$node, 'networkResponseFailure', this.clearSearchSuggestions);
  });
}, withChildComponents, withTemplate, withTemplateHelpers);
