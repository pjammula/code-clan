import Login from '../login';
import * as ScrollTo from 'globals/animations/scroll/scrollTo';
jasmine.getFixtures().fixturesPath = '/base/src/scripts/components/user/login/__tests__/fixtures/';

describeComponent(Login, function () {
  const fixture = readFixtures('LOGIN-MAIN.html');

  beforeEach(function () {
    this.setupComponent( fixture, {
      attachTo: '.js-login',
      parent: '.js-login-parent',
    },
      [{
        name: 'userData',
        data: {
          loggedIn: null,
        },
      }]);
  });

  afterEach(function() {
    this.component.removeResource('userData');
  });

  describe('Components', function () {
    describe('Login Component', function () {
      describe('Functionality tests', function () {

        describe('on receiving networkReturnsLoginResponse event from parent', function () {
          beforeEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .on('networkReturnsLoginResponse',
              this.component.activateButton.bind(this.component));
            this.component.select('button').attr('disabled', 'disabled');
          });
          afterEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .off('networkReturnsLoginResponse',
              this.component.activateButton.bind(this.component));
            this.component.select('button').removeAttr('disabled');
          });

          it('activates login button', function () {
            const methodSpy1 = spyOn(this.component, 'updateSpinnerState');
            const methodSpy2 = spyOn(this.component, 'hideSpinner');
            this.component.$node.closest(this.component.attr.parent).trigger('networkReturnsLoginResponse');
            expect(methodSpy1).toHaveBeenCalledWith(false);
            expect(methodSpy2).toHaveBeenCalledWith(this.component.select('buttonLoader'), 'button__loader');
            expect(this.component.select('button')).not.toBeDisabled();
          });
        });

        describe('on receiving networkFailsLoginRequest event from parent', function () {
          beforeEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .on('networkFailsLoginRequest',
              this.component.updateErrorState.bind(this.component));
          });
          afterEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .off('networkFailsLoginRequest',
              this.component.updateErrorState.bind(this.component));
          });

          it('updates error state with payload value', function () {
            const methodSpy = spyOn(this.component, 'mergeState');
            this.component.$node.closest(this.component.attr.parent).trigger('networkFailsLoginRequest', {
              status: 'error',
            });
            expect(methodSpy).toHaveBeenCalledWith({
              hasError: true,
            });
          });
        });

        describe('on receiving uiRequestsLoginView event from parent', function () {
          beforeEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .on('uiRequestsLoginView',
              this.component.updateVisibilityState.bind(this.component));
          });
          afterEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .off('uiRequestsLoginView',
              this.component.updateVisibilityState.bind(this.component));
          });

          it('updates visibility state with payload value', function () {
            const methodSpy = spyOn(this.component, 'mergeState');
            this.component.$node.closest(this.component.attr.parent).trigger('uiRequestsLoginView', {
              loginVisible: true,
            });
            expect(methodSpy).toHaveBeenCalledWith({
              isVisible: true,
            });
          });
        });

        describe('on receiving networkReturnsPasswordResetResponse event from parent', function () {
          beforeEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .on('networkReturnsPasswordResetResponse',
              this.component.updateVisibilityState.bind(this.component));
          });
          afterEach(function() {
            this.component.$node.closest(this.component.attr.parent)
              .off('networkReturnsPasswordResetResponse',
              this.component.updateVisibilityState.bind(this.component));
          });

          it('updates visibility state with payload value', function () {
            const methodSpy = spyOn(this.component, 'mergeState');
            this.component.$node.closest(this.component.attr.parent).trigger('networkReturnsPasswordResetResponse', {
              loginVisible: true,
            });
            expect(methodSpy).toHaveBeenCalledWith({
              isVisible: true,
            });
          });
        });

        describe('on submitting login form', function () {
          it('broadcasts event on parent to request form submission to network', function () {
            const methodSpy1 = spyOn(this.component, 'deactivateButton');
            const methodSpy2 = spyOn(this.component, 'updateErrorState');
            const eventSpy = spyOnEvent(this.component.attr.parent, 'uiRequestsLogin');

            spyOn(this.component, 'getFormAction').and.returnValue('/url');
            spyOn(this.component, 'serializeForm').and.returnValue({email: 'hello@example.com'});
            this.component.select('form').trigger('submit');
            expect(methodSpy1).toHaveBeenCalled();
            expect(methodSpy2).toHaveBeenCalledWith(null, {
              status: 'success',
            });
            expect(eventSpy).toHaveBeenTriggeredOnAndWith(this.component.attr.parent, {
              url: '/url',
              body: {email: 'hello@example.com'},
            });
          });
        });

      });

      describe('Unit tests', function () {
        describe('on initialize', function () {
          it('will exist', function () {
            expect(this.component).toBeDefined();
          });
        });

        describe('checkUnsubscribeNotification', function () {
          it('broadcasts event to request unsubscribe notification view', function () {
            const eventSpy = spyOnEvent(document, 'uiShowUnsubscribeNotification');
            spyOn(this.component, 'getUrlParameter').and.returnValue('unsubscribe');
            this.component.checkUnsubscribeNotification();
            expect(eventSpy).toHaveBeenTriggeredOn(document);
          });

          it('does not broadcasts event to request unsubscribe notification view', function () {
            const eventSpy = spyOnEvent(document, 'uiShowUnsubscribeNotification');
            spyOn(this.component, 'getUrlParameter').and.returnValue(undefined);
            this.component.checkUnsubscribeNotification();
            expect(eventSpy).not.toHaveBeenTriggeredOn(document);
          });
        });

        describe('handleStateChange', function() {
          it('calls downstream methods in reponse to state changes', function () {
            const methodSpy1 = spyOn(this.component, 'updateErrorUi');
            const methodSpy2 = spyOn(this.component, 'updateVisibilityUi');
            this.component.mergeState({
              hasError: true,
            });
            expect(methodSpy1).toHaveBeenCalled();

            this.component.mergeState({
              isVisible: true,
            });
            expect(methodSpy2).toHaveBeenCalled();
          });
        });

        describe('updateErrorUi', function () {
          beforeEach(function () {
            this.component.state.hasError = true;
          });
          afterEach(function () {
            this.component.state.hasError = null;
          });
          it('adds error message and broadcasts event to request error view', function () {
            const methodSpy1 = spyOn(this.component, 'activateButton');
            const methodSpy2 = spyOn(this.component, 'scrollToLoginFormAnimate');
            const eventSpy = spyOnEvent(document, 'uiTogglesLoginErrorMessage');
            this.component.updateErrorUi();

            expect(methodSpy1).toHaveBeenCalled();
            expect(methodSpy2).toHaveBeenCalledWith('submit');
            expect(eventSpy).toHaveBeenTriggeredOnAndWith(document, {
              hasError: true,
            });
            expect(this.component.select('error').text()).toEqual('Error Message');
            expect(this.component.select('error')).not.toHaveClass('hidden-xs-up');
          });
        });

        describe('updateVisibilityUi', function () {
          beforeEach(function () {
            this.component.state.isVisible = false;
          });
          afterEach(function () {
            this.component.state.isVisible = null;
          });
          it('hides component in response to visibility state change', function () {
            const methodSpy1 = spyOn(this.component, 'activateButton');
            this.component.updateVisibilityUi();

            expect(methodSpy1).toHaveBeenCalled();
            expect(this.component.$node).toHaveClass('hidden-xs-up');
          });
        });

        describe('deactivateButton', function () {
          it('deactivates login button', function () {
            const methodSpy1 = spyOn(this.component, 'updateSpinnerState');
            const methodSpy2 = spyOn(this.component, 'renderSpinner');
            this.component.deactivateButton();
            expect(methodSpy1).toHaveBeenCalledWith(true);
            expect(methodSpy2).toHaveBeenCalledWith(this.component.select('buttonLoader'), 'button__loader', 3000);
            expect(this.component.select('button')).toBeDisabled();
          });
        });

        describe('handleFieldValidation', function () {
          it('broadcasts event to request toggling of error message', function () {
            const field = {
              isValid() {
                return false;
              }
            }
            const eventSpy = spyOnEvent(document, 'uiTogglesLoginErrorMessage');
            this.component.handleFieldValidation(field);
            expect(eventSpy).toHaveBeenTriggeredOnAndWith(document, {
              hasError: true
            });
          });
        });

        describe('scrollToLoginFormAnimate', function () {
          it('scrolls to component view', function () {
            const methodSpy = spyOn(ScrollTo, 'scrollTo');
            const data = {};
            spyOn(this.component, 'getScrollPosition').and.returnValue(20);
            this.component.scrollToLoginFormAnimate(data);
            expect(methodSpy).toHaveBeenCalledWith({
              element: this.component.node,
              position: jasmine.any(Number),
              duration: 800,
            });
          });
        });

        describe('getScrollPosition', function () {
          it('return scroll position of component', function () {
            expect(this.component.getScrollPosition()).toEqual(jasmine.any(Number));
            expect(this.component.getScrollPosition('submit')).toEqual(jasmine.any(Number));
          });
        });

      });
    });
  });
});
