import LoginNetwork from '../network';
import fetchMock from 'fetch-mock';
jasmine.getFixtures().fixturesPath = '/base/src/scripts/components/user/login/__tests__/fixtures/';

describeComponent(LoginNetwork, function () {
  const fixture = readFixtures('LOGIN-NETWORK.html');

  beforeEach(function () {
    this.setupComponent(fixture, {
      attachTo: '.js-login-network',
      parent: '.js-login',
      grandparent: '.js-login-parent',
      loggedIn: null,
    }, [{
      name: 'userData',
      data: {
        userId: 'user1234',
      },
    }]);
  });
  afterEach(function () {
    this.component.removeResource('userData');
  });

  describe('Components', function () {
    describe('Login Network Component', function () {
      describe('Functionality tests', function () {
        describe('on receiving uiRequestsLogin event from grandparent', function () {
          beforeEach(function() {
            this.component.$node.closest(this.component.attr.grandparent)
              .on('uiRequestsLogin',
              this.component.loginUser.bind(this.component));
          });
          afterEach(function() {
            this.component.$node.closest(this.component.attr.grandparent)
              .off('uiRequestsLogin',
              this.component.loginUser.bind(this.component));
          });

          it('calls downstream functions and broadcasts event to notify login success', function (done) {
            const methodSpy1 = spyOn(this.component, 'sendUserDetails').and.returnValue(Promise.resolve('success'));
            const methodSpy2 = spyOn(this.component, 'updateUserData').and.returnValue(Promise.resolve('success'));
            const eventSpy = spyOnEvent(this.component.attr.grandparent, 'networkReturnsLoginResponse');

            this.component.$node.closest(this.component.attr.grandparent).trigger('uiRequestsLogin', {
              email: 'hello@example.com'
            });
            expect(methodSpy1).toHaveBeenCalledWith({
              email: 'hello@example.com'
            });
            setTimeout(() => {
              expect(methodSpy2).toHaveBeenCalled();
              expect(eventSpy).toHaveBeenTriggeredOnAndWith(this.component.attr.grandparent, {
                status: 'success',
              });
              done();
            }, 100);
          });

          it('calls downstream functions and broadcasts event to notify login failure', function (done) {
            const methodSpy1 = spyOn(this.component, 'sendUserDetails').and.returnValue(Promise.reject('fail'));
            const eventSpy = spyOnEvent(this.component.attr.grandparent, 'networkFailsLoginRequest');
            this.component.$node.closest(this.component.attr.grandparent).trigger('uiRequestsLogin', {
              email: 'hello@example.com'
            });
            setTimeout(() => {
              expect(eventSpy).toHaveBeenTriggeredOnAndWith(this.component.attr.grandparent, {
                status: 'error',
              });
              done();
            }, 100);
          });
        });
      });

      describe('Unit tests', function () {
        describe('on initialize', function () {
          it('will exist', function () {
            expect(this.component).toBeDefined();
          });
        });

        describe('sendUserDetails', function () {
          it('returns a promise to send user details to network', function () {
            expect(this.component.sendUserDetails({body: {email: 'hello@example.com'}})).toEqual(jasmine.any(Promise));
          });
        });

        describe('updateUserData', function () {
          it('returns a promise to update user details to network', function () {
            expect(this.component.updateUserData()).toEqual(jasmine.any(Promise));
          });
        });
      });
      
    });
  });
});