import { component } from 'flight';
import { scrollTo } from 'globals/animations/scroll/scrollTo';

// children
import Network from './network';

// mixins
import withState from 'flight-with-state';
import withResources from 'flight-with-resources';
import withChildComponents from 'flight-with-child-components';
import withForm from 'mixins/withForm';
import withHistory from 'mixins/withHistory';
import withStorage from 'mixins/withStorage';


export default component(withState, function Login() {
  this.attributes({
    // selectors
    root: '.js-login',
    form: '.js-login-form',
    error: '.js-login-error',
    button: '.js-login-button',
    buttonLoader: '.js-button-loader',
    // children
    network: '.js-login-network',
    // values
    loginErrorMessage() {
      return this.select('form').data('errorMessage');
    },
    isLoggedIn () {
      return !!this.getCookie('jwtKey');
    },
    redirectUrl () {
      return this.select('form').data('redirect-url');
    },
    
    spinnerStyle: 'button__loader',
  });

  this.initialState({
    isVisible: null,
    hasError: null,
  });

  this.processLoginResponse = function (e, data) {
    if (data.status == 'success') {
      this.redirectToDestination();
    } else {
      this.mergeState({
        hasError: true,
      });
    }
  };

  this.redirectToDestination = function () {
    this.changeWindowLocation(window, this.attr.redirectUrl);
  };

  this.activateButton = function () {
    // this.updateSpinnerState(false);
    // this.select('button').removeAttr('disabled');
    // this.hideSpinner(this.select('buttonLoader'), this.attr.spinnerStyle);
  };

  this.deactivateButton = function () {
    // this.updateSpinnerState(true);
    // this.select('button').attr('disabled', 'disabled');
    // this.renderSpinner(this.select('buttonLoader'), this.attr.spinnerStyle, 3000);
  };

  this.updateErrorState = function (e, data) {
    this.mergeState({
      hasError: data.status === 'error',
    });
  };

  this.updateVisibilityState = function (e, data) {
    this.mergeState({
      isVisible: data.loginVisible,
    });
  };

  this.updateErrorUi = function () {
    if (this.state.hasError) {
      this.activateButton();
      this.scrollToLoginFormAnimate('submit');
    }
    this.select('error')
        .text(this.attr.loginErrorMessage)
        .toggleClass('hidden-xs-up', !this.state.hasError);
    this.trigger('uiTogglesLoginErrorMessage', {
      hasError: this.state.hasError,
    });
  };

  this.updateVisibilityUi = function () {
    this.activateButton();
    this.$node.toggleClass('hidden-xs-up', !this.state.isVisible);
    this.node.scrollIntoView();
  };

  this.handleStateChange = function (current, previous) {
    if (current.hasError !== previous.hasError && current.hasError) {
      this.updateErrorUi();
    } else if (current.isVisible !== previous.isVisible) {
      this.updateVisibilityUi();
    }
  };

  this.handleLoginSubmission = function (e) {
    e.preventDefault();
    const form = this.select('form');
    this.deactivateButton();
    this.updateErrorState(null, {
      status: 'success',
    });
    this.$node.closest(document).trigger('uiRequestsLogin', {
      url: this.getFormAction(form),
      body: this.serializeForm(form),
    });
  };

  this.handleFieldValidation = function (field) {
    this.trigger('uiTogglesLoginErrorMessage', {
      hasError: !field.isValid(),
    });
  };

  this.resetForm = function () {
    this.select('form').parsley().reset();
    this.select('form').get(0).reset();
  };

  this.scrollToLoginFormAnimate = function (data) {
    // scrollTo({
    //   element: this.node,
    //   position: this.getScrollPosition(data),
    //   duration: 800,
    // });
  };

  // this.getScrollPosition = function (data) {
  //   if (data === 'submit') {
  //     return this.select('form').offset().top - 60;
  //   } else {
  //     return this.$node.offset().top - 60;
  //   }
  // };

  this.handleLoginClickForScroll = function () {
    if (!this.select('form').parsley().isValid()) {
      this.scrollToLoginFormAnimate();
    }
  };

  this.after('initialize', function () {
    this.attachChild(Network, this.select('network'), {
      parent: this.attr.root,
      loggedIn: this.attr.isLoggedIn,
    });

    this.after('stateChanged', this.handleStateChange);

    this.on('click', {
      button: this.handleLoginClickForScroll,
    });

    this.on('submit', {
      form: this.handleLoginSubmission,
    });

    this.bindFieldValidation = function () {
      this.select('form').parsley().on('field:validate', (field) => {
        this.handleFieldValidation(field);
      });
    };
    this.adjustHeightOfContent = function() {
      this.select('form').css('height', this.$node.closest(document).height() - (this.$node.closest(document).find('.js-header').height() + this.$node.closest(document).find('.js-footer').height()) + 'px');
    };


    this.on(this.$node.closest(document), 'networkReturnsLoginResponse', this.processLoginResponse);
    this.on(this.$node.closest(this.attr.parent), 'networkFailsLoginRequest', this.updateErrorState);
    this.on(this.$node.closest(this.attr.parent), 'uiRequestsLoginView networkReturnsPasswordResetResponse', this.updateVisibilityState);
    this.on(this.$node.closest(this.attr.parent), 'uiRequestsLoginView', this.resetForm);
    this.on(document, 'uiAttachedComponents', this.bindFieldValidation);
    this.on(window, "load", this.adjustHeightOfContent);
  });
}, withResources, withChildComponents, withForm, withHistory, withStorage);
