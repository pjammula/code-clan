import { component } from 'flight';
import apiFetch from '../../../globals/fetch/fetch';
import formatEndpoint from '../../../globals/fetch/format-endpoint';

// mixins
import withStorage from '../../../mixins/withStorage';
import withResources from 'flight-with-resources';

export default component(function Network() {
  this.attributes({
    parent: null,
    loggedIn: null,

    userUrl() {
      return this.node.getAttribute('data-user-url');
    },
    interpolate: /\{\%([\s\S]+?)\%\}/g,
  });

  this.getEndpoint = function (url) {
    return formatEndpoint(url, {
      
    }, this.attr.interpolate);
  };

  this.loginUser = function (e, data) {

    // const smresponse = {
    //   'jwtKey': 'blahblah',
    // };
    //this.updateUserData(smresponse);
    apiFetch(data.url, {
      method: 'POST',
      // credentials: 'include',
    }, JSON.stringify(data.body))
    .then(response => this.updateUserData(response))
    .catch(response => this.$node.closest(this.attr.parent).trigger('networkFailsLoginRequest',
            { status: 'error' }));
  };

  this.updateUserData = function (response) {
    const options = {
      expires: new Date(new Date().getTime() + 10 * 24 * 60 * 60 * 1000),
      path: '/',
    };

    if (response.token) {
      this.setCookie('jwtKey', response.token, options);
      this.$node.closest(document).trigger('networkReturnsLoginResponse',
      { status: 'success' });
    } else {
      this.$node.closest(document).trigger('networkReturnsLoginResponse',
      { status: 'failure' });
    }
  };

  this.after('initialize', function () {
    this.on(this.$node.closest(document), 'uiRequestsLogin', this.loginUser);
  });
}, withStorage, withResources);
