import { component } from 'flight';
import logger from 'globals/logger/logger';

// mixins
import withForm from 'mixins/withForm';
import withHistory from 'mixins/withHistory';
import withChildComponents from 'flight-with-child-components';
import withStorage from 'mixins/withStorage';

// children
import Network from './network';

export default component(function Logout() {
  this.attributes({
    // selectors
    root: '.js-logout',
    form: '.js-logout-form',
    // children
    network: '.js-logout-network',
  });

  this.handleLogoutSubmission = function (e) {
    e.preventDefault();
    const form = this.select('form');
    const formData = {};

    formData.url = this.getFormAction(form);
    formData.method = this.getFormMethod(form);

    this.$node.closest(document).trigger('uiRequestsLogOut', { formData });
  };

  this.handleLogoutSuccess = function () {
    this.changeWindowLocation(window, this.select('form').data('redirectUrl'));
  };

  this.handleLogoutFailure = function (error) {
    logger.error(error.name + ': ' + error.message);
    //this.changeWindowLocation(window, this.select('form').data('redirectUrl'));
  };

  this.after('initialize', function () {
    this.attachChild(Network, this.select('network'), {
      parent: this.attr.root,
    });
    this.on('submit', {
      form: this.handleLogoutSubmission,
    });

    this.on(this.$node.closest(this.attr.parent), 'networkReturnsLogoutSuccess', this.handleLogoutSuccess);
    this.on(this.$node.closest(this.attr.parent), 'networkReturnsLogoutFailure', this.handleLogoutFailure);
  });
}, withChildComponents, withForm, withHistory, withStorage);
