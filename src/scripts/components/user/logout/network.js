import { component } from 'flight';
import apiFetch from 'globals/fetch/fetch';

// mixins
import withStorage from 'mixins/withStorage';

export default component(function Network() {
  this.attributes({
    // selectors
    parent: null,
  });

  this.logoutUser = function (e, data) {
    this.sendLogoutRequest(data.formData)
      .then(() => this.handleLogoutResponse())
      .catch(error => this.sendLogoutFailure(error));
  };

  this.sendLogoutRequest = function (data) {
    const reqObj = {
        jwtToken : this.getCookie('jwtKey'),
    };
    return apiFetch(data.url, {
      method: data.method,
      credentials: 'include',
    }, JSON.stringify(reqObj))
  };

  this.handleLogoutResponse = function () {
      this.removeCookie('jwtKey');
      this.sendLogoutResponse();
  }

  this.sendLogoutResponse = function () {
    this.$node.closest(document).trigger('networkReturnsLogoutSuccess');
  };

  this.sendLogoutFailure = function (error) {
    this.$node.closest(document).trigger('networkReturnsLogoutFailure', { error });
  };

  this.after('initialize', function () {
    this.on(this.$node.closest(document), 'uiRequestsLogOut', this.logoutUser);
  });
}, withStorage);
