import defaults from './defaults';
import domUtils from './dom-utils';
import objectUtils from './object-utils';
import logger from '../logger/logger';

const slice = Array.prototype.slice;

export function accordion(element, options) {

  let settings;
  let accordionSections;
  let oneAtATimeSet = false;

  function init() {
    settings = {...defaults, ...options};
    if (element) {
      accordionSections = slice.call(element.querySelectorAll('.' + settings.accordionSectionClass));
      if (accordionSections.length > 0) {
        wrapSections();
        bindEvents();
      }
    } else {
      logger.error('No accordion element set.');
    }
  }

  function bindEvents() {
    element.addEventListener('click', onDocumentClick);
  }

  function unbindEvents() {
    element.removeEventListener('click', onDocumentClick);
  }

  function onDocumentClick(e) {
    if (domUtils.isDescendentByClass(settings.accordionTriggerClass, e.target) !== false) {
      const clickedParentSection = e.target.parentNode;
      if (clickedParentSection.getAttribute('data-accordion-override') === null) {
        e.preventDefault();
        if (clickedParentSection.classList.contains(settings.accordionOpenedClass)) {
          collapse(clickedParentSection);
        } else {
          expand(clickedParentSection);
        }
      }
    }
  }

  function wrapSections() {
    settings.initOpenSlideIndexes = objectUtils.convertToArray(settings.initOpenSlideIndexes);
    accordionSections.forEach(function (section, index) {
      const sectionContent = slice.call(section.children);
      const sectionContentWrapper = domUtils.createElement('div', settings.accordionContentWrapperClass);
      const sectionHeadingEl = section.querySelector('.' + settings.accordionHeadingClass);

      if (sectionHeadingEl) {
        let sectionTriggerLink = sectionHeadingEl.getAttribute('href');
        let sectionTriggerTarget = '_self';
        const sectionTriggerEl = domUtils.createElement('a', settings.accordionTriggerClass);

        if (sectionTriggerLink === null) {
          sectionTriggerLink = '#';
        } else {
          if (sectionHeadingEl.getAttribute('target') !== null) {
            sectionTriggerTarget = sectionHeadingEl.getAttribute('target');
          }
        }
        sectionTriggerEl.setAttribute('href', sectionTriggerLink);
        sectionTriggerEl.setAttribute('target', sectionTriggerTarget);
        sectionTriggerEl.innerHTML = sectionHeadingEl.innerHTML;

        if (settings.closedOnInit) {
          setToClosed(section);
        } else {
          if (settings.openSlideIndexes.indexOf(index) === -1) {
            setToClosed(section);
          } else {
            if (settings.oneAtATime) {
              if (oneAtATimeSet) {
                setToClosed(section);
              } else {
                oneAtATimeSet = true;
              }
            }
          }
        }
        domUtils.wrapElements(sectionContent, sectionContentWrapper);
        section.insertBefore(sectionTriggerEl, section.firstChild);
        domUtils.addCssClasses(sectionHeadingEl, [settings.accordionHiddenClass]);
      } else {
        logger.error('No section headings found.');
      }
    });
    if (settings.onSectionWrapComplete !== null) {
      settings.onSectionWrapComplete();
    }
  }

  function unwrapSections() {
    accordionSections.forEach(function (section) {
      const sectionTriggerEl = section.querySelector('.' + settings.accordionTriggerClass);
      const sectionHeading = section.querySelector('.' + settings.accordionHeadingClass);
      const sectonContentWrapper = section.querySelector('.' + settings.accordionContentWrapperClass);

      sectionTriggerEl.parentNode.removeChild(sectionTriggerEl);
      domUtils.addCssClasses(section, [settings.accordionOpenedClass]);
      domUtils.removeCssClasses(sectionHeading, [settings.accordionHiddenClass]);
      domUtils.unwrapElements(sectonContentWrapper);
    });
    if (settings.onSectionUnwrapComplete !== null) {
      settings.onSectionUnwrapComplete();
    }
  }

  function setToClosed(section) {
    domUtils.removeCssClasses(section, [settings.accordionOpenedClass]);
  }

  function expand(section) {
    if (settings.oneAtATime) {
      accordionSections.forEach(function (section) {
        domUtils.removeCssClasses(section, [settings.accordionOpenedClass]);
      });
    }
    domUtils.addCssClasses(section, [settings.accordionOpenedClass]);
    if (settings.onSectionExpandComplete !== null) {
      settings.onSectionExpandComplete(section);
    }
  }

  function collapse(section) {
    domUtils.removeCssClasses(section, [settings.accordionOpenedClass]);
    if (settings.onSectionCollapseComplete !== null) {
      settings.onSectionCollapseComplete(section);
    }
  }

  function collapseAll() {
    accordionSections.forEach(function (section) {
      domUtils.removeCssClasses(section, [settings.accordionOpenedClass]);
    });
  }

  function destroy() {
    unbindEvents();
    unwrapSections();
  }

  init();

  return {
    collapse,
    expand,
    destroy,
    collapseAll,
  };
}