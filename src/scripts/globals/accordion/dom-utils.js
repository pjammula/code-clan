export default {

  elements: {
    body: document.body,
  },

  isDescendentByClass(parentClass, el) {
    if (el.classList && el.classList.contains(parentClass)) {
      return el;
    }
    let node = el.parentNode;
    while (node !== null) {
      if ((typeof node.classList !== 'undefined') && (node.classList.contains(parentClass))) {
        return node;
      }
      node = node.parentNode;
    }
    return false;
  },

  isDescendentByEl(parentEl, el) {
    if (el.tagName === parentEl) {
      return el;
    }
    let node = el.parentNode;
    while (node !== null) {
      if ((typeof node.tagName !== 'undefined') && (node.tagName === parentEl)) {
        return node;
      }
      node = node.parentNode;
    }
    return false;
  },

  isElement(o) {
    return (
      typeof HTMLElement === 'object' ? o instanceof HTMLElement : o && typeof o === 'object' &&
          o !== null && o.nodeType === 1 && typeof o.nodeName === 'string'
    );
  },

  createElement(tag, classNames) {
    const el = document.createElement(tag);
    classNames = this.ensureArray(classNames);

    classNames.forEach(function (className) {
      el.classList.add(className);
    });
    return el;
  },

  emptyElement(parentEl) {
    while (parentEl.firstChild) {
      parentEl.removeChild(parentEl.firstChild);
    }
  },

  removeElements(els) {
    els = this.ensureArray(els);
    els.forEach(function (el) {
      el.parentNode.removeChild(el);
    });
  },

  wrapElements(elsToWrap, wrapperEl) {
    elsToWrap = this.ensureArray(elsToWrap);

    const firstElToWrap = elsToWrap[0];
    firstElToWrap.parentNode.insertBefore(wrapperEl, firstElToWrap);

    elsToWrap.forEach(function (elToWrap) {
      wrapperEl.appendChild(elToWrap);
    });
  },

  unwrapElements(wrapperEl) {
    const fragment = document.createDocumentFragment();
    while (wrapperEl.firstChild) {
      fragment.appendChild(wrapperEl.firstChild);
    }
    wrapperEl.parentNode.replaceChild(fragment, wrapperEl);
  },

  addCssClasses(el, cssClasses) {
    cssClasses = this.ensureArray(cssClasses);

    for (let i = 0; i < cssClasses.length; i++) {
      el.classList.add(cssClasses[i]);
    }
  },

  removeCssClasses(el, cssClasses) {
    cssClasses = this.ensureArray(cssClasses);

    for (let i = 0; i < cssClasses.length; i++) {
      el.classList.remove(cssClasses[i]);
    }
  },

  ensureArray(obj) {
    if (Array.isArray(obj) === false) {
      return [obj];
    }
    return obj;
  },
};
