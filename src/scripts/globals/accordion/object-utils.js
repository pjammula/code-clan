export default {
  convertToArray(out) {
    if (!Array.isArray(out)) {
      return [out];
    }
    return out;
  },
};
