import defaults from './defaults';
import numberUtils from '../../utility/numbers';
import easing from '../easing/easing';

export function cycle(element) {
  let currentIteration;
  let totalIterations;
  let changeInValue;
  let animationRequestId;
  let settings;

  const fps = 60;

  function verifyInitialValue() {
    if (isNaN(settings.initialValue)) {
      settings.initialValue = numberUtils.stringToNumber(element.textContent);
    }

    if (isNaN(settings.initialValue)) {
      throw new TypeError('initialValue must be a number');
    }
  }

  function verifyFinalValue() {
    if (isNaN(settings.finalValue)) {
      throw new TypeError('finalValue must be a number');
    }
  }

  function verifyDuration() {
    if (isNaN(settings.duration)) {
      throw new TypeError('duration must be a number');
    } else if (settings.duration < 0) {
      throw new RangeError('duration cannot be a negative number');
    }
  }

  function getEasingFunction() {
    switch (settings.easing) {
      case 'easeOut':
        return easing.easeOutCubic;
      case 'easeInOut':
        return easing.easeInOutCubic;
      case 'easeIn':
        return easing.easeInCubic;
      default:
        return easing.linearEase;
    }
  }

  function getCurrentValue() {
    const easingFunction = getEasingFunction();
    return Math.round(easingFunction(currentIteration, settings.initialValue, changeInValue, totalIterations));
  }

  function updateElementContent(value) {
    element.textContent = numberUtils.formatNumber(value, settings.format);
  }

  function onAnimationFrame() {
    if (currentIteration < totalIterations) {
      currentIteration++;
      updateElementContent(getCurrentValue());
      animationRequestId = window.requestAnimationFrame(onAnimationFrame);
    }
  }

  /**
   * public
   * init function
   * @param  {object} options
   */
  function init(options) {
    settings = {...defaults, ...options};

    verifyDuration();
    verifyInitialValue();
    verifyFinalValue();

    if (settings.finalValue === settings.initialValue) {
      return;
    }

    if (!window.requestAnimationFrame || settings.duration === 0) {
      updateElementContent(settings.finalValue);
    } else {
      if (animationRequestId !== null) {
        window.cancelAnimationFrame(animationRequestId);
      }

      currentIteration = 0;
      totalIterations = Math.ceil(fps * (settings.duration / 1000));
      changeInValue = settings.finalValue - settings.initialValue;
      element.textContent = numberUtils.formatNumber(settings.initialValue, settings.format);

      animationRequestId = window.requestAnimationFrame(onAnimationFrame);
    }
  }

  /**
   * public
   * stop function
   */
  function stop() {
    if (settings.finalValue === settings.initialValue) {
      return;
    }
    if (animationRequestId !== null) {
      window.cancelAnimationFrame(animationRequestId);
    }
  }

  // expose public api
  return {
    init,
    stop,
  };
}
