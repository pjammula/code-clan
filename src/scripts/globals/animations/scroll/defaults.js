export default {
  element: null,
  position: null,
  duration: 500,
  offset: 0,
  easing: 'easeOut',
};
