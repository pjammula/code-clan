import easing from '../easing/easing';
import defaults from './defaults';

export function scrollTo(options) {

  const fps = 60;
  let currentIteration = 0;
  let totalIterations;
  let initialPosition;
  let totalPositionChange;
  let easingFunction;
  let settings;

  function init() {
    settings = {...defaults, ...options};

    if (!settings.element && !settings.position) {
      return;
    }

    const finalPosition = getFinalPosition();

    if (!window.requestAnimationFrame || settings.duration === 0) {
      move(finalPosition);
    } else {
      initialPosition = getInitialPosition();
      totalIterations = Math.ceil(fps * (settings.duration / 1000));
      totalPositionChange = finalPosition - initialPosition + settings.offset;
      easingFunction = getEasingFunction();

      animateScroll();
    }
  }

  function move(amount) {
    document.documentElement.scrollTop = amount;
    document.body.parentNode.scrollTop = amount;
    document.body.scrollTop = amount;
  }

  function getElementsTopOffset(element) {
    if ( !element.getClientRects().length ) {
      return 0;
    }

    const rect = element.getBoundingClientRect();
    const doc = element.ownerDocument;
    const docElem = doc.documentElement;
    const win = doc.defaultView;

    return rect.top + win.pageYOffset - docElem.clientTop;
  }

  function getFinalPosition() {
    let finalPosition = settings.position ? settings.position : getElementsTopOffset(settings.element);
    const viewportHeight = getViewportHeight();
    const documentHeight = getDocumentHeight();
    const maxPosition = documentHeight - viewportHeight;

    if (finalPosition > maxPosition) {
      finalPosition = maxPosition;
    }

    return finalPosition;
  }

  function getInitialPosition() {
    return document.documentElement.scrollTop || document.body.parentNode.scrollTop || document.body.scrollTop;
  }

  function getViewportHeight() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
  }

  function getDocumentHeight() {
    return Math.max(
      document.documentElement.clientHeight,
      document.body.scrollHeight, document.documentElement.scrollHeight,
      document.body.offsetHeight, document.documentElement.offsetHeight);
  }

  function animateScroll() {
    if (currentIteration < totalIterations) {
      currentIteration++;
      const val = Math.round(easingFunction(currentIteration, initialPosition, totalPositionChange, totalIterations));
      move(val);
      window.requestAnimationFrame(animateScroll);
    }
  }

  function getEasingFunction() {
    switch (settings.easing) {
      case 'easeOut':
        return easing.easeOutCubic;
      case 'easeInOut':
        return easing.easeInOutCubic;
      case 'easeIn':
        return easing.easeInCubic;
      default:
        return easing.linear;
    }
  }

  init();
}
