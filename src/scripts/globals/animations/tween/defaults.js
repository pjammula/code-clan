export default {
  noOp() {},
  duration: 1000,
  easing: 'easeIn',
  delay: 0,
};
