import defaults from './defaults';
import easings from '../easing/easing';

export function tween(options) {
  const settings = {...defaults, ...options};
  const rAF = window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            window.msRequestAnimationFrame ||
            function (fn) { setTimeout(fn, 10); };
  let canceled = false;
  let chainedTweens = [];

  function single(from, to, easing, current, duration) {
    return easing(current, from, to - from, duration);
  }

  function loop(from, to, easing, current, duration) {
    let k;
    let o = {};
    for (k in from) {
      o[k] = easing(current, from[k], to[k] - from[k], duration);
    }
    return o;
  }

  function _onComplete(onComplete) {
    onComplete();
    chainedTweens.forEach(function (tw) {
      tw.start();
    });
  }

  function getEasingFunction() {
    switch (settings.easing) {
      case 'easeOut':
        return easings.easeOutCubic;
      case 'easeInOut':
        return easings.easeInOutCubic;
      case 'easeIn':
        return easings.easeInCubic;
      case 'linearEase':
        return easings.linearEase;
      default:
        return easings.linearEase;
    }
  }

  /**
   * start function
   */
  function start() {
    const { from, to, duration, delay } = settings;
    const easing = getEasingFunction();
    const onStart = settings.onStart || settings.noOp;
    const onProgress = settings.onProgress || settings.noOp;
    const onComplete = settings.onComplete || settings.noOp;
    const update = typeof from === 'object' ? loop : single;
    const startTime = performance.now() + delay;
    const endTime = startTime + duration;
    let time;

    onStart();

    onProgress(from);

    rAF(function progress() {
      if (canceled) return;
      time = performance.now();
      if (startTime >= time) {
        onProgress(from);
        rAF(progress);
      };
      if (endTime <= time) {
        onProgress(to);
        _onComplete(onComplete);
      } else if (startTime < time) {
        onProgress(update(from, to, easing, time - startTime, duration));
        rAF(progress);
      }
    });
  }

  /**
   * stop function
   */
  function stop() {
    canceled = true;
  }

  /**
   * chain function
   * @param  {object} tween to chain
   */
  function chain(tw) {
    chainedTweens.push(tw);
  }

  return {
    start,
    stop,
    chain,
  };
}
