import logger from '../logger/logger';

function getClientHeight(elements) {
  return elements.map((index, item) => {
    return elements[item].clientHeight;
  });
}

function applyHeightsToElements(elements, tallestNum) {
  return elements.map((item, index) => {
    return elements[index].style.height = `${tallestNum}px`;
  });
}

function render(elements, tallestElement) {
  return applyHeightsToElements(
    elements,
    tallestElement
  );
}

function findTallestInArray(arr) {
  return Math.max(...arr);
}

function findSmallestInArray(arr) {
  return Math.min.apply(Math, arr.filter(Number));
}

function resetHeightOf(elements) {
  elements.forEach((element) => element.style.height = 'auto');
}

function isObject(obj) {
  const objType = typeof obj;
  return !Array.isArray(obj) && objType === 'object';
}

export function getSmallestItemHeight(elements) {
  const arrayOfElements = Array.from(elements);
  return findSmallestInArray(getClientHeight(arrayOfElements));
}

export default function equalize(opts) {
  const {
    element,
  } = opts;

  const arrayOfElements = Array.from(element);
  resetHeightOf(arrayOfElements);
  const tallestElement = findTallestInArray(getClientHeight(arrayOfElements));

  if (!arrayOfElements.length) {
    logger.error(
      `You are trying to set equal heights ` +
      `to a DOM-node which does not exists. `
    );
  }

  if (!isObject(opts)) {
    logger.error(
      `The expected argument type of ` +
      `equalize is a plain object.`
    );
  }

  if (!opts.hasOwnProperty('element')) {
    logger.error(
      `Equalize is expecting the key ` +
      `"element" to calculate height from.`
    );
  }

  return render(
    arrayOfElements,
    tallestElement
  );
}
