import 'isomorphic-fetch';
import checkStatus from './check-status';
import parseJson from './parse-json';
import parseText from './parse-text';
import { parse as parseDomain } from 'psl';

function generateToken() {
  const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  let text = '';

  for (let i = 0; i < 15; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
}

function setCsrfCookie(name, value, days) {
  let expires = '';
  let upperLevelDomain = null;

  if (days) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = `; expires=${date.toGMTString()}`;
  }

  const domain = parseDomain(location.hostname);
  if (domain !== null) {
    upperLevelDomain = domain.sld + '.' + domain.tld;
  }
  document.cookie = `${name}=${value}${expires};domain=.${upperLevelDomain};path=/;secure`;
}

function getCsrftoken() {
  const cookieId = 'CSRF_TOKEN';
  const value = `; ${document.cookie}`;
  const parts = value.split(`; ${cookieId}=`);
  let token = null;
  if (parts.length === 2) {
    token = parts.pop().split(';').shift();
  } else {
    token = generateToken();
    setCsrfCookie(cookieId, token, 1);
  }
  return token;
}

const DEFAULTS = {
  method: 'GET',
  // credentials: 'include',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'x-csrf-token': getCsrftoken(),
  },
};

const makeCancelable = (promise) => {
  let hasCanceled_ = false;

  const wrappedPromise = new Promise((resolve, reject) => {
    promise.then((val) =>
      hasCanceled_ ? reject({ isCanceled: true }) : resolve(val)
    );
    promise.catch((error) =>
      hasCanceled_ ? reject({ isCanceled: true }) : reject(error)
    );
  });

  return {
    promise: wrappedPromise,
    cancel() {
      hasCanceled_ = true;
    },
  };
};

export default function apiFetch(url, options, body) {
  const fetchOptions = Object.assign(
      {},
      DEFAULTS,
      options, { body }
  );
  return fetch(url, fetchOptions)
      .then(checkStatus)
      .then(parseJson);
}

export function apiFetchCancelable(url, options, body) {
  const fetchOptions = Object.assign(
      {},
      DEFAULTS,
      options, { body }
  );
  return makeCancelable(fetch(url, fetchOptions)
      .then(checkStatus)
      .then(parseJson));
}

export function apiFetchText(url, options, body) {
  const fetchOptions = Object.assign(
      {},
      DEFAULTS,
      options, { body }
  );
  return fetch(url, fetchOptions)
      .then(checkStatus)
      .then(parseText);
}

export function apiFetchNoResponse(url, options, body) {
  const fetchOptions = Object.assign(
      {},
      DEFAULTS,
      options, { body }
  );
  return fetch(url, fetchOptions)
      .then(checkStatus);
}
