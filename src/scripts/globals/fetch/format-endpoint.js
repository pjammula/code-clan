import template from 'lodash/template';

const urlTemplate = function (rawUrl, interpolate) {
  return template(rawUrl, { interpolate });
};

/**
 * Formats API endpoints
 * @param  {string} rawUrl - preprocessed URL
 * @param  {object} variables - variable keys and values to be inserted into URL
 * @param  {object RegExp} interpolate - interpolation pattern
 * @return {string} formatted URL
 */
export default function formatEndpoint(rawUrl, variables, interpolate) {
  return urlTemplate(rawUrl, interpolate)(variables);
}
