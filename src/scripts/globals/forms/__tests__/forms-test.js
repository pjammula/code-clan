import forms from '../forms';
jasmine.getFixtures().fixturesPath = '/base/src/scripts/globals/forms/__tests__/fixtures/';

describeComponent(forms, function () {
  const fixture = readFixtures('FORMS-TEST-PARSLEY.html');

  beforeEach(function () {
    this.setupComponent(fixture);
  });

  describe('Globals', function () {
    it('should have component defined', function () {
      expect(this.component).toBeDefined();
    });

    it('should add validators on init', function () {
      const validators = [
        ['isEmail', jasmine.anything()],
        ['ukCode', jasmine.anything()],
        ['usCode', jasmine.anything()],
        ['caCode', jasmine.anything()],
        ['isMonth', jasmine.anything()],
        ['matchSavedCardNumber', jasmine.anything()],
        ['date', jasmine.anything()]
      ];
      const asyncValidators = [
        ['isEmailFree', jasmine.anything(), undefined]
      ];
      spyOn(window.Parsley, 'addValidator').and.callThrough();
      spyOn(window.Parsley, 'addAsyncValidator').and.callThrough();

      this.component.initFormValidation();

      expect(window.Parsley.addValidator.calls.allArgs()).toEqual(validators);
      expect(window.Parsley.addAsyncValidator.calls.allArgs()).toEqual(asyncValidators);
    });

  });

  describe('Card Expiry Date Validation', function() {

        it('it should return field values and ids without index', function () {
              const parsleyField = $('#expiryMonth-0').parsley();

              const values = this.component.getExpiryDateFieldsValues(parsleyField);

              expect(values.length).toEqual(4);
              expect(values[0]).toEqual('10');
              expect(values[1]).toEqual('11');
              expect(values[2]).toEqual('expiryMonth-0');
              expect(values[3]).toEqual('expiryYear-0');
        });
        it('it should return field values and ids with index', function () {
            const parsleyField = $('#0-expiryMonth-0').parsley();

            const values = this.component.getExpiryDateFieldsValues(parsleyField);

            expect(values.length).toEqual(4);
            expect(values[0]).toEqual('53');
            expect(values[1]).toEqual('12');
            expect(values[2]).toEqual('0-expiryMonth-0');
            expect(values[3]).toEqual('0-expiryYear-0');
        });
        it('it should return good expiry date true', function () {
             const parsleyField = $('#0-expiryMonth-1').parsley();

             spyOn(this.component, 'getExpiryDateFieldsValues').and.returnValue(["12", "18", '0-expiryMonth-1', '0-expiryYear-1']);

             const values = this.component.dateValidatorHandler(parsleyField);

             expect(values.toString()).toEqual('true');
        });
        it('it should return good expiry date false, month not numeric', function () {
               const parsleyField = $('#0-expiryMonth-2').parsley();

               spyOn(this.component, 'getExpiryDateFieldsValues').and.returnValue(["18", "18", '0-expiryMonth-2', '0-expiryYear-2']);

               const values = this.component.dateValidatorHandler(parsleyField);

               expect(values.toString()).toEqual('true');
        });
        it('it should return good expiry date false, year out of range', function () {
               const parsleyField = $('#0-expiryMonth-3').parsley();

               spyOn(this.component, 'getExpiryDateFieldsValues').and.returnValue(["18", '00', '0-expiryMonth-3', '0-expiryYear-3']);

               const values = this.component.dateValidatorHandler(parsleyField);

               expect(values.toString()).toEqual('false');
        });
  });

  describe('Email address format validation', function () {
    it('valid emails should validate successully', function () {
      executeEmailValidation('prettyandsimple@example.com', true);
      executeEmailValidation('very.common@example.com', true);
      executeEmailValidation('disposable.style.email.with+symbol@example.com', true);
      executeEmailValidation('other.email-with-dash@example.com', true);
      executeEmailValidation('x@example.com', true);
      executeEmailValidation('email.with\'apostrophe@dyson.com', true);
      executeEmailValidation('example-indeed@strange-example.com', true);
      executeEmailValidation('example@s.solutions', true);
      executeEmailValidation('test@test.co.uk', true);
      executeEmailValidation('test_test_@dyson.com', true);
    });
    
    it('invalid email should not validate successfully', function() {
      executeEmailValidation('plainaddress', false);
      executeEmailValidation('#@%^%#$@#$@#.com', false);
      executeEmailValidation('@example.com', false);
      executeEmailValidation('Joe Smith <email@example.com>', false);
      executeEmailValidation('email.example.com', false);
      executeEmailValidation('email@example@example.com', false);
      executeEmailValidation('.email@example.com', false);
      executeEmailValidation('email.@example.com', false);
      executeEmailValidation('email..email@example.com', false);
      executeEmailValidation('あいうえお@example.com', false);
      executeEmailValidation('email@example.com (Joe Smith)', false);
      executeEmailValidation('email@example', false);
      executeEmailValidation('email@-example.com', false);
      executeEmailValidation('email@111.222.333.44444', false);
      executeEmailValidation('email@example..com', false);
      executeEmailValidation('Abc..123@example.com', false);
      executeEmailValidation('"(),:;<>[\\]@example.com', false);
      executeEmailValidation('just"not"right@example.com', false);
      executeEmailValidation('this\\ is-really"not\\allowed@example.com', false);
      executeEmailValidation('i am amazing@dyson.com', false);
      executeEmailValidation('much.more unusual@example.com', false);
      executeEmailValidation('very.unusual.@.unusual.com@example.com', false);
      executeEmailValidation('very.(),:;<>[]\\.VERY.\\\"very@\\\\ \\\"very\\\".unusual\"@strange.example.com', false);
      executeEmailValidation('admin@mailserver1', false);
      executeEmailValidation('#!$%&\'*+-/=?^_`{}|~@example.org', false);
      executeEmailValidation('()<>[]:,;@\\\\\\!#$%&\'-/=?^_`{}| ~.a\"@example.org', false);
      executeEmailValidation(' @example.org', false);
      executeEmailValidation('user@localserver', false);
      executeEmailValidation('user@tt', false);
      executeEmailValidation('user@[IPv6:2001:DB8::1]', false);
      executeEmailValidation('Pelé@example.com', false);
      executeEmailValidation('δοκιμή@παράδειγμα.δοκιμή', false);
      executeEmailValidation('我買@屋企.香港', false);
      executeEmailValidation('甲斐@黒川.日本', false);
      executeEmailValidation('чебурашка@ящик-с-апельсинами.рф', false);
      executeEmailValidation('संपर्क@डाटामेल.भारत', false);
      executeEmailValidation('emoji😈@example.com', false);
    });

    function executeEmailValidation(emailAddress, expectedResult) {
      // ARRANGE
      const field = $('.js-email');
      const form = $('.js-email-form');

      field.val(emailAddress);

      const $expectedResult = expectedResult;
      let actualResult = !$expectedResult;

      form.parsley().on('form:validate', function (formInstance) {
        actualResult = formInstance.isValid();
      });

      // ACT
      form.parsley().validate();

      // ASSERT
      expect(actualResult).toEqual($expectedResult, "Email validation failed for: " + emailAddress);
    }
  });
});
