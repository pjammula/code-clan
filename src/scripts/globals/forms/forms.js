import { component } from 'flight';
import $ from 'jquery';
import extend from 'lodash/extend';
import forEach from 'lodash/forEach';
import 'parsleyjs';

export default component(function Forms() {
  this.attributes({
    // selectors
    form: '.js-forms',
    input: '.js-forms-text',
    select: '.js-forms-select',
    group: '.js-form-group',
    control: '.js-form-control',
    savedCardNumber: '.js-saved-card-number',
    selectedSavedCardNumber: '.js-saved-card-number.selected',
    expiryMonthField: '.js-expiry-month',
    expiryYearField: '.js-expiry-year',
    textarea: '.js-forms-textarea',
    email: '.js-user-email',
    // values
    parsleyConfig: {
      successClass: 'forms--valid',
      errorClass: 'forms--error',
      focus: 'first',
      trigger: 'focusout',
      validationMinlength: 0,
      errorsContainer(element) {
        return $(element.$element).closest('.forms-group').find('.js-form-error');
      },
      errorsWrapper: '<span></span>',
      errorTemplate: '<span></span>',
      excluded: 'input[type=button], input[type=submit], input[type=reset],' +
      ' input[type=hidden], [data-parsley-disabled]',
      classHandler(element) {
        return $(element.$element).closest('.forms-group');
      },
      validators: {},
    },
  });

  this.initFormValidation = function () {
    window.ParsleyConfig = extend({}, window.ParsleyConfig, this.attr.parsleyConfig);
    this.addValidators();
    this.bindForms();
  };

  this.addValidators = function () {
    const self = this;
    window.Parsley.addValidator('isEmail', {
      validateString(value) {
        const emailRegex = new RegExp(/^([A-Za-z0-9_+'-]+[.]?)*[A-Za-z0-9_]@([A-Za-z0-9]+[_+'.-]?)*[A-Za-z0-9][.][A-Za-z]{2,62}$/);
        return emailRegex.test(value);
      },
      priority: 32,
    });

    window.Parsley.addValidator('ukCode', {
      validateString(value, requirement, element) {
        const ukPostCodeRegex = /^[A-Z]{1,2}[A-Z0-9]{1,2} ?[0-9][A-Z]{2}/i;
        const ukPostCodeFormatRegex = /(^[A-Z]{1,2}[A-Z0-9]{1,2})([0-9][A-Z]{2}$)/i;
        const isValidPostCode = ukPostCodeRegex.test(value.trim());

        if (value && isValidPostCode) {
          element.$element.val(value.trim().replace(ukPostCodeFormatRegex, '$1 $2').toUpperCase());
        }
        return isValidPostCode;
      },
      priority: 32,
    });

    window.Parsley.addValidator('usCode', {
      validateString(value) {
        const usPostCodeRegex = /^[0-9\-]{4,16}$/i;
        return usPostCodeRegex.test(value);
      },
      priority: 32,
    });

    window.Parsley.addValidator('caCode', {
      validateString(value) {
        const caPostCodeRegex = /^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} \d{1}[A-Z]{1}\d{1}$/i;
        return caPostCodeRegex.test(value);
      },
      priority: 32,
    });

    window.Parsley.addValidator('isMonth', {
      validateString(value) {
        const monthRegex = /^(0[1-9]|1[0-2])$/;
        return monthRegex.test(value);
      },
      priority: 32,
    });

    window.Parsley.addValidator('matchSavedCardNumber', {
      validateString(value) {
        if (self.select('selectedSavedCardNumber').get(0) !== undefined) {
          return self.select('selectedSavedCardNumber') && (self.select('selectedSavedCardNumber')
            .get(0).innerText.slice(-4) === value.slice(-4));
        }

        return self.select('savedCardNumber') && (self.select('savedCardNumber')
          .get(0).innerHTML.slice(-4) === value.slice(-4));
      },
      priority: 32,
    });

    window.Parsley.addValidator('date', {
      requirementType: 'string',
      validateNumber: this.dateValidatorHandler,
      priority: 32,
      isNumeric: this.isNumeric,
      getExpiryDateFieldsValues: this.getExpiryDateFieldsValues,
    });

    if (this.select('email')) {
      window.Parsley.addAsyncValidator('isEmailFree', (xhr) => {
        let response;
        try {
          response = JSON.parse(xhr.responseText);
        } catch (e) {
          response = {};
        }
        return response.message === 'Username Free';
      }, this.select('email').data('emailCheckAction'));
    }
  };

  /**
   * Function to check if given input is a number
   * @function
   * @param {Integer} n - Value to be tested
   * @returns {boolean} If the value being tested is a number or not
   */
  this.isNumeric = function (n) {
    return !Number.isNaN(parseFloat(n)) && Number.isFinite(n);
  };


  /**
   * For the case when a logged in user has multiple cards added
   * we should know which fields of the expiryDate have been accessed.
   * Therefor these fields have an index which require a special
   * escaping before being accessed with jquery.
   * @function
   * @param {ParsleyField} parsleyField - parsley object
   * @returns {Array} Array containing the values for month, year and the
   * 2 ids of these fields
   */
  this.getExpiryDateFieldsValues = function (parsleyField) {
    let month;
    let year;
    let componentId = parsleyField.element.id.toString();
    let monthFieldId;
    let yearFieldId;

    if (componentId.indexOf('expiryYear') !== -1) {
      yearFieldId = componentId;
      year = /^\d/.test(componentId) ? $(`#\\3${componentId} `).val() : $(`#${componentId}`).val();
      componentId = componentId.replace('expiryYear', 'expiryMonth');
      monthFieldId = componentId;
      month = /^\d/.test(componentId) ? $(`#\\3${componentId} `).val() : $(`#${componentId}`).val();
    } else {
      monthFieldId = componentId;
      month = /^\d/.test(componentId) ? $(`#\\3${componentId} `).val() : $(`#${componentId}`).val();
      componentId = componentId.replace('expiryMonth', 'expiryYear');
      yearFieldId = componentId;
      year = /^\d/.test(componentId) ? $(`#\\3${componentId} `).val() : $(`#${componentId}`).val();
    }

    return [month, year, monthFieldId, yearFieldId];
  };

  this.dateValidatorHandler = function (value, requirements, parsleyField) {
    const expiryDateValues = this.getExpiryDateFieldsValues(parsleyField);
    let month = expiryDateValues[0];
    let year = expiryDateValues[1];

    if (month.length !== 2 || year.length !== 2) {
      return false;
    }

    month = parseInt(month, 10);
    year = parseInt(year, 10);

    if (this.isNumeric(month) === false || this.isNumeric(year) === false) {
      return false;
    }

    const date = new Date();
    const currentMonth = date.getMonth() + 1; // +1 for 0-based month
    const currentYear = parseInt(date.getFullYear().toString().substring(2, 4), 10);

    if (year > currentYear) {
      return true;
    }

    if (year === currentYear) {
      if (month >= currentMonth) {
        return true;
      }
    }

    return false;
  };


  this.bindForms = function () {
    forEach(this.select('form'), function (form) {
      $(form).attr('novalidate', 'novalidate');
      $(form).parsley(this.attr.parsleyConfig);
    }.bind(this));
  };

  this.handlePageLoad = function () {
    forEach(this.select('input'), function (input) {
      if (input.value.trim() !== '') {
        $(input).parent().addClass('forms-control--filled');
      }
    });
    forEach(this.select('select'), function (select) {
      if (select.value.trim() !== '') {
        $(select).parent().addClass('forms-control--filled');
      }
    });
  };

  this.handleFocusIn = function (e) {
    $(e.target.parentNode).addClass('forms-control--filled');
  };

  this.handleFocusOut = function (e) {
    if (e.target.value.trim() === '') {
      $(e.target.parentNode).removeClass('forms-control--filled');
    }
  };

  this.handleAutoFill = function (e) {
    if (e.target.value.trim() !== '') {
      $(e.target.parentNode).addClass('forms-control--filled');
    }
  };

  this.equalizeFields = function (form, field) {
    const group = field.closest(this.attr.group);
    const height = group.height();
    const sibling = group.data('sibling');
    const siblingDataTag = `[data-id="${sibling}"]`;

    form.find(siblingDataTag).css({
      minHeight: height,
    });
  };

  this.resetFields = function (form, field) {
    const group = field.closest(this.attr.group);
    const sibling = group.data('sibling');
    const siblingDataTag = `[data-id="${sibling}"]`;

    if (!this.isLinkedFieldValid(form, field)) {
      return;
    }
    form.find(siblingDataTag).css({
      minHeight: 0,
    });
  };

  this.isLinkedFieldValid = function (form, field) {
    const control = field.closest(this.attr.control);
    const linkedWith = control.data('linkedWith');
    const controlLinkedWithClass = `.${linkedWith}`;

    if (control.data('linkedWith')) {
      const linked = form.find(controlLinkedWithClass);
      return linked.parsley().isValid();
    }

    return true;
  };

  this.validateLinkedField = function (form, field) {
    const control = field.closest(this.attr.control);
    const linkedWith = control.data('linkedWith');
    const controlLinkedWithClass = `.${linkedWith}`;

    if (control.data('linkedWith')) {
      const linked = form.find(controlLinkedWithClass);
      if (!linked.parsley().isValid()) {
        linked.parsley().validate();
      }
    }
  };

  this.bindFieldErrorReporting = function (elem) {
    $(elem.$element).attr('aria-invalid', 'true');
    $(elem.$element).attr('aria-labelledby', elem._ui.errorsWrapperId);
    $(elem.$element).attr('aria-describedby', elem._ui.errorsWrapperId);
  };

  this.bindFieldSuccessReporting = function (elem) {
    $(elem.$element).attr('aria-invalid', 'false');
    $(elem.$element).removeAttr('aria-labelledby');
    $(elem.$element).removeAttr('aria-describedby');
  };

  /*
  There were cases when one filed remains on 'invalidated' state after the other field was validated
  So when one of monthField or yearField is validated, we check for the other one and force a validation
  on it
  */
  this.expiryDateSuccess = function (parsleyField) {
    if (parsleyField.$element.attr('name') === 'expiryYear' || parsleyField.$element.attr('name') === 'expiryMonth') {
      let monthField;
      let yearField;
      const expiryDateValues = this.getExpiryDateFieldsValues(parsleyField);
      const monthFieldId = expiryDateValues[2];
      const yearFieldId = expiryDateValues[3];

      $(this.attr.expiryMonthField).each(function (index, monthElement) {
        if (monthElement.id === monthFieldId) {
          monthField = $(this);
          return false;
        }
      });

      $(this.attr.expiryYearField).each(function (index, yearElement) {
        if (yearElement.id === yearFieldId) {
          yearField = $(this);
          return false;
        }
      });

      if (parsleyField.$element.attr('name') === 'expiryYear') {
        if (!monthField.parsley().isValid()) {
          monthField.parsley().validate();
        } else {
          monthField.parsley().reset();
        }
      }

      if (parsleyField.$element.attr('name') === 'expiryMonth') {
        if (!yearField.parsley().isValid()) {
          yearField.parsley().validate();
        } else {
          yearField.parsley().reset();
        }
      }
    }
  };

  this.after('initialize', function () {
    this.initFormValidation();
    this.on(document, 'uiAttachedComponents', this.bindForms);
    this.on(document, 'uiAttachedComponents', this.handlePageLoad);
    this.on(document, 'uiRenderedSavedCardForm', this.bindForms);
    this.on('focusin', {
      input: this.handleFocusIn,
      select: this.handleFocusIn,
      textarea: this.handleFocusIn,
    });
    this.on('focusout', {
      input: this.handleFocusOut,
      select: this.handleFocusOut,
      textarea: this.handleFocusOut,
    });
    this.on(document, 'input', this.handleAutoFill);

    this.on(document, 'field:error', (e, data) => this.equalizeFields(data.form, data.field));

    this.on(document, 'field:reset', (e, data) => this.resetFields(data.form, data.field));

    window.Parsley.on('field:error', (field) => {
      this.equalizeFields(field.parent.$element, field.$element);
      this.bindFieldErrorReporting(field);
    });

    window.Parsley.on('field:destroy', (field) => {
      this.resetFields(field.parent.$element, field.$element);
    });

    window.Parsley.on('field:reset', (field) => {
      this.resetFields(field.parent.$element, field.$element);
    });

    window.Parsley.on('field:success', (field) => {
      this.resetFields(field.parent.$element, field.$element);
      this.validateLinkedField(field.parent.$element, field.$element);
      this.bindFieldSuccessReporting(field);
      this.expiryDateSuccess(field);
    });
  });
});