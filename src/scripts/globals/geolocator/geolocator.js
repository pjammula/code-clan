export function geolocator() {
  function _isGeolocationAvailable() {
    return 'geolocation' in navigator ? true : false;
  }

  function getCoordinates() {
    let _coordinates;
    if (!_isGeolocationAvailable()) {
      return 'Geolocation is not supported by this device.';
    }
    _coordinates = new Promise(function (resolve, reject) {
      navigator.geolocation.getCurrentPosition(function (position) {
        const latitude = position.coords.latitude;
        const longitude = position.coords.longitude;

        resolve({ latitude, longitude });
      }, function () {
        reject('Geolocation did not return any results.');
      });
    });

    return _coordinates;
  }

  return {
    getCoordinates,
  };
}
