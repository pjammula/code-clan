import { component } from 'flight';

import Viewport from './viewport/viewport';
import Forms from './forms/forms';
import Utility from './utility/utility';
import User from './user/user';

const Globals = component(function Globals() {
  this.after('initialize', function () {
    Viewport.attachTo(window.document.body);
    Forms.attachTo(window.document.body);
    Utility.attachTo(window.document.body);
    User.attachTo(window.document.body);
  });
});

export default Globals;

