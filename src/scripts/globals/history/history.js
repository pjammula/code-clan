export function history() {
  const supported = !!window.history;
  let initialized = false;
  let popCallbacks;
  let pushCallbacks;

  function onPopState(callback) {
    if (supported) {
      popCallbacks.push(callback);
    }
  }

  function onPushState(callback) {
    if (supported) {
      pushCallbacks.push(callback);
    }
  }

  function pushState(state, title, uri) {
    if (supported) {
      window.history.pushState(state, title, uri);
      triggerPushStates(state, title, uri);
    }
  }

  function triggerPopStates(state) {
    popCallbacks.forEach(function (callback) {
      callback(state);
    });
  }

  function triggerPushStates(state, title, uri) {
    pushCallbacks.forEach(function (callback) {
      callback(state, title, uri);
    });
  }

  function replaceInitialState() {
    if (supported) {
      window.history.replaceState({}, document.title, window.location.href);
    }
  }

  function init() {
    if (supported && !initialized) {
      popCallbacks = [];
      pushCallbacks = [];
      initialized = true;

      replaceInitialState();

      window.addEventListener("popstate", function (e) {
        const state = e.state;

        if (state) {
          triggerPopStates(state);
        }
      }, false);
    }
  }

  init();

  return {
    init,
    pushState,
    onPopState,
    onPushState,
  };
}
