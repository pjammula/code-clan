import logger from '../logger';
import moment from 'moment';

const timeFormat = 'DD-MMM-YYYY HH:mm:ss';

describe('Globals', function () {
  describe('Logger Component', function () {
    it('should call log', function () {
      spyOn(console, 'log').and.callThrough();
      const currentTime = moment().format(timeFormat);
      logger.log('Hello World');
      expect(console.log).toHaveBeenCalledWith('[' + currentTime + ']', 'Hello World');
    });

    it('should call debug', function () {
      spyOn(console, 'debug').and.callThrough();
      const currentTime = moment().format(timeFormat);
      logger.debug('Hello World');
      expect(console.debug).toHaveBeenCalledWith('[' + currentTime + ']', 'Hello World');
    });

    it('should call info', function () {
      spyOn(console, 'info').and.callThrough();
      const currentTime = moment().format(timeFormat);
      logger.info('Hello World');
      expect(console.info).toHaveBeenCalledWith('[' + currentTime + ']', 'Hello World');
    });

    it('should call warn', function () {
      spyOn(console, 'warn').and.callThrough();
      const currentTime = moment().format(timeFormat);
      logger.warn('Hello World');
      expect(console.warn).toHaveBeenCalledWith('[' + currentTime + ']', 'Hello World');
    });

    it('should call error', function () {
      spyOn(console, 'error').and.callThrough();
      const currentTime = moment().format(timeFormat);
      logger.error('Hello World');
      expect(console.error).toHaveBeenCalledWith('[' + currentTime + ']', 'Hello World');
    });
  });
});
