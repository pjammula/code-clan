import moment from 'moment';

const timeFormat = 'DD-MMM-YYYY HH:mm:ss';

const levels = ['debug', 'log', 'info', 'warn', 'error'];

const slice = Array.prototype.slice;

const logger = {};

levels.forEach(function (level) {
  logger[level] = function () {
    const args = slice.call(arguments);
    args.unshift('[' + moment().format(timeFormat) + ']');
    if (process.env.NODE_ENV === 'development') {
      console[level].apply(console, args);
    }
  };
});

export default logger;
