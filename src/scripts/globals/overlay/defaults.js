export default {
  openClass: 'overlay-is-open',
  containerClass: 'overlay',
  backdropClass: 'overlay__backdrop',
  closeButtonClass: ['overlay__close-button', 'js-overlay-close'],
  contentClass: 'overlay__content',
  contentClassModifier: 'overlay__content',
  backdrop: true,
};
