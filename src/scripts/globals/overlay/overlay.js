import domUtils from './dom-utils';
import defaults from './defaults';
import { dispatchJQueryEvent } from './dispatch-events';

export function overlay(element, options) {

  const bodyEl = domUtils.elements.body;
  let containerEl;
  let backdropEl;
  let contentEl;
  let elsCreated = false;
  let settings;
  let triggerEl;
  let firstTabEl;
  let lastTabEl;
  let triggeredByKeyboard;
  const focusableElsString = [
    'a[href]',
    'area[href]',
    'button:not([disabled])',
    'input:not([disabled])',
    'select:not([disabled])',
    'textarea:not([disabled])',
    'datalist:not([disabled])',
    'iframe',
    'object',
    'embed',
    '[tabindex]:not([tabindex="-1"])',
    '[contenteditable]',
  ].join();

  function init() {
    if (element) {
      settings = {...defaults, ...options};
      createElements(element);
    }
  }

  function open(triggeringEvent) {
    if (elsCreated) {
      triggerEl = document.activeElement;
      triggeredByKeyboard = isTriggeredByKeyboard(triggeringEvent);
      domUtils.addCssClasses(containerEl, settings.openClass);
      bodyEl.classList.add(settings.openClass);
      dispatchJQueryEvent(element, 'uiOpensOverlay');
      setTimeout(() => {
        const elements = Array.from(element.querySelectorAll(focusableElsString));
        firstTabEl = getFocusibleElement(elements);
        attachFocusTrap();
        if (firstTabEl && triggeredByKeyboard) {
          firstTabEl.focus();
        } else {
          element.focus();
        }
      }, 10);
    }
  }

  function getTabbable(element) {
    return Array.from(element.querySelectorAll(focusableElsString));
  }

  function close() {
    if (elsCreated) {
      domUtils.removeCssClasses(containerEl, settings.openClass);
      bodyEl.classList.remove(settings.openClass);
      dispatchJQueryEvent(element, 'uiClosesOverlay');
      if (triggeredByKeyboard) {
        triggerEl.focus();
      }
      detachFocusTrap();
    }
  }

  function getFocusibleElement(list) {
    return list.find(item => {
      if (item.offsetWidth !== 0 || item.offsetHeight !== 0 || item.getClientRects().length !== 0) {
        return true;
      };
      return false;
    });
  };

  function createElements(element) {
    if (!elsCreated) {
      containerEl = document.createElement('div');
      domUtils.addCssClasses(containerEl, settings.containerClass);

      contentEl = document.createElement('div');
      domUtils.addCssClasses(contentEl, settings.contentClass);
      containerEl.appendChild(contentEl);

      if (domUtils.isElement(element)) {
        domUtils.wrapElements(element, contentEl);
        domUtils.wrapElements(contentEl, containerEl);
      }

      if (settings.backdrop) {
        backdropEl = document.createElement('div');
        domUtils.addCssClasses(backdropEl, settings.backdropClass);
        containerEl.appendChild(backdropEl);
      }

      bindEvents();
      elsCreated = true;
    }
  }

  function bindEvents() {
    document.addEventListener('click', function (e) {
      const clickedEl = e.target;
      const isCloseButton = domUtils.isDescendentByClass(settings.closeButtonClass[0], clickedEl);
      const isBackDrop = domUtils.isDescendentByClass(settings.backdropClass, clickedEl);
      if ((isCloseButton !== false) || (isBackDrop !== false)) {
        close();
      }
    });
    containerEl.addEventListener('keyup', function (e) {
      if (e.keyCode === 27) {
        close();
      }
    });
  }

  function attachFocusTrap() {
    containerEl.addEventListener('keydown', setFocusTrap);
  }

  function detachFocusTrap() {
    containerEl.removeEventListener('keydown', setFocusTrap);
  }

  function setFocusTrap(e) {
    const elements = Array.from(element.querySelectorAll(focusableElsString));
    firstTabEl = getFocusibleElement(elements);
    lastTabEl = getFocusibleElement(elements.reverse());

    if (!(firstTabEl && lastTabEl)) {
      e.preventDefault();
      return;
    }

    if (e.keyCode === 9) {
      if (e.shiftKey) {
        if (document.activeElement === firstTabEl) {
          e.preventDefault();
          lastTabEl.focus();
        }
      } else {
        if (document.activeElement === lastTabEl) {
          e.preventDefault();
          firstTabEl.focus();
        }
      }
    }
  }

  function isTriggeredByKeyboard(eventObject) {
    if (eventObject) {
      const isKeyPress = eventObject.type === 'keydown' || eventObject.type === 'keyup';
      const isEnterKeyPress = eventObject.keyCode === 13;
      return isKeyPress && isEnterKeyPress;
    }
    return true;
  }

  init();

  return {
    init,
    open,
    close,
    getTabbable,
  };
}

export function createTabLoop(element) {
  const overlayFunctions = overlay();
  const tab = overlayFunctions.getTabbable(element);
  const first = tab[0];
  const last = tab[(tab.length - 1)];
  last.addEventListener('keydown', function (e) {
    if (e.key === 'Tab' && !e.shiftKey) {
      e.preventDefault();
      first.focus();
    }
  });
  first.addEventListener('keydown', function (e) {
    if (e.key === 'Tab' && e.shiftKey) {
      e.preventDefault();
      last.focus();
    }
  });
  return [first, last];
};
