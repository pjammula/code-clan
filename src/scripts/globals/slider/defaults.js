export default {
  /**
   * slides scrolled at once
   * @slidesToScroll {Number}
   */
  slidesToScroll: 1,

  /**
   * time in milliseconds for the animation of a valid slide attempt
   * @slideSpeed {Number}
   */
  slideSpeed: 300,

  /**
   * time for the snapBack of the slider if the slide attempt was not valid
   * @snapBackSpeed {Number}
   */
  snapBackSpeed: 100,

  /**
   * Basic easing functions: https://developer.mozilla.org/de/docs/Web/CSS/transition-timing-function
   * cubic bezier easing functions: http://easings.net/de
   * @ease {String}
   */
  ease: 'cubic-bezier(0.02, 0.01, 0.47, 1)',

  /**
   * number of visible slides or false
   * use infinite or rewind, not both
   * @infinite {number}
   */
  infinite: false,

  /**
   * enable peak feature with which part of the next and previous slides are visible
   * use true or false
   * @peak {Boolean}
   */
  peak: false,

  /**
   * class name for slider frame
   * @classNameFrame {string}
   */
  classNameFrame: 'js-frame',

  /**
   * class name for slides container
   * @classNameSlideContainer {string}
   */
  classNameSlideContainer: 'js-slides',

  /**
   * class name for slider prev control
   * @classNamePrevCtrl {string}
   */
  classNamePrevCtrl: 'js-slider-prev',

  /**
   * class name for slider next control
   * @classNameNextCtrl {string}
   */
  classNameNextCtrl: 'js-slider-next',

  /**
   * class name for current active slide
   * if emptyString then no class is set
   * @classNameActiveSlide {string}
   */
  classNameActiveSlide: 'active',

  /**
   * class name for slider indicator list
   * @classNameIndicatorList {string}
   */
  classNameIndicatorList: 'slider__indicator-list',

  /**
   * class name for slider indicator item
   * @classNameIndicatorItem {string}
   */
  classNameIndicatorItem: 'slider__indicator-item',

  /**
   * enables mouse events for swiping on desktop devices
   * @enableMouseEvents {boolean}
   */
  enableMouseEvents: false,

  /**
   * enables touch events for swiping on touch devices
   * @enableTouchEvents {boolean}
   */
  enableTouchEvents: true,

  /**
   * enables indicators
   * @showIndicators {Boolean}
   */
  showIndicators: false,

  /**
   * enables reset of slider on window resize
   * @resetOnResize {Boolean}
   */
  resetOnResize: true,

  /**
   * sets slide width parameter
   * @variableSlideWidth {Boolean}
   */
  variableSlideWidth: false,

  /**
   * class name for slide to be excluded from slider
   * @type {String}
   */
  excludeSlideClass: 'hidden',

  /**
   * min number of slides required to show controls
   * @type {Number}
   */
  slidesCtrlThreshold: 3,

  /**
   * window instance
   * @window {object}
   */
  window,
};
