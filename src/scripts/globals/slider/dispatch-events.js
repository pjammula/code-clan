import $ from 'jquery';

/**
 * dispatch custom events
 *
 * @param  {element} el         slideshow element
 * @param  {string}  type       custom event name
 * @param  {object}  detail     custom detail information
 */
export default function dispatchEvent(target, type, detail) {
  $(target).trigger({
    type,
    detail,
  });
}
