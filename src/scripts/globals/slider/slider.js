import detectPrefixes from '../utility/prefixes.js';
import dispatchEvent from './dispatch-events.js';
import defaults from './defaults.js';
import SUPPORT_PASSIVE from '../utility/support-passive';
import logger from '../logger/logger';

const slice = Array.prototype.slice;

export function slider(slider, opts) {
  let position;
  let slidesWidth;
  let frameWidth;
  let slides;

  /**
   * slider DOM elements
   */
  let frame;
  let slideContainer;
  let offsetLeft;
  let prevCtrl;
  let nextCtrl;

  let prefixes;
  let transitionEndCallback;
  let debouncedWindowResizeHandler;

  let index = 0;
  let options = {};
  let visibility = {start: true, end: true};

  /**
   * if object is jQuery convert to native DOM element
   */
  if (typeof jQuery !== 'undefined' && slider instanceof jQuery) {
    slider = slider[0];
  }

  /**
   * private
   * returns the widest element from a collection of elements
   */

  function getWidestElementFrom(collectionOfElements) {
    return collectionOfElements
      .map((element) => {
        return calcOuterWidthOf(element)
      })
      .sort((a, b) => b - a)[0];
  }

  /**
   * private
   * calculates & returns the width of an element including left & right margins
   */

  function calcOuterWidthOf(element) {
    const offsetWidth = element.offsetWidth;
    const computedStyles = getComputedStyle(element);
    const horizontalMargins = parseInt(computedStyles.marginLeft) + parseInt(computedStyles.marginRight);
    return offsetWidth + horizontalMargins;
  }

  /**
   * private
   * returns the width of an element including left & right padding
   */
  function calcInnerWidthOf(element) {
    return element.getBoundingClientRect().width || element.offsetWidth;
  }

  /**
   * private
   * set active class to element which is the current slide
   */
  function setActiveElement(slides, currentIndex) {
    const { classNameActiveSlide } = options;

    slides.forEach((element, index) => {
      if (element.classList.contains(classNameActiveSlide)) {
        element.classList.remove(classNameActiveSlide);
      }
    });

    slides[currentIndex].classList.add(classNameActiveSlide);
  }

  /**
   * private
   * remove active class from all elements
   */
  function removeActiveElement(slides) {
    const { classNameActiveSlide } = options;

    slides.forEach((element, index) => {
      if (element.classList.contains(classNameActiveSlide)) {
        element.classList.remove(classNameActiveSlide);
      }
    });
  }

  /**
   * private
   * debounce utility func
   */
  function debounce (fn, delay) {
    let timer = null;
    return function () {
      const args = arguments;
      clearTimeout(timer);
      timer = setTimeout(() => {
        fn.apply(this, args);
      }, delay);
    };
  }

  /**
   * private
   * setup indicators and set current one to active
   */
  function setupIndicators(slides, currentIndex) {
    const {
        classNameActiveSlide,
        classNameIndicatorList,
        classNameIndicatorItem,
    } = options;
    const indicatorList = createElement('ul', classNameIndicatorList);

    slides.forEach(function (slide, index) {
      const el = createElement('li', classNameIndicatorItem);
      if (index === currentIndex) {
        el.classList.add(classNameActiveSlide);
      }
      el.innerHTML = index;
      indicatorList.appendChild(el);
    });
    slider.appendChild(indicatorList);
  }

  /**
   * private
   * update indicators on slide
   */
  function updateIndicators(currentIndex) {
    const {
        classNameActiveSlide,
        classNameIndicatorItem,
    } = options;
    const indicators = slice.call(slider.getElementsByClassName(classNameIndicatorItem));

    indicators.forEach((indicator, index) => {
      if (indicator.classList.contains(classNameActiveSlide)) {
        indicator.classList.remove(classNameActiveSlide);
      }
    });

    indicators[currentIndex].classList.add(classNameActiveSlide);
  }

  /**
   * private
   * creates dom elements
   */
  function createElement(tag, classNames) {
    const el = document.createElement(tag);

    if (Array.isArray(classNames) === false) {
      classNames = [classNames];
    }
    classNames.forEach(function (className) {
      el.classList.add(className);
    });

    return el;
  }

  /**
   * private
   * setupInfinite: function to setup if infinite is set
   *
   * @param  {array} slideArray
   * @return {array} array of updated slideContainer elements
   */
  function setupInfinite(slideArray) {
    const { infinite } = options;

    const front = slideArray.slice(0, infinite);
    const back = slideArray.slice(slideArray.length - infinite, slideArray.length);

    front.forEach(function (element) {
      const cloned = element.cloneNode(true);

      slideContainer.appendChild(cloned);
    });

    back.reverse()
      .forEach(function (element) {
        const cloned = element.cloneNode(true);

        slideContainer.insertBefore(cloned, slideContainer.firstChild);
      });

    slideContainer.addEventListener(prefixes.transitionEnd, onTransitionEnd);

    return slice.call(slideContainer.children);
  }

  /**
   * [dispatchSliderEvent description]
   * @return {[type]} [description]
   */
  function dispatchSliderEvent(phase, type, detail) {
    dispatchEvent(slider, `${phase}.slider.${type}`, detail);
  }

  /**
   * translates to a given position in a given time in milliseconds
   *
   * @to        {number} number in pixels where to translate to
   * @duration  {number} time in milliseconds for the transistion
   * @ease      {string} easing css property
   */
  function translate(to, duration, ease) {
    const style = slideContainer && slideContainer.style;

    if (style) {
      style[prefixes.transition + 'TimingFunction'] = ease;
      style[prefixes.transition + 'Duration'] = duration + 'ms';

      if (prefixes.hasTranslate3d) {
        style[prefixes.transform] = 'translate3d(' + to + 'px, 0, 0)';
      } else {
        style[prefixes.transform] = 'translate(' + to + 'px, 0)';
      }
    }
  }

  /**
   * slidefunction called by prev, next & touchend
   *
   * determine nextIndex and slide to next postion
   * under restrictions of the defined options
   *
   * @direction  {boolean}
   */
  function slide(nextIndex, direction, multiCard) {
    const {
        slideSpeed,
        slidesToScroll,
        infinite,
        ease,
        peak,
        classNameActiveSlide,
        showIndicators
    } = options;
    let duration = multiCard ? slideSpeed * nextIndex : slideSpeed;
    const nextSlide = direction ? index + 1 : index - 1;
    const maxOffset = Math.round(slidesWidth - frameWidth + offsetLeft);

    dispatchSliderEvent('before', 'slide', {
      index,
      nextSlide,
    });

    if (typeof nextIndex !== 'number') {
      if (direction) {
        nextIndex = index + slidesToScroll;
      } else {
        nextIndex = index - slidesToScroll;
      }
    }

    nextIndex = Math.min(Math.max(nextIndex, 0), slides.length - 1);

    if (infinite && direction === undefined) {
      nextIndex += infinite;
    }
    let nextOffset = Math.min(Math.max((slides[nextIndex].offsetLeft - offsetLeft) * -1, maxOffset * -1), 0);

    /**
     * translate to the nextOffset by a defined duration and ease function
     */
    translate(nextOffset, duration, ease);

    /**
     * update the position with the next position
     */
    position.x = nextOffset;

    /**
     * update the index with the nextIndex only if
     * the offset of the nextIndex is in the range of the maxOffset
     */
    if (peak || slides[nextIndex].offsetLeft - offsetLeft <= maxOffset) {
      index = nextIndex;
    }

    if (infinite && (nextIndex === slides.length - infinite || nextIndex === 0)) {
      if (direction) {
        index = infinite;
      }

      if (!direction) {
        index = slides.length - (infinite * 2);
      }

      position.x = (slides[index].offsetLeft - offsetLeft) * -1;

      transitionEndCallback = function () {
        translate((slides[index].offsetLeft - offsetLeft) * -1, 0, undefined);
      };
    }

    if (classNameActiveSlide) {
      setActiveElement(slice.call(slides), index);
    }

    visibility.start =(slides[nextIndex].offsetLeft == 0 && nextIndex == 0);
    visibility.end = (slides[nextIndex].offsetLeft - offsetLeft >= maxOffset);
    updateControls(visibility);


    if (showIndicators) {
      updateIndicators(index);
    }

    dispatchSliderEvent('after', 'slide', {
      slider,
      currentSlide: index,
    });
  }


  /**
   * private
   * update controls on slider
   */
  function updateControls(visibility) {
    if (prevCtrl && nextCtrl) {
      if (visibility.start && visibility.end) {
        prevCtrl.classList.add('hidden-xs-up');
        nextCtrl.classList.add('hidden-xs-up');
      } else if (visibility.end && !visibility.start) {
        nextCtrl.classList.add('hidden-xs-up');
        prevCtrl.classList.remove('hidden-xs-up');
      } else if (!visibility.end && visibility.start) {
        nextCtrl.classList.remove('hidden-xs-up');
        prevCtrl.classList.add('hidden-xs-up');
      } else {
        nextCtrl.classList.remove('hidden-xs-up');
        prevCtrl.classList.remove('hidden-xs-up');
      }
    }
  }

  /**
   * public
   * setup function
   */
  function setup() {
    dispatchSliderEvent('before', 'init');

    prefixes = detectPrefixes();
    options = {...defaults, ...opts};

    const {
        classNameFrame,
        classNameSlideContainer,
        classNamePrevCtrl,
        classNameNextCtrl,
        enableMouseEvents,
        enableTouchEvents,
        classNameActiveSlide,
        showIndicators,
        slidesCtrlThreshold,
        resetOnResize,
        peak,
        excludeSlideClass,
        noOffset,
    } = options;

    frame = slider.getElementsByClassName(classNameFrame)[0];
    slideContainer = frame.getElementsByClassName(classNameSlideContainer)[0];
    prevCtrl = slider.getElementsByClassName(classNamePrevCtrl)[0];
    nextCtrl = slider.getElementsByClassName(classNameNextCtrl)[0];
    offsetLeft = peak ? slideContainer.offsetLeft : 0;
    offsetLeft = noOffset ? 0 : offsetLeft;
    position = {
      x: slideContainer.offsetLeft,
      y: slideContainer.offsetTop,
    };

    if (slideContainer.children.length === 0) {
      logger.error('You are trying to create a slider without slides');
      return;
    }

    if (options.infinite) {
      slides = setupInfinite(slice.call(slideContainer.children).filter(el => !el.classList.contains(excludeSlideClass)));
    } else {
      slides = slice.call(slideContainer.children).filter(el => (!el.classList.contains(excludeSlideClass) && !(el.nodeName ==='ESI:CHOOSE')));
    }

    reset();

    if (classNameActiveSlide) {
      setActiveElement(slides, index);
    }

    if (prevCtrl && nextCtrl) {
      prevCtrl.addEventListener('click', prev);
      nextCtrl.addEventListener('click', next);
    }

    if (enableTouchEvents) {
      slideContainer.addEventListener('touchstart', onTouchstart);
    }

    if (enableMouseEvents) {
      slideContainer.addEventListener('mousedown', onTouchstart);
      slideContainer.addEventListener('click', onClick);
    }

    if (showIndicators) {
      setupIndicators(slides, index);
    }

    if (resetOnResize) {
      debouncedWindowResizeHandler = debounce(onResize, 100);
      options.window.addEventListener('resize', debouncedWindowResizeHandler);
    }

    if(slides.length > slidesCtrlThreshold && prevCtrl && nextCtrl) {
      visibility.end = false;
    }

    updateControls(visibility);

    dispatchSliderEvent('after', 'init');
  }

  /**
   * public
   * reset function: called on resizes
   */
  function reset() {
    const { infinite, ease, slideSpeed, classNameActiveSlide, peak, variableSlideWidth } = options;
    const style = slideContainer && slideContainer.style;
    slidesWidth = variableSlideWidth ? calcInnerWidthOf(slideContainer) : getWidestElementFrom(slides) * slides.length;
    frameWidth = calcInnerWidthOf(frame);

    if (frameWidth === slidesWidth) {
      slidesWidth = slides.reduce(function (previousValue, slide) {
        return previousValue + calcInnerWidthOf(slide);
      }, 0);
    }

    if (style && peak) {
      style['width'] = Math.ceil(slidesWidth) + 'px';
    }

    index = 0;

    if (infinite) {
      translate((slides[index + infinite].offsetLeft - offsetLeft) * -1, 0, null);

      index = index + infinite;
      position.x = (slides[index].offsetLeft - offsetLeft) * -1;
    } else {
      translate(0, slideSpeed, ease);
    }

    if (classNameActiveSlide) {
      setActiveElement(slice.call(slides), index);
    }
  }

  /**
   * public
   * slideTo: called on clickhandler
   */
  function slideTo(index, longSlide) {
    slide(index, false, longSlide);
  }

  /**
   * public
   * returnIndex function: called on clickhandler
   */
  function returnIndex() {
    return index - options.infinite || 0;
  }

  /**
   * public
   * prev function: called on clickhandler
   */
  function prev() {
    slide(false, false);
  }

  /**
   * public
   * next function: called on clickhandler
   */
  function next() {
    slide(false, true);
  }

  /**
   * public
   * destroy function: called to gracefully destroy the lory instance
   */
  function destroy() {
    const {
        showIndicators,
        classNameIndicatorList,
    } = options;

    if (!slides) {
      return;
    }

    dispatchSliderEvent('before', 'destroy');

    // remove event listeners
    slideContainer.removeEventListener(prefixes.transitionEnd, onTransitionEnd);
    slideContainer.removeEventListener('touchstart', onTouchstart);
    slideContainer.removeEventListener('touchmove', onTouchmove);
    slideContainer.removeEventListener('touchend', onTouchend);
    slideContainer.removeEventListener('mousemove', onTouchmove);
    slideContainer.removeEventListener('mousedown', onTouchstart);
    slideContainer.removeEventListener('mouseup', onTouchend);
    slideContainer.removeEventListener('mouseleave', onTouchend);
    slideContainer.removeEventListener('click', onClick);
    options.window.removeEventListener('resize', debouncedWindowResizeHandler);

    slideContainer.removeAttribute('style');
    removeActiveElement(slides);

    if (prevCtrl) {
      prevCtrl.removeEventListener('click', prev);
    }

    if (nextCtrl) {
      nextCtrl.removeEventListener('click', next);
    }

    if (options.infinite) {
      Array.apply(null, Array(options.infinite)).forEach(function () {
        slideContainer.removeChild(slideContainer.firstChild);
        slideContainer.removeChild(slideContainer.lastChild);
      });
    }

    if (showIndicators) {
      const indicatorList = slider.getElementsByClassName(classNameIndicatorList);
      slider.removeChild(indicatorList[0]);
    }

    dispatchSliderEvent('after', 'destroy');
  }

  // event handling

  let touchOffset;
  let delta;
  let isScrolling;

  function onTransitionEnd() {
    if (transitionEndCallback) {
      transitionEndCallback();

      transitionEndCallback = undefined;
    }
  }

  function onTouchstart(event) {
    const { enableMouseEvents } = options;
    const touches = event.touches ? event.touches[0] : event;

    if (enableMouseEvents) {
      slideContainer.addEventListener('mousemove', onTouchmove);
      slideContainer.addEventListener('mouseup', onTouchend);
      slideContainer.addEventListener('mouseleave', onTouchend);
    }

    slideContainer.addEventListener('touchmove', onTouchmove);
    slideContainer.addEventListener('touchend', onTouchend);

    const { pageX, pageY } = touches;

    touchOffset = {
      x: pageX,
      y: pageY,
      time: Date.now(),
    };

    isScrolling = undefined;

    delta = {};

    dispatchSliderEvent('on', 'touchstart', {
      event,
    });
  }

  function onTouchmove(event) {
    const touches = event.touches ? event.touches[0] : event;
    const { pageX, pageY } = touches;

    delta = {
      x: pageX - touchOffset.x,
      y: pageY - touchOffset.y,
    };

    if (typeof isScrolling === 'undefined') {
      isScrolling = !!(isScrolling || Math.abs(delta.x) < Math.abs(delta.y));
    }

    if (!isScrolling && touchOffset) {
      event.preventDefault();
      translate(position.x + delta.x, 0, null);
    }

    // may be
    dispatchSliderEvent('on', 'touchmove', {
      event,
    });
  }

  function onTouchend(event) {
    /**
     * time between touchstart and touchend in milliseconds
     * @duration {number}
     */
    const duration = touchOffset ? Date.now() - touchOffset.time : undefined;

    /**
     * is valid if:
     *
     * -> swipe attempt time is over 300 ms
     * and
     * -> swipe distance is greater than 25px
     * or
     * -> swipe distance is more then a third of the swipe area
     *
     * @isValidSlide {Boolean}
     */
    const isValid = Number(duration) < 300 &&
        Math.abs(delta.x) > 25 ||
        Math.abs(delta.x) > frameWidth / 3;

    /**
     * is out of bounds if:
     *
     * -> index is 0 and delta x is greater than 0
     * or
     * -> index is the last slide and delta is smaller than 0
     *
     * @isOutOfBounds {Boolean}
     */
    const isOutOfBounds = !index && delta.x > 0 ||
        index === slides.length - 1 && delta.x < 0;

    const direction = delta.x < 0;

    if (!isScrolling) {
      if (isValid && !isOutOfBounds) {
        slide(false, direction);
      } else {
        translate(position.x, options.snapBackSpeed);
      }
    }

    touchOffset = undefined;

    /**
     * remove eventlisteners after swipe attempt
     */
    slideContainer.removeEventListener('touchmove', onTouchmove);
    slideContainer.removeEventListener('touchend', onTouchend);
    slideContainer.removeEventListener('mousemove', onTouchmove);
    slideContainer.removeEventListener('mouseup', onTouchend);
    slideContainer.removeEventListener('mouseleave', onTouchend);

    dispatchSliderEvent('on', 'touchend', {
      event,
    });
  }

  function onClick(event) {
    if (delta.x) {
      event.preventDefault();
    }
  }

  function onResize(event) {
    if (!isScrolling && frameWidth !== calcInnerWidthOf(frame)) {
      reset();

      dispatchSliderEvent('on', 'resize', {
        slider,
        event,
      });
    }
  }

  // trigger initial setup
  setup();

  // expose public api
  return {
    setup,
    reset,
    slideTo,
    returnIndex,
    prev,
    next,
    destroy,
  };
}
