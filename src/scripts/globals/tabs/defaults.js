export default {
  activeClass: 'active',
  triggerClass: 'js-tabs-toggle',
  sectionClass: 'js-tabs-content',
  targetName: 'data-target',
  indexName: 'data-index',
  useTriggerOrder: true,
  initialActiveSlideIndex: 0,
  onTabChange: null,
};
