import $ from 'jquery';
import CustomEvent from 'custom-event';

/**
 * dispatch custom jquery events
 *
 * @param  {Element} target     target element
 * @param  {String}  type       custom event name
 * @param  {Object}  detail     custom detail information
 */
export function dispatchJQueryEvent(target, type, detail) {
  $(target).trigger({
    type,
    detail,
  });
}

/**
 * dispatch custom events
 *
 * @param  {Element} target     target element
 * @param  {String}  type       custom event name
 * @param  {Object}  detail     custom detail information
 */
export function dispatchEvent(target, type, detail) {
  const event = new CustomEvent(
    type, {
      bubbles: true,
      cancelable: true,
      detail,
    }
  );
  target.dispatchEvent(event);
}
