export default {

  isDescendentByClass(parentClass, el) {
    if (el.classList && el.classList.contains(parentClass)) {
      return el;
    }
    let node = el.parentNode;
    while (node !== null) {
      if ((typeof node.classList !== 'undefined') && (node.classList.contains(parentClass))) {
        return node;
      }
      node = node.parentNode;
    }
    return false;
  },
};
