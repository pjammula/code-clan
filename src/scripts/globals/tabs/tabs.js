import defaults from './defaults';
import domUtils from './dom-utils';
import { dispatchEvent } from './dispatch-events';
import SUPPORT_PASSIVE from '../utility/support-passive';

const slice = Array.prototype.slice;

export function tabs(element, options) {
  let settings;
  let triggerEls = [];
  let sectionEls = [];
  let initialTabSet = false;

  function setInitialTab() {
    if (!initialTabSet) {
      [triggerEls[settings.initialActiveSlideIndex], sectionEls[settings.initialActiveSlideIndex]].forEach(function (el) {
        el.classList.add(settings.activeClass);
      });
    }
  }

  function onTriggerClick(e) {
    const clickedEl = domUtils.isDescendentByClass(settings.triggerClass, e.target);
    if (clickedEl !== false) {
      e.preventDefault();
      const clickedIndex = clickedEl.getAttribute(settings.indexName);
      let oldIndex = null;
      triggerEls.forEach(function (trigger, index) {
        if (trigger.classList.contains(settings.activeClass)) {
          oldIndex = index;
        }
        [trigger, trigger.tabTargetEl].forEach(function (el) {
          if (index !== parseInt(clickedIndex, 10)) {
            el.classList.remove(settings.activeClass);
          } else {
            el.classList.add(settings.activeClass);
          }
        });
      });
      if (settings.onTabChange !== null) {
        settings.onTabChange({
          newTabIndex: parseInt(clickedIndex, 10),
          oldTabIndex: oldIndex,
        });
      }
    }
  }

  function bindEvents() {
    document.addEventListener('click', onTriggerClick);
    document.addEventListener('touchstart', onTriggerClick, SUPPORT_PASSIVE ? { capture: false, passive: true } : false);
  }

  function addSection(triggerEl, index) {
    let sectionEl;
    if (settings.useTriggerOrder === true) {
      sectionEls = slice.call(element.querySelectorAll('.' + settings.sectionClass));
      sectionEl = sectionEls[index];
    } else {
      sectionEl = element.querySelector(triggerEl.getAttribute(settings.targetName));
      sectionEls.push(sectionEl);
    }
    triggerEls[index]['tabTargetEl'] = sectionEl;
    triggerEl.setAttribute(settings.indexName, index);
  }

  /**
   * public
   * init function
   */
  function init() {
    if (element) {
      settings = {...defaults, ...options};
      triggerEls = slice.call(element.querySelectorAll('.' + settings.triggerClass));
      triggerEls.forEach(function (triggerEl, index) {
        addSection(triggerEl, index);
        if (triggerEl.classList.contains(settings.activeClass)) {
          initialTabSet = true;
        }
      });
      setInitialTab();
      bindEvents();
    }
  }

  /**
   * public
   * destroy function
   */
  function destroy() {
    document.removeEventListener('click', onTriggerClick);
    document.removeEventListener('touchstart', onTriggerClick);
    triggerEls.forEach(function (triggerEl) {
      triggerEl.classList.remove(settings.activeClass);
    });
    sectionEls.forEach(function (sectionEl) {
      sectionEl.classList.remove(settings.activeClass);
    });
    triggerEls = [];
    sectionEls = [];
  }

  /**
   * public
   * add function
   * @param {Elmenet} triggerEl - element to add
   */
  function add(triggerEl) {
    triggerEls.push(triggerEl);
    addSection(triggerEl, triggerEls.length);
  }

  /**
   * public
   * show function
   * @param {integer} index - index of tab toggle to show
   */
  function show(index) {
    triggerEls.forEach(function (triggerEl) {
      if (triggerEl.getAttribute(settings.indexName) === index) {
        dispatchEvent(triggerEl, 'click', {});
      }
    });
  }

  // trigger initial setup
  init();

  // expose public api
  return {
    init,
    destroy,
    add,
    show,
  };
}
