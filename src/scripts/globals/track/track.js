import $ from 'jquery';

const INSTANCE_MAP = new Map();
const OBSERVER_MAP = new Map();

const options = {
  root: null,
  rootMargin: '0px',
};

/**
 * Monitor element and trigger callback at threshold
 * @param  {HTMLElement} element
 * @param  {Number} threshold Number between 0 and 1,
 * indicating how much of the element should be visible before triggering
 */
export function track(element, threshold = 0) {
  if (!element) return;
  let observerInstance = OBSERVER_MAP.get(threshold);
  if (!observerInstance) {
    observerInstance = new IntersectionObserver(onChange, Object.assign({}, options, { threshold }));
    OBSERVER_MAP.set(threshold, observerInstance);
  }

  INSTANCE_MAP.set(element, {
    visible: false,
    threshold,
  });
  observerInstance.observe(element);
}

/**
 * Stop observing an element. If an element is removed from the DOM
 * or otherwise destroyed, make sure to call this method.
 * @param  {HTMLElement} element
 */
export function untrack(element) {
  if (!element) return;

  if (INSTANCE_MAP.has(element)) {
    const { threshold } = INSTANCE_MAP.get(element);
    const observerInstance = OBSERVER_MAP.get(threshold);

    if (observerInstance) {
      observerInstance.unobserve(element);
      $(element).off('uiTracksElement');
    }

    // Check if we are still observing any elements with the same threshold.
    let itemsLeft = false;
    INSTANCE_MAP.forEach(item => {
      if (item.threshold === threshold) {
        itemsLeft = true;
      }
    });

    if (observerInstance && !itemsLeft) {
      // No more elements to observe for threshold, disconnect observer
      observerInstance.disconnect();
      OBSERVER_MAP.delete(threshold);
    }

    // Remove reference to element
    INSTANCE_MAP.delete(element);
  }
}

function dispatchEvent(target, element) {
  $(target).trigger('uiTracksElement', {
    inView: element.inView,
    percentageScrolled: element.intersectionRatio * 100,
  });
}

function onChange(changes) {
  changes.forEach(intersection => {
    if (INSTANCE_MAP.has(intersection.target)) {
      const { isIntersecting, intersectionRatio, target } = intersection;
      const { visible, threshold } = INSTANCE_MAP.get(target);

      // Trigger on 0 ratio only when not visible. This is fallback for browsers without isIntersecting support
      let inView = visible
        ? intersectionRatio > threshold
        : intersectionRatio >= threshold;

      if (isIntersecting !== undefined) {
        // If isIntersecting is defined, ensure that the element is actually intersecting.
        // Otherwise it reports a threshold of 0
        inView = inView && isIntersecting;
      }

      INSTANCE_MAP.set(target, {
        visible: inView,
        threshold,
      });

      dispatchEvent(target, {
        inView,
        intersectionRatio,
      });
    }
  });
}

export default {
  track,
  untrack,
};
