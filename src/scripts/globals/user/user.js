import { component } from 'flight';
import apiFetch from '../fetch/fetch';
import withResources from 'flight-with-resources';

export default component(function User() {
  this.attributes({
    esiUrl() {
      return this.$node.data('esiurl');
    },
    defaultUser: {
      credentialID: 'anonymous',
      loggedIn: false,
      machines: {},
      defaultContactAddress: {},
      firstName: null,
      lastName: null,
      name: null,
    },
  });

  this.requestUserData = function (event) {
    this.removeResource('userData');
    if (typeof window.user !== 'undefined') {
      this.getUserFromWindow()
          .then(response => this.provideUserData(response, event));
    } else {
      this.getUserFromApi()
          .then(response => this.provideUserData(response, event))
          .catch(() => this.provideUserData(this.attr.defaultUser, event));
    }
  };

  this.processUserData = function (response) {
    return {
      userId: response.credentialID,
      loggedIn: response.credentialID !== 'anonymous' ? true : false,
      machines: response.machines,
      name: response.name,
      defaultContactAddress: response.defaultContactAddress,
      firstName: response.defaultContactAddress ? response.defaultContactAddress.firstName : null,
      lastName: response.defaultContactAddress ? response.defaultContactAddress.lastName : null,
    };
  };

  this.provideUserData = function (response, event) {
    const userData = this.processUserData(response);
    this.removeResource('userData');
    this.provideResource('userData', userData);
    this.trigger(this.$node.closest(document), event, userData);
  };

  this.updateUserData = function (e, response) {
    const userData = this.processUserData(response);
    this.removeResource('userData');
    this.provideResource('userData', userData);
  };

  this.getUserFromWindow = function () {
    return new Promise(resolve => {
      const userData = {
        credentialID: window.user.credentialID,
        machines: window.user.machines,
        defaultContactAddress: window.user.defaultContactAddress,
        name: window.user.name,
      };
      resolve(userData);
    });
  };

  this.getUserFromApi = function () {
    return apiFetch(this.attr.esiUrl);
  };

  this.after('initialize', function () {
    //this.requestUserData('networkReturnsUserData');
    //this.on(document, 'uiLogsIn', this.updateUserData);
  });
}, withResources);
