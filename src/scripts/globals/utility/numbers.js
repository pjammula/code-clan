export default {
  stringToNumber(str) {
    return parseFloat(str.replace(/\D/g, ''));
  },
  formatNumber(number, format) {
    switch (format) {
      case '0':
        return number.toString();
      case '0,0':
      default:
        return this.formatNumberStandard(number);
    }
  },
  formatNumberStandard(number) {
    let nStr = number.toString();
    nStr += '';
    const x = nStr.split('.');
    let x1 = x[0];
    const x2 = x.length > 1 ? '.' + x[1] : '';
    const rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
  },
};
