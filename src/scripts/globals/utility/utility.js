import { component } from 'flight';
import $ from 'jquery';

export default component(function Utility() {
  this.attributes({
  });

  this.addToggleAttr = function () {
    $.fn.extend({
      toggleAttr(attr, turnOn) {
        const justToggle = (turnOn === undefined);
        return this.each(function () {
          if ((justToggle && !$(this).is('[' + attr + ']')) || (!justToggle && turnOn)) {
            $(this).attr(attr, attr);
          } else {
            $(this).removeAttr(attr);
          }
        });
      },
    });
  };

  this.after('initialize', function () {
    this.addToggleAttr();
  });
});
