import { component } from 'flight';
import { setThrottleInterval, listenTo } from '../visibility/viewport';

export default component(function Viewport() {
  this.attributes({
    // values
    resize: 100,
    orientation: 100,
    scroll: 100,
    visibility: 100,
  });

  /**
   * Binds scroll, resize, orientation and visibility listeners
   * @return {void}
   */
  this.bindListeners = function () {
    setThrottleInterval('scroll', this.attr.scroll);
    setThrottleInterval('resize', this.attr.resize);
    setThrottleInterval('orientation', this.attr.orientation);
    setThrottleInterval('visibility', this.attr.visibility);
    listenTo('all');
  };

  /**
   * Toggle body scroll
   * @param  {event} e
   * @param  {object} data - payload of event
   * @return {void}
   */
  this.toggleBodyScroll = function (e, data) {
    data.opened ? this.$node.addClass('no-scroll') : this.$node.removeClass('no-scroll');
  };

  this.after('initialize', function () {
    this.on(document, 'uiAttachedComponents', this.bindListeners);
    this.on(document, 'uiToggledNavDrawer uiToggledNavDropdown uiToggledOverlay uiToggledStickyNav',
        this.toggleBodyScroll);
  });
});
