import { getSize, getScrollPosition, broadcast } from './utils';

export default class TrackedElement {
  constructor(node) {
    this.node = node;
    this.updatePosition();
  }
}

TrackedElement.prototype.update = function (force) {
  this.inViewport().percentInViewport();
  this.percentScrolled();

  const type = (this.lastResult !== this.inview) ? 'visibility' :
            (this.lastPercentage !== this.percentage) ? 'percentage' :
            (force) ? 'update' : false;

  if (type) {
    broadcast('uiTracksElement', {
      element: this,
      type,
      inviewport: this.inview,
      percentage: this.percentage,
      percentageScrolled: this.percentageScrolled,
    }, this.node);
  }

  return this;
};

TrackedElement.prototype.updatePosition = function () {
  const rect = this.node.getBoundingClientRect();
  const scroll = getScrollPosition();
  const width = this.width = rect.width;
  const height = this.height = rect.height;
  const top = this.top = scroll.top + rect.top;
  const left = this.left = scroll.left + rect.left;
  this.bottom = top + height;
  this.right = left + width;
  this.area = width * height;
  return this;
};

TrackedElement.prototype.inViewport = function () {
  this.lastInview = this.inview || false;
  const scrollPos = getScrollPosition();
  const viewportDims = getSize();
  const viewport = {
    top: scrollPos.top,
    left: scrollPos.left,
    bottom: scrollPos.top + viewportDims.height,
    right: scrollPos.left + viewportDims.width,
  };

  this.inview = (

    // is in viewport vertically
    ((this.top >= viewport.top && this.top < viewport.bottom) ||
    (this.bottom > viewport.top && this.bottom <= viewport.bottom)) &&

    // is in viewport horizontally
    ((this.left >= viewport.left && this.left < viewport.right) ||
      (this.right > viewport.left && this.right <= viewport.right))
  );
  return this;
};

TrackedElement.prototype.percentInViewport = function () {
  this.lastPercentage = this.percentage || 0;

  const viewport = getSize();
  const scroll = getScrollPosition();
  const inViewWidth = Math.min(this.right, (scroll.left + viewport.width)) - Math.max(this.left, scroll.left);
  const inViewHeight = Math.min(this.bottom, (scroll.top + viewport.height)) - Math.max(this.top, scroll.top);
  const percentage = (inViewWidth * inViewHeight) / (this.area / 100);
  this.percentage = ((inViewHeight > 0 && inViewWidth > 0) && percentage > 0) ? Math.round(percentage) : 0;
  return this;
};

TrackedElement.prototype.percentScrolled = function () {
  const viewport = getSize();
  const scroll = getScrollPosition();
  const inView = scroll.top + viewport.height - this.top;
  const percentage = inView * 100 / (this.bottom - this.top);
  const scrolled = percentage < 100 ? Math.round(percentage) : 100;
  this.percentageScrolled = inView > 0 ? scrolled : 0;
  return this;
};
