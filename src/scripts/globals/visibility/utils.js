import $ from 'jquery';

export function broadcast(eventType, data, target) {
  target = target || document;

  $(target).trigger(eventType, data);
}

export function getHeight(ignoreScrollbars) {
  return ignoreScrollbars ? document.documentElement.clientHeight :
    Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
}

export function getWidth(ignoreScrollbars) {
  return ignoreScrollbars ? document.documentElement.clientWidth :
    Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
}

export function getSize(ignoreScrollbars) {
  return {
    height: getHeight(ignoreScrollbars),
    width: getWidth(ignoreScrollbars),
  };
}

export function getScrollPosition() {
  const de = document.documentElement;
  const db = document.body;
  const isCSS1Compat = ((document.compatMode || '') === 'CSS1Compat');
  const ieX = isCSS1Compat ? de.scrollLeft : db.scrollLeft;
  const ieY = isCSS1Compat ? de.scrollTop : db.scrollTop;

  return {
    height: db.scrollHeight,
    width: db.scrollWidth,
    left: window.pageXOffset || window.scrollX || ieX,
    top: window.pageYOffset || window.scrollY || ieY,
  };
}

export function getOrientation() {
  const orientation = window.screen.orientation || window.screen.mozOrientation ||
    window.screen.msOrientation || undefined;

  if (orientation) {
    return typeof orientation === 'string' ?
      orientation.split('-')[0] :
      orientation.type.split('-')[0];
  } else if (window.matchMedia) {
    return window.matchMedia('(orientation: portrait)').matches ? 'portrait' : 'landscape';
  }
  return getHeight() >= getWidth() ? 'portrait' : 'landscape';
}

export function detectVisiblityAPI() {
  let hiddenName;
  let eventType;
  if (typeof document.hidden !== 'undefined') {
    hiddenName = 'hidden';
    eventType = 'visibilitychange';
  } else if (typeof document.mozHidden !== 'undefined') {
    hiddenName = 'mozHidden';
    eventType = 'mozvisibilitychange';
  } else if (typeof document.msHidden !== 'undefined') {
    hiddenName = 'msHidden';
    eventType = 'msvisibilitychange';
  } else if (typeof document.webkitHidden !== 'undefined') {
    hiddenName = 'webkitHidden';
    eventType = 'webkitvisibilitychange';
  }

  return {
    hiddenName,
    eventType,
  };
}

export function getVisibility() {
  const hiddenName = detectVisiblityAPI().hiddenName;
  return document[hiddenName];
}

export function debounce(func, wait) {
  let timeout;
  return function () {
    const args = arguments;
    const later = () => {
      timeout = null;
      func.apply(this, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}

export function throttle(func, wait) {
  let timeout;
  return function () {
    if (timeout) {
      return;
    }
    const args = arguments;
    const later = () => {
      timeout = null;
      func.apply(this, args);
    };

    timeout = setTimeout(later, wait);
  };
}
