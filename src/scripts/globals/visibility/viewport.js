import { broadcast, getSize, getOrientation, getVisibility, getScrollPosition,
  detectVisiblityAPI, throttle, debounce } from './utils';

const listeners = {};
const intervals = {
  resize: 100,
  orientation: 100,
  visibility: 100,
  scroll: 100,
};
let lastScrollTop = 0;

function listenToResize() {
  if (listeners.resize) {
    return;
  }
  const eventType = 'resize';
  const handler = debounce(function (ev) {
    broadcast('uiResizedWindow', {
      viewport: getSize(),
      originalEvent: ev,
    });
  }, intervals.resize);


  window.addEventListener(eventType, handler);
  listeners.resize = {
    eventType,
    handler,
  };
}

function listenToOrientation() {
  if (listeners.orientation) {
    return;
  }

  const eventType = 'orientationchange';
  const handler = debounce(function (ev) {
    broadcast('uiChangedOrientation', {
      viewport: getSize(),
      orientation: getOrientation(),
      originalEvent: ev,
    });
  }, intervals.orientation);

  window.addEventListener(eventType, handler);
  listeners.orientation = {
    eventType,
    handler,
  };
}

function listenToVisibility() {
  if (listeners.visibility) {
    return;
  }

  const eventType = detectVisiblityAPI().eventType;
  const handler = debounce(function (ev) {
    broadcast('uiUpdatedVisibility', {
      hidden: getVisibility(),
      originalEvent: ev,
    });
  }, intervals.visibility);

  window.addEventListener(eventType, handler);

  listeners.visibility = {
    eventType,
    handler,
  };
}

function listenToScroll() {
  if (listeners.scroll) {
    return;
  }

  const eventType = 'scroll';
  const handler = throttle(function (ev) {
    const scrollPos = getScrollPosition();
    broadcast('uiScrolledWindow', {
      viewport: getSize(),
      direction: scrollPos.top > lastScrollTop ? 'DOWN' : 'UP',
      scrollHeight: scrollPos.height,
      scrollLeft: scrollPos.left,
      scrollTop: scrollPos.top,
      scrollWidth: scrollPos.width,
      originalEvent: ev,
    });
    lastScrollTop = scrollPos.top;
  }, intervals.scroll);

  window.addEventListener(eventType, handler);
  listeners.scroll = {
    eventType,
    handler,
  };
}

export function setThrottleInterval(eventType, interval) {
  if (typeof arguments[0] === 'number') {
    setThrottleInterval('scroll', arguments[0]);
    setThrottleInterval('resize', arguments[1]);
    setThrottleInterval('orientation', arguments[2]);
    setThrottleInterval('visibility', arguments[3]);
  } else if (interval) {
    intervals[eventType] = interval;
  }
}

export function listenTo(eventType) {
  if (eventType === 'resize' || eventType === 'all') {
    listenToResize();
  }

  if (eventType === 'scroll' || eventType === 'all') {
    listenToScroll();
  }

  if (eventType === 'orientation' || eventType === 'all') {
    listenToOrientation();
  }

  if (eventType === 'visibility' || eventType === 'all') {
    listenToVisibility();
  }
}

export function stopListeningTo(eventType) {
  if (eventType === 'all') {
    Object.keys(listeners).forEach(stopListeningTo);
  } else if (listeners[eventType]) {
    window.removeEventListener(listeners[eventType].eventType, listeners[eventType].handler);
    delete listeners[eventType];
  }
}
