import TrackedElement from './element';
import { listenTo } from './viewport';
import $ from 'jquery';

const tracked = [];
let tracking = false;
let bodyHeight = 0;

/*
* Start tracking an element
*/
export function track(element) {
  const exists = tracked.filter(sameElement(element));
  if (exists.length) {
    element = exists[0];
  } else {
    element = new TrackedElement(element);
    tracked.push(element);
    element.update();
    initEvents();
  }
  return element;
}

/*
* Stop tracking an element
*/
export function untrack(element) {
  const exists = tracked.filter(sameElement(element));
  if (exists.length) {
    $(element).off('uiTracksElement');
    tracked.splice(tracked.indexOf(exists[0]), 1);
    element = null;
  }
}


/*
* Call the update method on all tracked elements
*/
export function update(force) {
  tracked.forEach(function (element) {
    element.update(!!force);
  });
}

/*
* Call the updatePositions method on all tracked elements
*/
export function updatePositions(force) {
  tracked.forEach(function (element) {
    element.updatePosition().update(!!force);
  });
}

/*
* Initialise tracking on specified element
*/
export function init(selector) {
  let elements = [];

  try {
    elements = document.querySelectorAll(selector);
  } catch (err) {
    return;
  }

  if (elements.length) {
    [].slice.call(elements).forEach(track);
    update();
  }

  initEvents();
  bodyHeight = document.body.clientHeight;
}

/**
 * Stop tracking all currently tracked elements
 */
export function destroy() {
  tracked.length = 0;
  if (tracking === true) {
    $(document).off('uiChangedOrientation', updatePositions);
    $(document).off('uiResizedWindow', updatePositions);
    $(document).off('uiScrolledWindow', updateScrollHandler);
    $(document).off('uiUpdatedVisibility', update);
    tracking = false;
  }
}

function sameElement(element) {
  return function (item) {
    return item.node === element;
  };
}

function updateScrollHandler() {
  if (bodyHeight !== document.body.clientHeight) {
    bodyHeight = document.body.clientHeight;
    updatePositions();
  } else {
    update();
  }
}

function initEvents() {
  if (tracking === false) {
    listenTo('all');
    $(document).on('uiChangedOrientation', updatePositions);
    $(document).on('uiResizedWindow', updatePositions);
    $(document).on('uiScrolledWindow', updateScrollHandler);
    $(document).on('uiUpdatedVisibility', update);
    tracking = true;
  }
}
