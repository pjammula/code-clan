import * as TestImageLoader from '../image_loader';

const imageBreakpoints = '767:cropPathE=mobile&amp;fit=stretch,1&amp;wid=194|1024:cropPathE=tablet&amp;fit=stretch,1&amp;wid=278|1440:cropPathE=desktop&amp;fit=stretch,1&amp;wid=256';
const imageSizes = JSON.stringify({"mobile": "767", "tablet":"1024", "desktop": "1920"});

const elem = document.createElement('div');
elem.setAttribute('data-src', 'hi');
elem.setAttribute('data-custom-mobile-breakpoint', 'x-you');
elem.setAttribute('data-custom-tablet-breakpoint', 'color');
elem.setAttribute('data-custom-desktop-breakpoint', 'lullaby');
elem.setAttribute('data-breakpoints', imageBreakpoints);

const imageLoader = new TestImageLoader.ImageLoader(elem, imageSizes);

describe('Image Loader', function () {
  describe('if customised cropping paths exist', function () {
    it('should override default cropping paths', function () {
      const breakpointsString = imageBreakpoints;
      const expected = '767:cropPathE=x-you&amp;fit=stretch,1&amp;wid=194|1024:cropPathE=color&amp;fit=stretch,1&amp;wid=278|1440:cropPathE=lullaby&amp;fit=stretch,1&amp;wid=256';
      expect(imageLoader.getCustomisedBreakpointString(breakpointsString)).toEqual(expected);
    });
  });
});
