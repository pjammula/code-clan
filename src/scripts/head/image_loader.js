function minBy(array, iteratee) {
  var result;
  var index = -1;
  var length = array ? array.length : 0;
  var computed, value, current;

  while (++index < length) {
    value = array[index];
    current = iteratee(value);

    if (current != null && (computed === undefined
          ? (current === current)
          : (current < computed)
      )) {
      computed = current;
      result = value;
    }
  }
  return result;
};

function maxBy(array, iteratee) {
  var result;
  var index = -1;
  var length = array ? array.length : 0;
  var computed, value, current;

  while (++index < length) {
    value = array[index];
    current = iteratee(value);

    if (current != null && (computed === undefined
          ? (current === current)
          : (current > computed)
      )) {
      computed = current;
      result = value;
    }
  }
  return result;
};



export function ImageLoader(elem, imageSizes) {
  this.init = function () {
    if (!elem) {
      return console.warn('Element is required to initialize ImageLoader');
    }

    this.imgPreLoadSize = {
      desktop: '&fit=stretch,1&wid=1025',
      tablet: '&fit=stretch,1&wid=768',
      mobile: '&fit=stretch,1&wid=325',
    };

    this.baseSrc = elem.getAttribute('data-src');

    if (!this.baseSrc) {
      return console.warn('data-src attribute is required to initialize ImageLoader');
    }

    this.breakpoints = this.parseBreakpoints(elem.getAttribute('data-breakpoints'));

    this.minBreakpoint = minBy(this.breakpoints, function (item) {
      return item.width;
    });

    this.maxBreakpoint = maxBy(this.breakpoints, function (item) {
      return item.width;
    });

    this.placeholderSizes = this.parsePlaceholderSizes(imageSizes);

    this.setupImageObserver();

    this.loadLowResImage();

    this.loadImage();
  };

  this.loadImage = function () {
    elem.src = this.getImageSrc(this.baseSrc);
    elem.addEventListener('load', function () {
      this.observer.disconnect();
      elem.style.height = 'auto';
    }.bind(this));
  };

  this.getZoomFactor = function () {
    var clientWidth = document.documentElement.clientWidth;
    var clientHeight = document.documentElement.clientHeight;
    var zoomFactor = {
      orientation: 'landscape',
      zoom: 1,
    };

    if (typeof window.orientation !== 'undefined') {
      zoomFactor.orientation = ((screen.width/screen.height) > 1) ? 'landscape' : 'portrait';
      zoomFactor.zoom = clientWidth / window.innerWidth;
    } else {
      zoomFactor.orientation = clientWidth > clientHeight ? 'landscape' : 'portrait';
    }

    return zoomFactor;
  };

  this.getPixelRatio = function () {
    var retVal = 1;

    if (window.devicePixelRatio) {
      retVal = window.devicePixelRatio;
    } else if (screen.deviceXDPI) {
      retVal = window.screen.deviceXDPI / window.screen.logicalXDPI;
    } else if ('matchMedia' in window && window.matchMedia) {
      if (window.matchMedia('(min-resolution: 2dppx)').matches ||
        window.matchMedia('(min-resolution: 192dpi)').matches) {
        retVal = 2;
      } else if (window.matchMedia('(min-resolution: 1.5dppx)').matches ||
        window.matchMedia('(min-resolution: 144dpi)').matches) {
        retVal = 1.5;
      }
    }

    return retVal;
  };

  this.getCustomisedBreakpointString = function (breakpointsString) {
    if (elem.getAttribute('data-custom-mobile-breakpoint')) {
      breakpointsString = breakpointsString.replace("mobile", elem.getAttribute('data-custom-mobile-breakpoint'));
    };

    if (elem.getAttribute('data-custom-tablet-breakpoint')) {
      breakpointsString = breakpointsString.replace("tablet", elem.getAttribute('data-custom-tablet-breakpoint'));
    };

    if (elem.getAttribute('data-custom-desktop-breakpoint')) {
      breakpointsString = breakpointsString.replace("desktop", elem.getAttribute('data-custom-desktop-breakpoint'));
    };

    return breakpointsString;
  };

  this.parseBreakpoints = function (breakpointsString) {
    this.getCustomisedBreakpointString(breakpointsString);

    var breakpointsArray = !!breakpointsString ? breakpointsString.split('|') : [];

    return breakpointsArray.map(function (item) {
      var data = item.split(':');
      var width = data[0] - 0;
      var isCommands = data[1] || '';
      var viewport = data[1].match(/cropPathE=(.*?)&/) ? data[1].match(/cropPathE=(.*?)&/).pop() : 'mobile';

      return { width: width, isCommands: isCommands, viewport: viewport };
    });
  };

  this.parsePlaceholderSizes = function (placeholderString) {
    return JSON.parse(placeholderString);
  };

  this.getPlaceholderViewport = function () {
    var zoom = this.getZoomFactor().zoom;
    var screenWidth = Math.round(document.documentElement.clientWidth * zoom);
    var breakpoint;

    if (screenWidth <= this.minBreakpoint.width) {
      breakpoint = this.minBreakpoint;
    } else if (screenWidth >= this.maxBreakpoint.width) {
      breakpoint = this.maxBreakpoint;
    } else {
      this.breakpoints.forEach(function (item) {
        if (screenWidth <= item.width && !breakpoint) {
          breakpoint = item;
        }
      });
    }

    return breakpoint.viewport;
  };

  this.getImageSrc = function (baseSrc) {
    var zoom = this.getZoomFactor().zoom;
    var screenWidth = Math.round(document.documentElement.clientWidth * zoom);
    var pixelRatio = this.getPixelRatio();
    var breakpoint;
    var commands;
    var retinaWidth;

    if (screenWidth <= this.minBreakpoint.width) {
      breakpoint = this.minBreakpoint;
    } else if (screenWidth >= this.maxBreakpoint.width) {
      breakpoint = this.maxBreakpoint;
    } else {
      this.breakpoints.forEach(function (item) {
        if (screenWidth <= item.width && !breakpoint) {
          breakpoint = item;
        }
      });
    }

    commands = breakpoint.isCommands;

    if (pixelRatio > 1) {
      retinaWidth = commands.match(/wid=([0-9]*)/) ? parseInt(commands.match(/wid=([0-9]*)/).pop()) * 2 : 320;
      commands = commands.replace(/wid=[0-9]*/, 'wid=' + retinaWidth);
    }
    baseSrc = decodeURI(baseSrc);

    return encodeURI(baseSrc + (baseSrc.indexOf('?') > 0 ? '&' : '?') + commands);
  };

  this.getLowResImageSrc = function(baseSrc, viewport) {
    return baseSrc + '&cropPathE=' + viewport + this.imgPreLoadSize[viewport];
  };

  this.loadLowResImage = function () {
    var originalImageWidth = elem.clientWidth;
    var viewport = this.getPlaceholderViewport();
    var size = this.placeholderSizes[viewport];
    var ratio = size.width / size.height;

    if (originalImageWidth) {
      elem.style.height = originalImageWidth / ratio + 'px';
    }
    elem.setAttribute('data-loaded', true);
    elem.src = this.getLowResImageSrc(this.baseSrc, viewport);
  };

  this.setupImageObserver = function () {
    var config = { attributes: true, childList: false, characterData: false };
    var startTime = performance.now();
    this.observer = new MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        if (mutation.attributeName === 'data-loaded') {
          elem.setAttribute('data-load-time', performance.now() - startTime);
        }
      });
    });
    this.observer.observe(elem, config);
  };

  this.init();
}

(function (w) {
  w.ImageLoader = ImageLoader;
})(window);
