(function (w) {
  function minBy(array, iteratee) {
    var result;
    var index = -1;
    var length = array ? array.length : 0;
    var computed, value, current;

    while (++index < length) {
      value = array[index];
      current = iteratee(value);

      if (current != null && (computed === undefined
            ? (current === current)
            : (current < computed)
          )) {
        computed = current;
        result = value;
      }
    }
    return result;
  }

  function maxBy(array, iteratee) {
    var result;
    var index = -1;
    var length = array ? array.length : 0;
    var computed, value, current;

    while (++index < length) {
      value = array[index];
      current = iteratee(value);

      if (current != null && (computed === undefined
            ? (current === current)
            : (current > computed)
          )) {
        computed = current;
        result = value;
      }
    }
    return result;
  }

  function ResponsiveImagePlaceholder(elem, inserted) {
    this.init = function () {
      if (!elem) {
        return console.warn('Element is required to initialize ResponsiveImagePlaceholder');
      }

      ResponsiveImagePlaceholder.promises = ResponsiveImagePlaceholder.promises || [];
      this.baseSrc = elem.getAttribute('data-src');

      if (!this.baseSrc) {
        return console.warn('data-src attribute is required to initialize ResponsiveImagePlaceholder');
      }

      this.breakpoints = this.parseBreakpoints(elem.getAttribute('data-breakpoints'));

      this.minBreakpoint = minBy(this.breakpoints, function (item) {
        return item.width;
      });

      this.maxBreakpoint = maxBy(this.breakpoints, function (item) {
        return item.width;
      });

      this.fetchImageProps().then(function (data) {
        if (!data) {
          return;
        }
        this.onPropsRecieved.call(this, data);
        if (inserted) {
          this.dispatchInsertEvent();
        }
      }.bind(this));
    };

    this.getZoomFactor = function () {
      var clientWidth = document.documentElement.clientWidth;
      var clientHeight = document.documentElement.clientHeight;
      var zoomFactor = {
        orientation: 'landscape',
        zoom: 1,
      };

      if (typeof window.orientation !== 'undefined') {
        zoomFactor.orientation = ((screen.width/screen.height) > 1) ? 'landscape' : 'portrait';
        zoomFactor.zoom = clientWidth / window.innerWidth;
      } else {
        zoomFactor.orientation = clientWidth > clientHeight ? 'landscape' : 'portrait';
      }

      return zoomFactor;
    };

    this.getPixelRatio = function () {
      var retVal = 1;

      if (window.devicePixelRatio) {
        retVal = window.devicePixelRatio;
      } else if (screen.deviceXDPI) {
        retVal = window.screen.deviceXDPI / window.screen.logicalXDPI;
      } else if ('matchMedia' in window && window.matchMedia) {
        if (window.matchMedia('(min-resolution: 2dppx)').matches ||
            window.matchMedia('(min-resolution: 192dpi)').matches) {
          retVal = 2;
        } else if (window.matchMedia('(min-resolution: 1.5dppx)').matches ||
            window.matchMedia('(min-resolution: 144dpi)').matches) {
          retVal = 1.5;
        }
      }

      return retVal;
    };

    this.parseBreakpoints = function (breakpointsString) {
      var breakpointsArray = !!breakpointsString ? breakpointsString.split('|') : [];

      return breakpointsArray.map(function (item) {
        var data = item.split(':');
        var width = data[0] - 0;
        var isCommands = data[1] || '';

        return { width: width, isCommands: isCommands };
      });
    };

    this.getSizeFromPropsXML = function (propsXML) {
      var width = propsXML.querySelector('property[name="width"]').getAttribute('value');
      var height = propsXML.querySelector('property[name="height"]').getAttribute('value');

      return { width: width - 0, height: height - 0 };
    };

    this.onPropsRecieved = function (propsXML) {
      var size = this.getSizeFromPropsXML(propsXML);

      this.setPlaceholder(size);
    };

    this.fetchImageProps = function () {
      var imageSrc = this.getImageSrc(this.baseSrc);
      var promise = new Promise(function (resolve, reject) {
        var request = new XMLHttpRequest();
        request.open('GET', imageSrc + '&req=props,xml', true);

        request.onload = function () {
          if (request.responseXML !== null) {
            resolve(request.responseXML);
          } else {
            resolve(null);
          }
        };

        request.onerror = function () {
          resolve(null);
        };

        request.send();
      });

      ResponsiveImagePlaceholder.promises.push(promise);

      return promise;
    };

    this.getImageSrc = function (baseSrc) {
      var zoom = this.getZoomFactor().zoom;
      var screenWidth = Math.round(document.documentElement.clientWidth * zoom);
      var breakpoint;
      var commands;

      if (screenWidth <= this.minBreakpoint.width) {
        breakpoint = this.minBreakpoint;
      } else if (screenWidth >= this.maxBreakpoint.width) {
        breakpoint = this.maxBreakpoint;
      } else {
        this.breakpoints.forEach(function (item) {
          if (screenWidth <= item.width && !breakpoint) {
            breakpoint = item;
          }
        });
      }

      commands = breakpoint.isCommands;
      baseSrc = decodeURI(baseSrc);

      return encodeURI(baseSrc + (baseSrc.indexOf('?') > 0 ? '&' : '?') + commands);
    };

    this.setPlaceholder = function (size) {
      var placeholder = document.createElement('div');
      var originalImageWidth = elem.clientWidth;
      var ratio = size.width / size.height;

      if (originalImageWidth) { // image is visible
        placeholder.style.width = originalImageWidth + 'px';
        placeholder.style.height = (originalImageWidth / ratio) + 'px';
      }

      placeholder.style.maxWidth = '100%';
      placeholder.classList.add('responsive-image__placeholder');

      elem.classList.add('not-loaded');

      !!elem.parentNode && elem.parentNode.appendChild(placeholder);
    };

    this.dispatchInsertEvent = function () {
      var eventPayload = { detail: elem };
      var event;

      if (typeof w.CustomEvent === 'function') {
        event = new CustomEvent('uiInsertedImage', eventPayload);
      } else {
        event = document.createEvent('CustomEvent');
        event.initCustomEvent('uiInsertedImage', true, true, eventPayload);
      }

      elem.dispatchEvent(event);
    };

    this.init();
  }

  w.ResponsiveImagePlaceholder = ResponsiveImagePlaceholder;
})(window);
