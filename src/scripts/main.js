import 'babel-polyfill';
import 'intersection-observer';
import svgUseIt from 'svg-use-it';
import { compose, registry, advice, debug } from 'flight';
import Globals from './globals/globals';
import Components from './components/components';

debug.enable(false);
compose.mixin(registry, [advice.withAdvice]);

document.addEventListener('DOMContentLoaded', () => {
  Globals.attachTo(window.document.body);
  Components.attachTo(window.document.body);
  svgUseIt();
});
