import withAccessbility from '../withAccessibility';

describeMixin(withAccessbility, function () {
  beforeEach(function () {
    this.setupComponent('<div class="js-component" aria-expanded="false"></div>');
  });
  describe('Mixin Tests', function () {
    describe('withAccessbility Mixin', function () {
      it('can toggle aria state to true', function () {
        this.component.toggleAria(this.component.$node, { aria: 'expanded' });
        expect($(this.component.$node).attr('aria-expanded')).toEqual('true');
      });
      it('can toggle aria state to false', function () {
        this.component.toggleAria(this.component.$node, { aria: 'expanded' });
        this.component.toggleAria(this.component.$node, { aria: 'expanded' });
        expect($(this.component.$node).attr('aria-expanded')).toEqual('false');
      });
      it('can set aria attribute to initial state', function () {
        $(this.component.$node).attr('aria-disabled', true);
        this.component.clearAria(this.component.$node, { aria: 'disabled' });
        expect($(this.component.$node).attr('aria-disabled')).toEqual('false');
      });
    });
  });
});
