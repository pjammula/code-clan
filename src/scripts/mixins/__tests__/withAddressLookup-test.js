import withAddressLookup from '../withAddressLookup';
jasmine
  .getFixtures()
  .fixturesPath = '/base/src/scripts/mixins/__tests__/fixtures/';

describeMixin(withAddressLookup, function () {
  const fixture = readFixtures('WITH-ADDRESS-LOOKUP.html');

  beforeEach(function () {
    this.setupComponent(fixture);
  });

  describe('Mixin Tests', function () {
    describe('withAddressLookup Mixin', function () {
      describe('On QAS Error', function () {
        beforeEach(function () {
          // setup spies
          this.hideAddressLookupSpy = spyOn(this.component, 'hideAddressLookup');
          this.showAddressFieldsSpy = spyOn(this.component, 'showAddressFields');
          this.unbindLookupTriggerSpy = spyOn(this.component, 'unbindLookupTrigger');
          // test setup
          this.address = {
            events: this.component.select('addressLookupInput'),
          };
          this.component.bindErrorHandlers(this.address);
          this.address.events.trigger('request-error');
        });

        it('calls hideAddressLookup function', function () {
          expect(this.hideAddressLookupSpy).toHaveBeenCalled();
        });

        it('calls showAddressFields function', function () {
          expect(this.showAddressFieldsSpy).toHaveBeenCalledWith(true);
        });

        it('calls unbindLookupTrigger function', function () {
          expect(this.unbindLookupTriggerSpy).toHaveBeenCalled();
        });
      });

      describe('on initialize', function() {
        it('calls bindAddressLookup function', function () {
          window.ContactDataServices = {
            address (options) {

            }
          };

          spyOn(this.component, 'bindErrorHandlers');
          spyOn(this.component, 'bindPreSearchHandlers');
          spyOn(this.component, 'bindSearchHandlers');
          spyOn(this.component, 'bindPostSearchHandlers');

          this.component.bindAddressLookup();

          expect(this.component.select('addressLookupInput').data('initialised')).toBe(true);
        });
      })
    });
  });
});
