import withClickOutside from '../withClickOutside';

describeMixin(withClickOutside, function () {
  beforeEach(function () {
    this.setupComponent('<div class="js-component"></div>');
    this.component.$node.wrap('<div class="js-wrapper-div"></div>');
  });
  afterEach(function () {
    $('.js-wrapper-div').remove();
  });
  describe('Mixin Tests', function () {
    describe('withClickOutside Mixin', function () {
      beforeEach(function () {
        this.component.callbackFunction = function () {
          console.log('firing');
        };
      });
      it('detects when an event has fired outside of the component ' +
         'and executes a callback function', function () {
        spyOn(this.component, 'callbackFunction');
        this.component.handleClickOutside(this.component.$node, this.component.callbackFunction);
        $('.js-wrapper-div').trigger('click');
        expect(this.component.callbackFunction).toHaveBeenCalled();
      });
      it('does not fire if the event fires inside the component', function () {
        spyOn(this.component, 'callbackFunction');
        this.component.handleClickOutside(this.component.$node, this.component.callbackFunction);
        $('.js-component').trigger('click');
        expect(this.component.callbackFunction).not.toHaveBeenCalled();
      });
    });
  });
});
