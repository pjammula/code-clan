import withDataLayer from '../withDataLayer';
describeMixin(withDataLayer, function () {
  beforeEach(function () {
    this.setupComponent();
  });
  describe('Mixin Tests', function () {
    describe('withDataLayer Mixin', function () {
      let mockData;
      beforeEach(function () {
        mockData = {
          PageInfo: {
            PageType: 'product',
            PagePath: 'http://clientsite.com/cart',
          },
        };
      });
      it('add data to the dataLayer', function () {
        this.component.pushToDataLayer(mockData);
        expect(window.dataLayer).toEqual(mockData);
      });
    });
  });
});
