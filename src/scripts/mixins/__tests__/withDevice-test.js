import withDevice from '../withDevice';

describeMixin(withDevice, function () {
  beforeEach(function () {
    this.setupComponent();
  });
  describe('Mixin Tests', function () {
    describe('withDevice Mixin', function () {
      it('returns vendor attributes', function () {
        expect(this.component.getVendor()).toEqual({
          dom: 'WebKit',
          lowercase: 'webkit',
          css: '-webkit-',
          js: 'Webkit',
        });
      });
      it('returns the viewport size', function () {
        spyOn(this.component, 'getViewport').and.returnValue('sm');
        expect(this.component.getViewport()).toEqual('sm');
      });

      it('returns the current zoom level', function () {
        expect(this.component.getZoom()).toEqual(jasmine.any(Number));
      });

      it('returns the result of retina screen detection', function () {
        expect(this.component.isRetina()).toEqual(jasmine.any(Boolean));
      });
    });
  });
});
