import withDevice from '../withForm';

describeMixin(withDevice, function () {
  beforeEach(function () {
    this.setupComponent('<form class="js-component" action="/api/url" method="GET">' +
        '<input type="text" id="input" name="input" value="Hello">Hello</input></form>');
  });
  describe('Mixin Tests', function () {
    describe('withForm Mixin', function () {
      it('returns serialized form', function () {
        expect(this.component.serializeForm(this.component.$node)).toEqual({ 'input': 'Hello' });
      });

      it('returns form action', function () {
        expect(this.component.getFormAction(this.component.$node)).toEqual('/api/url');
      });

      it('returns form method', function () {
        expect(this.component.getFormMethod(this.component.$node)).toEqual('GET');
      });
    });
  });
});
