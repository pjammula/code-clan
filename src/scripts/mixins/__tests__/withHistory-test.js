import withHistory from '../withHistory';
describeMixin(withHistory, function () {
  beforeEach(function () {
    this.setupComponent();
  });
  describe('Mixin Tests', function () {
    describe('withHistory Mixin', function () {
      let mockWindow;
      beforeEach(function () {
        mockWindow = {
          location: {
            href: 'http://clientsite.com/pdp',
          },
        };
      });
      it('changes the url in the current window, redirecting the user', function () {
        const cartPageUrl = 'http://clientsite.com/cart';
        expect(mockWindow.location.href).toEqual('http://clientsite.com/pdp');
        this.component.changeWindowLocation(mockWindow, cartPageUrl);
        expect(mockWindow.location.href).toEqual(cartPageUrl);
      });
    });
  });
});
