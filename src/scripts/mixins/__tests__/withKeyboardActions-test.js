import withKeyboardActions from '../withKeyboardActions';

describeMixin(withKeyboardActions, function () {
  beforeEach(function () {
    this.setupComponent();
  });
  describe('Mixin Tests', function () {
    describe('withKeyboardActions Mixin', function () {

      describe('isEnterKey', function () {
        it('returns true if event keyCode is 13', function () {
          const event = { type: 'keydown', keyCode: 13, originalEvent: { key: 'Enter' } };

          expect(this.component.isEnterKey(event)).toEqual(true);
        });

        it('returns true if event key is Enter', function () {
          const event = { type: 'keydown', key: 'Enter' };

          expect(this.component.isEnterKey(event)).toEqual(true);
        });

        it('returns true if original event key is Enter', function () {
          const event = { type: 'keydown', originalEvent: { key: 'Enter' } };

          expect(this.component.isEnterKey(event)).toEqual(true);
        });

        it('returns true if original event keyCode is 13', function () {
          const event = { type: 'keydown', originalEvent: { keyCode: 13 } };

          expect(this.component.isEnterKey(event)).toEqual(true);
        });

        it('returns false if event keyCode is not 13', function () {
          const event = { type: 'keydown', keyCode: 27, originalEvent: {} };

          expect(this.component.isEnterKey(event)).toEqual(false);
        });
      });

      describe('isTabKey', function () {
        it('returns true is event keyCode is 9', function () {
          const event = { type: 'keydown', keyCode: 9 };

          expect(this.component.isTabKey(event)).toEqual(true);
        });

        it('returns true is event key is Tab', function () {
          const event = { type: 'keydown', key: 'Tab' };

          expect(this.component.isTabKey(event)).toEqual(true);
        });

        it('returns true is original event key is Tab', function () {
          const event = { type: 'keydown', originalEvent: { key: 'Tab' } };

          expect(this.component.isTabKey(event)).toEqual(true);
        });

        it('returns true is original event keyCode is 9', function () {
          const event = { type: 'keydown', originalEvent: { keyCode: 9 } };

          expect(this.component.isTabKey(event)).toEqual(true);
        });

        it('returns false is event keyCode is not 9', function () {
          const event = { type: 'keydown', keyCode: 27, originalEvent: {} };

          expect(this.component.isTabKey(event)).toEqual(false);
        });
      });

      describe('isEscapeKey', function () {
        it('returns true if event keyCode is 27', function () {
          const event = { type: 'keydown', keyCode: 27, originalEvent: { key: 'Escape' } };

          expect(this.component.isEscapeKey(event)).toEqual(true);
        });

        it('returns true if event key is Escape', function () {
          const event = { type: 'keydown', key: 'Escape' };

          expect(this.component.isEscapeKey(event)).toEqual(true);
        });

        it('returns true if original event key is Escape', function () {
          const event = { type: 'keydown', originalEvent: { key: 'Escape' } };

          expect(this.component.isEscapeKey(event)).toEqual(true);
        });

        it('returns true if original event keyCode is 27', function () {
          const event = { type: 'keydown', originalEvent: { keyCode: 27 } };

          expect(this.component.isEscapeKey(event)).toEqual(true);
        });

        it('returns false if event keyCode is not 27', function () {
          const event = { type: 'keydown', keyCode: 9 };

          expect(this.component.isEscapeKey(event)).toEqual(false);
        });
      });

    });
  });
});
