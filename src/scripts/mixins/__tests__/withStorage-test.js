import withStorage from '../withStorage';

describeMixin(withStorage, function () {
  beforeEach(function () {
    this.setupComponent();
  });
  describe('Mixin Tests', function () {
    describe('withStorage Mixin', function () {
      describe('Interacts with Local Storage', function () {
        afterEach(function () {
          window.localStorage.clear();
        });
        it('returns the value if there is an item with a key in local storage', function () {
          window.localStorage.setItem('testKey', JSON.stringify('testValue'));
          expect(this.component.readLocalStorage('testKey')).toEqual('testValue');
        });
        it('can write key value pairs to local storage', function () {
          expect(window.localStorage.length).toEqual(0);
          this.component.writeLocalStorage('testKey', 'testValue');
          expect(window.localStorage.length).toEqual(1);
        });
        it('can remove items from local storage', function () {
          this.component.writeLocalStorage('testKey', 'testValue');
          expect(window.localStorage.length).toEqual(1);
          this.component.removeLocalStorage('testKey');
          expect(window.localStorage.length).toEqual(0);
        });
        it('can clear local storage', function () {
          this.component.writeLocalStorage('testKey', 'testValue');
          this.component.writeLocalStorage('testKey2', 'testValue2');
          expect(window.localStorage.length).toEqual(2);
          this.component.clearLocalStorage();
          expect(window.localStorage.length).toEqual(0);
        });
      });
      describe('Interacts with Session Storage', function () {
        afterEach(function () {
          window.sessionStorage.clear();
        });
        it('returns undefined if session storage is empty', function () {
          expect(this.component.readSessionStorage()).toBeUndefined();
        });
        it('returns the value if there is an item with a key in session storage', function () {
          window.sessionStorage.setItem('testKey', JSON.stringify('testValue'));
          expect(this.component.readSessionStorage('testKey')).toEqual('testValue');
        });
        it('can write key value pairs to session storage', function () {
          expect(window.sessionStorage.length).toEqual(0);
          this.component.writeSessionStorage('testKey', 'testValue');
          expect(window.sessionStorage.length).toEqual(1);
        });
        it('can remove items from session storage', function () {
          this.component.writeSessionStorage('testKey', 'testValue');
          expect(window.sessionStorage.length).toEqual(1);
          this.component.removeSessionStorage('testKey');
          expect(window.sessionStorage.length).toEqual(0);
        });
        it('can clear session storage', function () {
          this.component.writeSessionStorage('testKey', 'testValue');
          this.component.writeSessionStorage('testKey2', 'testValue2');
          expect(window.sessionStorage.length).toEqual(2);
          this.component.clearSessionStorage();
          expect(window.sessionStorage.length).toEqual(0);
        });
      });
      describe('Interacts with cookies', function () {
        afterEach(function () {
          this.component.clearCookie();
        });
        it('can set a cookie with Infinity as date', function () {
          expect(this.component.getCookie('testCookie')).toBeUndefined();
          let testCookieKey = 'testCookie';
          let testCookieValue = 'cookieTest';
          let testCookieOptions = {
            expires: Infinity,
          };
          this.component.setCookie(testCookieKey, testCookieValue, testCookieOptions);
          expect(document.cookie).toEqual('testCookie=%22cookieTest%22');
        });
        it('can set a cookie with a number as date', function () {
          let testCookieKey = 'testCookie';
          let testCookieValue = 'cookieTest';
          let testCookieOptions = {
            expires: 31052016,
          };
          this.component.setCookie(testCookieKey, testCookieValue, testCookieOptions);
          expect(document.cookie).toEqual('testCookie=%22cookieTest%22');
        });
        it('can set a cookie with a JS Date Object as date', function () {
          let testCookieKey = 'testCookie';
          let testCookieValue = 'cookieTest';
          let testCookieOptions = {
            expires: Date.now(),
          };
          this.component.setCookie(testCookieKey, testCookieValue, testCookieOptions);
          expect(document.cookie).toEqual('testCookie=%22cookieTest%22');
        });
        it('returns the value of a cookie if the cookie exists', function () {
          let testCookieKey = 'testCookie';
          let testCookieValue = 'cookieTest';
          let testCookieOptions = {
            expires: Infinity,
          };
          this.component.setCookie(testCookieKey, testCookieValue, testCookieOptions);
          expect(this.component.getCookie('testCookie')).toEqual(testCookieValue);
        });
        it('returns undefined if the cookie does not exist', function () {
          expect(this.component.getCookie('testCookie')).toBeUndefined();
        });
        it('can remove a cookie', function () {
          let testCookieKey = 'testCookie';
          let testCookieValue = 'cookieTest';
          let testCookieOptions = {
            expires: Infinity,
          };
          this.component.setCookie(testCookieKey, testCookieValue, testCookieOptions);
          expect(this.component.removeCookie(testCookieKey));
        });
        it('can clear cookies', function () {
          let testCookieKey = 'testCookie';
          let testCookieValue = 'cookieTest';
          let testCookieOptions = {
            expires: Infinity,
          };
          let testCookieKey1 = 'testCookie1';
          let testCookieValue1 = 'cookieTest1';
          let testCookieOptions1 = {
            expires: Infinity,
          };
          this.component.setCookie(testCookieKey, testCookieValue, testCookieOptions);
          this.component.setCookie(testCookieKey1, testCookieValue1, testCookieOptions1);
          this.component.clearCookie();
          expect(document.cookie).toEqual('');
        });
      });
    });
  });
});
