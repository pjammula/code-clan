import withTrackedAnimation from '../withTrackedAnimation';
import $ from 'jquery';

describeMixin(withTrackedAnimation, function () {
  beforeEach(function () {
    this.setupComponent('<div class="js-component"></div>');
    this.component.$node.wrap('<div class="js-wrapper-div"></div>');
  });
  afterEach(function () {
    $('.js-wrapper-div').remove();
  });
  describe('Mixin Tests', function () {
    describe('withTrackedAnimation Mixin', function () {
      it('returns a boolean for whether the component is in range', function () {
        const percent = 10;
        const otherPercent = 10;
        expect(this.component.isInRange(percent, otherPercent)).toBe(true);
      });
      it('returns a boolean for whether animations are on', function () {
        const animationState = 'animationOn';
        expect(this.component.isAnimationOn(animationState)).toBe(true);
      });
      it('adds the passed in class to the passed in element', function () {
        const cssClass = 'testCSSClass';
        const element = document.createElement('div');
        this.component.startAnimation(element, cssClass);
        expect(element).toHaveClass('testCSSClass');
      });
    });
  });
});
