import $ from 'jquery';

export default function withAccessibility() {
  this.attributes({
    aria: '',
  });

  /**
   * Toggles aria attribute of element
   * @param  {object} options - object containing aria attribute
   * @return {void}
   */
  this.toggleAria = function (element, options) {
    const o = $.extend(this.attributes, options);
    const currentState = $(element).attr('aria-' + o.aria);
    let newState;

    if (currentState === 'false') {
      newState = 'true';
    } else {
      newState = 'false';
    }

    $(element).attr('aria-' + o.aria, newState);
  };

  /**
   * Initializes aria attribute of element
   * @param  {object} options - object containing aria attribute
   * @return {void}
   */
  this.clearAria = function (element, options) {
    const o = $.extend(this.attributes, options);
    $(element).attr('aria-' + o.aria, false);
  };

  /**
   * Toggles tabindex attribure of element
   * @param  {HTMLElement} element - jQuery wrapped HTML element
   */
  this.toggleTabIndex = function (element) {
    const currentIndex = element.attr('tabindex');
    element.attr('tabindex', currentIndex == 0 ? -1 : 0);
  };

  /**
   * Removes tabindex attribure of element
   * @param  {HTMLElement} element - jQuery wrapped HTML element
   */
  this.removeTabIndex = function (element) {
    const currentIndex = element.attr('tabindex');
    currentIndex == 0 ? -1 : element.removeAttr('tabindex');
  };
}
