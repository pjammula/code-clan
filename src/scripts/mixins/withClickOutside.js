import $ from 'jquery';
import SUPPORT_PASSIVE from '../globals/utility/support-passive';

export default function withClickOutside() {
  /**
   * Mixin provides outside click detection and executes callback function
   * @param  {string} elm - class name of element for which outside click detection is required
   * @param  {function} func - callback function to execute when outside click is detected
   * @return {void}
   */
  this.handleClickOutside = function (elm, func) {
    document.addEventListener('click', function (event) {
      if ($(event.target).closest(elm).length > 0) {
        return;
      }
      func.call(this);
    }.bind(this), true);
    document.addEventListener('touchstart', function (event) {
      if ($(event.target).closest(elm).length > 0) {
        return;
      }
      func.call(this);
    }.bind(this), SUPPORT_PASSIVE ? { capture: true, passive: true } : true);
  };
}
