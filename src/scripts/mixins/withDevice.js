import $ from 'jquery';

export default function withDevice() {
  this.attributes({

  });

  /**
   * Returns result of device viewport check
   * @return {string} 'xs' - mobile,
   *                  'sm' - tablet,
   *                  'md' - small desktop,
   *                  'lg' - large desktop,
   *                  'xl' - extra-large desktop
   */
  this.getViewport = function () {
    const envs = ['xs', 'sm', 'md', 'lg', 'xl'];
    const $el = $('<div>');
    let i;
    let env;
    $el.appendTo($('body'));
    for (i = envs.length - 1; i >= 0; i--) {
      env = envs[i];
      $el.addClass('hidden-' + env + '-up');
      if ($el.is(':hidden')) {
        $el.remove();
        return env;
      }
    }
  };

  this.isViewport = function (viewport) {
    return viewport === this.getViewport();
  };

  this.isDesktopAndAbove = function () {
    return this.isViewport('lg') || this.isViewport('xl');
  };

  this.isTabletAndBelow = function () {
    return this.isViewport('xs') || this.isViewport('sm') || this.isViewport('md');
  };

  this.isMobile = function () {
    return this.isViewport('xs') || this.isViewport('sm');
  };

  /**
   * Returns browser vendor details
   * @return {object} browser vendor
   */
  this.getVendor = function () {
    const styles = window.getComputedStyle(document.documentElement, '');
    const pre = (Array.prototype.slice.call(styles).join('')
          .match(/-(moz|webkit|ms)-/) || (styles.OLink === '' && ['', 'o']))[1];
    const dom = ('WebKit|Moz|MS|O').match(new RegExp('(' + pre + ')', 'i'))[1];

    return {
      dom,
      lowercase: pre,
      css: '-' + pre + '-',
      js: pre[0].toUpperCase() + pre.substr(1),
    };
  };

  /**
   * Returns result of current screen zoom level
   * @return {integer} screen zoom
   */
  this.getZoom = function () {
    return window.devicePixelRatio || (window.screen.deviceXDPI / window.screen.logicalXDPI) || 1;
  };

  /**
   *  Returns the correct animation end event for the browser.
   * @returns {string} end event
   */
  this.getAnimationEndEvent = function () {
    let t;
    const el = document.createElement('fakeelement');

    const animations = {
      animation: 'animationend',
      OAnimation: 'oAnimationEnd',
      MozAnimation: 'animationend',
      WebkitAnimation: 'webkitAnimationEnd',
    };

    for (t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  };

  /**
   *  Returns the correct transition end event for the browser.
   * @returns {string} end event
   */
  this.getTransitionEndEvent = function () {
    let t;
    const el = document.createElement('fakeelement');

    const animations = {
      transition: 'transitionend',
      OTransition: 'oTransitionEnd',
      MozTransition: 'transitionend',
      WebkitTransition: 'webkitTransitionEnd',
    };

    for (t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  };

  /**
   *  Returns the correct animation start event for the browser.
   * @returns {string} end event
   */
  this.getAnimationStartEvent = function () {
    let t;
    const el = document.createElement('fakeelement');

    const animations = {
      animation: 'animationstart',
      OAnimation: 'oAnimationStart',
      MozAnimation: 'animationstart',
      WebkitAnimation: 'webkitAnimationStart',
    };

    for (t in animations) {
      if (el.style[t] !== undefined) {
        return animations[t];
      }
    }
  };

  /**
   * Returns result of retina screen detection
   * @return {boolean} true - retina / false - non-retina
   */
  this.isRetina = function () {
    const dpr = window.devicePixelRatio ||
              (window.screen.deviceXDPI / window.screen.logicalXDPI) || 1;

    if (dpr > 1) {
      return true;
    }
    return false;
  };
}
