import $ from 'jquery';

export default function withForm() {
  this.attributes({
  });

  this.getFormAction = function (form) {
    return form.attr('action');
  };

  this.getFormMethod = function (form) {
    return form.attr('method');
  };

  this.serializeForm = function (form) {
    const a = form.serializeArray();
    const o = {};
    $.each(a, function () {
      if (o[this.name]) {
        if (!o[this.name].push) {
          o[this.name] = [o[this.name]];
        }
        o[this.name].push(this.value || '');
      } else {
        o[this.name] = this.value || '';
      }
    });
    return o;
  };

  this.selectNextField = function (e, data) {
    const fields = this.select('autoTabable');
    const current = $(data.el);
    const currentIndex = fields.index(current);
    let nextIndex = 0;

    if (this.validateFieldCount(current) && this.validateFieldAction(currentIndex, fields.length)) {
      nextIndex = currentIndex + 1;
      fields.eq(nextIndex).focus();
    }
  };

  /**
   * Validates field action
   * @param  {[type]} index  [description]
   * @param  {[type]} length [description]
   * @return {[type]}        [description]
   */
  this.validateFieldAction = function (index, length) {
    return index + 1 < length;
  };

  /**
   * Validates count of typed characters against maxlength
   * @param  {object} element - jquery wrapped input element
   * @return {boolean} true: meets maxlength, false: does not meet maxlength
   */
  this.validateFieldCount = function (element) {
    return element.val().length === parseInt(element.attr('maxlength'), 10);
  };
}
