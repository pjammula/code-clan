export default function withHistory() {
  this.attributes({
  });

  this.changeWindowLocation = function (w = window, url) {
    w.location.href = url;
  };

  this.getURLVariables = function (window) {
    return window.location.search.substring(1).split('&');
  };

  this.replaceWindowLocation = function (w = window, url) {
    w.location.replace(url);
  };

  /**
   * Gets value of specified URL query parameter
   * @param  {string} param - URL query parameter
   * @return {string} value of query parameter
   */
  this.getUrlParameter = function (param, w = window) {
    const pageURL = w.location.search.substring(1);
    const urlVariables = pageURL.split('&');
    const varCount = urlVariables.length;

    for (let i = 0; i < varCount; i++) {
      const sParameterName = urlVariables[i].split('=');
      if (sParameterName[0] === param) {
        return decodeURIComponent(sParameterName[1]);
      }
    }
  };

  this.getUrlVars = function (w = window) {
    var vars = [], hash;
    var hashes = w.location.href.slice(w.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
    }
    return vars;
  };

  /**
   * Replaces value of specified URL query parameter
   * @param  {string} key - URL query parameter key
   * @param  {string} value - URL query parameter value
   * @return {string} updated url
   */
  this.replaceUrlParameter = function (key, value, w = window) {
    return w.location.href
        .replace(new RegExp("([?&]" + key + "(?=[=&#]|$)[^#&]*|(?=#|$))"), "&" + key + "=" + encodeURIComponent(value))
        .replace(/^([^?&]+)&/, "$1?");
  };
}
