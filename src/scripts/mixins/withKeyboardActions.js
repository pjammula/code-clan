export default function withKeyboardActions() {
  this.isEnterKey = function (e) {
    if (e.originalEvent) {
      return e.originalEvent.key === 'Enter' || e.originalEvent.keyCode === 13;
    } else {
      return e.key === 'Enter' || e.keyCode === 13;
    }
  };

  this.isTabKey = function (e) {
    if (e.originalEvent) {
      return e.originalEvent.key === 'Tab' || e.originalEvent.keyCode === 9;
    } else {
      return e.key === 'Tab' || e.keyCode === 9;
    }
  };

  this.isEscapeKey = function (e) {
    if (e.originalEvent) {
      return e.originalEvent.key === 'Escape' || e.originalEvent.keyCode === 27;
    } else {
      return e.key === 'Escape' || e.keyCode === 27;
    }
  };
}
