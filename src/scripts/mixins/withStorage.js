export default function withStorage() {
  this.attributes({
    path: '/',
  });

  /**
   * Private function for appending expiry parameter
   * @param  {object} date
   * @return {string}  expiry path with date
   */
  /* previously date.toUTCString was date.toGMTString()
    but that has been deprecated */
  function dateToExpiryString(date) {
    switch (date.constructor) {
      case Number:
        return date === Infinity ? '; expires=Fri, 31 Dec 9999 23:59:59 GMT' : '; max-age=' + date;
      case String:
        return '; expires=' + date;
      case Date:
        return '; expires=' + date.toUTCString();
      default:
    }
  }

  /**
   * Returns value of stored key in local storage
   * @param  {string} key - stored key in local storage
   * @return {string/boolean} value
   */
  this.readLocalStorage = function (key) {
    let value;
    if (window.Modernizr.localstorage) {
      value = window.localStorage.getItem(key);
      if (this.isJson(value)) {
        value = JSON.parse(value);
      }
      return value === 'undefined' ? undefined : value;
    }
  };

  /**
   * Writes key and value in local storage
   * @param  {string} key
   * @param  {string/boolean} value
   * @return {void}
   */
  this.writeLocalStorage = function (key, value) {
    if (window.Modernizr.localstorage) {
      window.localStorage.setItem(key, JSON.stringify(value));
    }
  };

  /**
   * Removes key from local storage
   * @param  {string} key
   * @return {void}
   */
  this.removeLocalStorage = function (key) {
    if (window.Modernizr.localstorage) {
      window.localStorage.removeItem(key);
    }
  };

  /**
   * Clears local storage
   * @return {void}
   */
  this.clearLocalStorage = function () {
    if (window.Modernizr.localstorage) {
      window.localStorage.clear();
    }
  };

  /**
   * Returns value of stored key in session storage
   * @param  {string} key - stored key in session storage
   * @return {string/boolean} value
   */
  this.readSessionStorage = function (key) {
    let value;
    if (window.Modernizr.sessionstorage) {
      value = JSON.parse(window.sessionStorage.getItem(key));
      return value === null ? undefined : value;
    }
  };

  /**
   * Writes key and value in session storage
   * @param  {string} key
   * @param  {string/boolean} value
   * @return {void}
   */
  this.writeSessionStorage = function (key, value) {
    if (window.Modernizr.sessionstorage) {
      window.sessionStorage.setItem(key, JSON.stringify(value));
    }
  };

  /**
   * Removes key from session storage
   * @param  {string} key
   * @return {void}
   */
  this.removeSessionStorage = function (key) {
    if (window.Modernizr.sessionstorage) {
      window.sessionStorage.removeItem(key);
    }
  };

  /**
   * Clears session storage
   * @return {void}
   */
  this.clearSessionStorage = function () {
    if (window.Modernizr.sessionstorage) {
      window.sessionStorage.clear();
    }
  };

  /* check for JSON or not */
  this.isJson = function (str) {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  /**
   * Gets cookie stored against key from browser
   * @param  {string} key
   * @return {obj} jsonefied value
   */
  this.getCookie = function (key) {
    const value = decodeURI(document.cookie.replace(new RegExp('(?:(?:^|.*;\\s*)' +
        encodeURI(key).replace(/[\-\.\+\*]/g, '\\$&') +
        '\\s*\\=\\s*((?:[^;](?!;))*[^;]?).*)|.*'), '$1')) || null;
    var json = value;
    if (this.isJson(value)) {
      json = JSON.parse(value);
    }
    return json === null ? undefined : json;
  };

  /**
   * Sets cookie in browser
   * @param {string} key name of cookie
   * @param {string/boolean} value of cookie
   * @param {obj} options for storing cookie
   */
  this.setCookie = function (key, value, options) {
    if (/^(?:expires|max\-age|path|domain|secure)$/i.test(key)) {
      throw new Error('Invalid key name');
    }
    document.cookie = encodeURI(key) + '=' + encodeURI(JSON.stringify(value)) +
      dateToExpiryString(options.expires) +
      (options.domain ? '; domain=' + options.domain : '') +
      (options.path ? '; path=' + options.path : '') +
      (options.secure ? '; secure' : '');
  };

  /**
   * Removes cookie from browser
   * @param  {[type]} key name of cookie
   * @return {void}
   */
  this.removeCookie = function (key) {
    if (this.getCookie(key) !== undefined) {
      document.cookie = encodeURI(key) + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT' +
          (this.attr.path ? '; path=' + this.attr.path : '');
    }
  };

  /**
   * Clears all cookies from browser
   * @return {void}
   */
  this.clearCookie = function () {
    let i;
    let l;
    const keys = document.cookie
                       .replace(/((?:^|\s*;)[^\=]+)(?=;|$)|^\s*|\s*(?:\=[^;]*)?(?:\1|$)/g, '')
                       .split(/\s*(?:\=[^;]*)?;\s*/);

    for (i = 0, l = keys.length; i < l; i++) {
      this.removeCookie(decodeURI(keys[i]));
    }
  };
}
