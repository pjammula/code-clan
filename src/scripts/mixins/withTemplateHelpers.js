export default function withTemplateHelpers() {
  this.addHelpers = function () {
    this.templateHelpers({
      isEquals(value, test, options) {
        if (value === test) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      isMore(value, test, options) {
        if (value && value > test) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      isLess(value, test, options) {
        if (value && value < test) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      times(n, block) {
        let accum = '';
        for (let i = 0; i < n; ++i) {
          accum += block.fn(i);
        }
        return accum;
      },
      multiply(a, b) {
        return a * b;
      },
      selectOptions(selected, options) {
        return options.fn(this).replace(
          new RegExp(' value=\"' + selected + '\"'),
          '$& selected="selected"');
      },
      trim(value) {
        return value.substr(value.length - 4);
      },
      isAbsent(value, test, options) {
        if (!value && !test) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      isOdd(value, options) {
        if (value % 2) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      isEqual(value, test, options) {
        if (value && value === test) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      notEqual(value, test, options) {
        if (value && value !== test) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
      checkedIf(condition) {
        return condition === true ? ' checked' : '';
      },
      disabledIf(condition) {
        return condition === true ? ' disabled' : '';
      },
      requiredIf(condition) {
        return condition === true ? ' required' : '';
      },
      hasValue(value1, value2, options) {
        if (value1 || value2) {
          return options.fn(this);
        }
        return options.inverse(this);
      },
    });
  };
}
