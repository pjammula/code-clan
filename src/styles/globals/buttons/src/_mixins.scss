////
/// @group buttons
////

/// Button size
/// Ouputs dimensions for a specific button size
///
/// @param {String} $size
@mixin buttonsSize($size) {
  $button-dimensions: map-get($buttons-sizes, $size);
  $border-width: map-get($button-dimensions, border-width);
  $vertical-padding: map-get($button-dimensions, padding-right);
  $horizontal-padding: map-get($button-dimensions, padding-top);

  min-height: map-get($button-dimensions, min-height);
  min-width: map-get($button-dimensions, min-width);
  width: map-get($button-dimensions, width);
  padding: $horizontal-padding $vertical-padding;
  line-height: map-get($button-dimensions, line-height);
  border-width: $border-width;

  @include containerRespondTo(tablet) {
    min-width: 6/5 * map-get($button-dimensions, min-width);
    padding: $horizontal-padding + 2px $vertical-padding * 5/2;
  }
  @include containerRespondTo(desktop) {
    padding: $horizontal-padding + 4px $vertical-padding;
  }
}

/// Button text
/// Ouputs text for a specific button size
///
/// @param {String} $size
@mixin buttonsText($size, $includeType: false) {
  @if ($includeType) {
    @include typographySetFontType(button-text-type);
  }
  @include typographySetFontSize(button-text-#{$size});
}

// Properties for a state of a given theme
//
// @param {String} $state - One of normal, hover, focus, selected, disabled, pressed
@mixin _buttonsPropertiesForState($theme, $state) {
  @if _buttonsThemeHasState($theme, $state) {
    $states: map-get($buttons-themes, $theme);

    @each $property, $value in map-get($states, $state) {
      #{$property}: #{$value};
    }
  }
}
@function _buttonsPropertiesForState($theme, $state){
  @if _buttonsThemeHasState($theme, $state) {
    $states: map-get($buttons-themes, $theme);

    @each $property, $value in map-get($states, $state) {
      @return $value;
    }
  }
}

/// Button theme construct
/// Outputs styles for all button states
///
/// @param {String} $theme
@mixin buttonsTheme($theme) {
  @include _buttonsPropertiesForState($theme, normal);

  &[aria-selected=true],
  &[aria-pressed=true] {
    @include _buttonsPropertiesForState($theme, pressedselected);
  }

  &[disabled] {
    @include _buttonsPropertiesForState($theme, disabled);
  }

  &:not([disabled]) {
    &:focus:not(:hover),
    &:hover {
      @include _buttonsPropertiesForState($theme, hover);
      text-decoration: none;
    }
    &:active {
      @include _buttonsPropertiesForState($theme, hoverafter);
    }
  }

  &:focus:not(:active):not(:hover) {
    @include _buttonsPropertiesForState($theme, focus);
  }

  &:not([disabled]):hover::before {
    @include _buttonsPropertiesForState($theme, hoverbefore);
  }

  &:not([disabled]):hover::after {
    @keyframes hover-in-#{$theme} {
      0% {
        background-color: _buttonsPropertiesForState($theme, hoverafter);
        padding-top: 0;
        width: 0;
      }
      100% {
        background-color: _buttonsPropertiesForState($theme, hoverafter);
        padding-top: spacingGetMagnitude(360);
        width: spacingGetMagnitude(360);
      }
    }
    animation:  _buttonsAnimationGetProperty('hover-in', 'direction')hover-in-#{$theme} _buttonsAnimationGetProperty('hover-in', 'timing') _buttonsAnimationGetProperty('hover-in', 'easing');
    @include _buttonsPropertiesForState($theme, hoverafter);
  }

  &:not([disabled]):hover:active::before {
    @include _buttonsPropertiesForState($theme, hoverafter);
  }

  &:active {
    @keyframes clicked-on-#{$theme} {
      0% {
      }
      50% {
        transform: scale(0.9);
        @include _buttonsPropertiesForState($theme, pressedselected);
      }
      100% {
        transform: scale(1);
        @include _buttonsPropertiesForState($theme, pressedselected);
      }
    }
    animation: clicked-on-#{$theme} _buttonsAnimationGetProperty('clicked-on', 'timing') _buttonsAnimationGetProperty('clicked-on', 'easing');
  }

  &:not(.button-no-active-state):hover:active::after {
    @include _buttonsPropertiesForState($theme, pressedselected);
  }

  &:not(.button-no-active-state):hover:focus::after {
    @include _buttonsPropertiesForState($theme, pressedselected);
  }

  &.button-no-active-state:hover:active::after {
    @include _buttonsPropertiesForState($theme, hoverbefore);
  }

  @if ($theme == "transactional") or ($theme == "linking") {

    &.active {
      &:before {
        @include _buttonsPropertiesForState($theme, pressedselected);
      }
    }

    &.completed {

      @keyframes checkmark-pop {
        0% {
          transform: translate3d(-50%, -50%, 0) rotate(45deg) scale(0.3);
        }
        90% {
          transform: translate3d(-50%, -50%, 0) rotate(45deg) scale(1.2);
        }
        100% {
          transform: translate3d(-50%, -50%, 0) rotate(45deg) scale(1);
        }
      }

      &:after {
        @if ($theme == "linking") {
           @include _buttonsPropertiesForState($theme, pressedafterblack);
        }
        @else {
           @include _buttonsPropertiesForState($theme, pressedafter);
        }
        content: '';
        display: block;
        width: spacingGetMagnitude(2);
        height: spacingGetMagnitude(4);
        top: 50%;
        left: 50%;
        border-width: 0 spacingGetMagnitude(.5) spacingGetMagnitude(.5) 0;
        transform: translate3d(-50%, -50%, 0) rotate(45deg) scale(1);
        animation: checkmark-pop _buttonsAnimationGetProperty('checkmark-pop', 'timing') _buttonsAnimationGetProperty('checkmark-pop', 'easing');
        position: absolute;
        border-radius: 0;
      }
    }
  }
}

/// Button construct
/// Basic button styling and default states for specific theme and size
///
/// @param {String} $size (medium)
/// @param {String} $theme (linking)
@mixin buttons($size: large, $theme: linking) {
  box-sizing: border-box;
  vertical-align: middle;
  outline: 0;
  border-radius: 0;
  cursor: pointer;
  -webkit-appearance: none;
  user-select: none;
  background-clip: padding-box;
  -moz-osx-font-smoothing: grayscale;
  transform: translateZ(0);
  font-weight: 500;
  font-size: 1rem;
  line-height: 1.5;
  min-height: spacingGetMagnitude(14);
  min-width: spacingGetMagnitude(25);
  border: 0 solid #ececec;
  position: relative;
  padding: spacingGetMagnitude(6) 0;
  width: spacingGetMagnitude(360);
  display: block;
  text-align: center;
  color: white;
  text-decoration: none;
  font-family: sans-serif;
  overflow: hidden;
  max-width: 100%;
  margin: 0 auto;

  &[disabled] {
    pointer-events: none;
    cursor: default;
  }

  @include buttonsText($size, true);
  @include buttonsSize($size);
  @include buttonsTheme($theme);

  &::-moz-focus-inner {
    border: 0;
    padding: 0;
  }

  &:before, &:after {
    content: '';
    position: absolute;
    z-index: -1;
  }

  &:before {
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }

  &:after {
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    border-radius: 50%;
    height: 0;
  }
}
