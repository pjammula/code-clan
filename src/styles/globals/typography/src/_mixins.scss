////
/// @group typography
////

/// Adds typography use-case
///
/// @param {string} $use-case - name of use case
/// @param {map} $use-case-map
@mixin typographyAddFont($use-case, $use-case-map) {
  $typography-use-cases: typographyAddUseCase($use-case, $use-case-map) !global;
}

/// Shorthand for setting font
///
/// @param {string} $use-case
@mixin typographySetFont($use-case) {
  @include typographySetFontType($use-case);
  @include typographySetFontSize($use-case);
  @include typographySetFontSpace($use-case);
}

/// Sets font type using specified use case
///
/// @param {string} $use-case
@mixin typographySetFontType($use-case) {
  @if (map-has-key($typography-use-cases, $use-case)) {
    $use-case-map: map-get($typography-use-cases, $use-case);
      @if (map-has-key($use-case-map, type)) {
        $ft-map: map-get($use-case-map, type);
        @include _typographyProcessFontTypeMap($ft-map);
      } @else {
        @warn "Type is not defined for font use-case '" + $use-case + "'"
      } // sass-lint:disable-line brace-style
  } @else {
    @warn "Font use-case '" + $use-case + "' is not defined in typography use-cases";
  }
}

/// Sets font size using specified use case
///
/// @param {string} $use-case
@mixin typographySetFontSize($use-case) {
  @if (map-has-key($typography-use-cases, $use-case)) {
    $use-case-map: map-get($typography-use-cases, $use-case);
      @if (map-has-key($use-case-map, layout)) {
        $fs-map: map-get($use-case-map, layout);
        @include _typographyProcessFontSizeMap($fs-map, size);
      } @else {
        @warn "Layout is not defined for font use-case '" + $use-case + "'";
      }
    } @else {
    @warn "Font use-case '" + $use-case + "' is not defined in typography use-cases";
  }
}

/// Sets font space using specified use case
///
/// @param {string} $use-case
@mixin typographySetFontSpace($use-case) {
  @if (map-has-key($typography-use-cases, $use-case)) {
    $use-case-map: map-get($typography-use-cases, $use-case);
      @if (map-has-key($use-case-map, layout)) {
        $fs-map: map-get($use-case-map, layout);
        @include _typographyProcessFontSizeMap($fs-map, space);
      } @else {
        @warn "Layout is not defined for font use-case '" + $use-case + "'";
      }
    } @else {
    @warn "Font use-case '" + $use-case + "' is not defined in typography use-cases";
  }
}

/// Offset text vertically compensates dead space by removing some margin bottom
///
/// @param {number} $offset - number of pixels to add (e.g. 3) or remove (e.g. -5)
@mixin typographyFontOffset($offset) {
  @if ($offset > 0) {
    padding-top: $offset + 0;
    margin-bottom: -$offset + 0;
  } @else if ($offset < 0) {
    margin-top: $offset + 0;
    padding-bottom: -$offset + 0;
  }
}

/// Processes font-type map
///
/// @param {map}
///
/// @access private
@mixin _typographyProcessFontTypeMap($ft-map) {
  $name: null;
  $weight: null;
  $style: null;

  @if (map-has-key($ft-map, name)) {$name: map-get($ft-map, name)};
  @if (map-has-key($ft-map, weight)) {$weight: map-get($ft-map, weight)};
  @if (map-has-key($ft-map, style)) {$name: map-get($ft-map, style)};

  @include _typographySetType($name, $weight, $style);
}

/// Processes font-size map
///
/// @param {map}
///
/// @access private
@mixin _typographyProcessFontSizeMap($fs-map, $parent, $fs-breakpoints: $grid-breakpoints) {
  @each $fs-breakpoint, $fs-font-size in $fs-map {
    @if $fs-breakpoint == null {
      @warn "Breakpoint name not defined";
    } @else {
      @if map-has-key($fs-breakpoints, $fs-breakpoint) {
        @include mediaAddAbove($fs-breakpoint) {
          @if ($parent == 'size') {
            @include _typographySetSize($fs-font-size);
          }
          @if ($parent == 'space') {
            @include _typographySetSpace($fs-font-size);
          }
        }
      } @else {
        @warn "Breakpoint name '" + $fs-breakpoint + "' is not found in global list"
      } // sass-lint:disable-line brace-style
    }
  }
}

/// Shorthand for setting font type properties
///
/// @param {string} $family
/// @param {string} $weight [normal]
/// @param {string} $style [normal]
///
/// @access private
@mixin _typographySetType($family, $weight: normal, $style: normal) {
  font-family: fontsGetFontFamilyWithFallbacks($family);
  font-weight: fontsWeight($weight);
  font-style: $style;
}

/// Shorthand for setting font size properties (size, line-height)
///
/// @param {list/number} $fs-font-size
///
/// @access private
@mixin _typographySetSize($fs-font-size) {
  @if type-of($fs-font-size) == "list" {
    $fs: nth($fs-font-size, 1) * $grid-vertical-rhythm;

    font-size: typographyGetRem($fs);
    line-height: typographyGetLineHeight($fs);
  } @else if type-of($fs-font-size) == "number" {
    $fs: $fs-font-size * $grid-vertical-rhythm;

    font-size: typographyGetRem($fs);
    line-height: typographyGetLineHeight($fs);
  } @else {
    @warn "Passed font-size must be a list or number";
  }
}

/// Shorthand for setting font space properties (bottom margin)
///
/// @param {list} $fs-font-size
///
/// @access private
@mixin _typographySetSpace($fs-font-size) {
  @if type-of($fs-font-size) == "list" and length($fs-font-size) > 1 {
    margin-bottom: typographyGetRem(nth($fs-font-size, 2) * $grid-vertical-rhythm);
  }
}


/// Shorthand for font style settings
///
/// @param {string} $family
/// @param {string} $weight [normal]
/// @param {string} $style [normal]
///
/// @access private
@mixin _typographyFont($family, $weight: normal, $style: normal) {
  font-family: fontsGetFontFamilyWithFallbacks($family);
  font-weight: fontsWeight($weight);
  font-style: $style;
}

/// DEPRECATED - Use typographySetFont with use cases instead. Will be removed
/// in near future.
/// Shorthand for font size settings
///
/// @param {Number} $sizeMobile - font size on mobile, e.g. 18
/// @param {Number} $sizeTablet - font size on tablet, e.g. 16
/// @param {Number} $sizeDesktop - font size on desktop, e.g. 14
/// @param {Number} $sizeWide - font size on wide, e.g. 12
/// @param {Number} $lineHeight [null] - line height
/// @param {Number} $offset [0] - number of pixels to offset vertically
@mixin typographyFontSize($sizeMobile, $sizeTablet, $sizeDesktop, $sizeWide,
                          $lineHeightMobile: null, $lineHeightTablet: null,
                          $lineHeightDesktop: null, $offset: 0) {
  font-size: typographyGetRem($sizeMobile + 0);
  line-height: $lineHeightMobile;
  @include containerRespondTo(tablet) {
    font-size: typographyGetRem($sizeTablet + 0);
    line-height: $lineHeightTablet;
  }
  @include containerRespondTo(desktop) {
    font-size: typographyGetRem($sizeDesktop + 0);
    line-height: $lineHeightDesktop;
  }
  @include containerRespondTo(wide) {
    font-size: typographyGetRem($sizeWide + 0);
    line-height: $lineHeightDesktop;
  }
  @include typographyFontOffset($offset);
}

/// Shorthand for fixed font size settings
///
/// @param {Number} $size - font size on mobile, tablet, dessktop, wide, e.g. 18
/// @param {Number} $lineHeight [null] - line height (unitless)
/// @param {Number} $offset [0] - number of pixels to offset vertically
@mixin typographyFixedFontSize($size, $lineHeight: null, $offset: 0) {
  font-size: typographyGetRem($size + 0);
  @if ($lineHeight) {
    line-height: $lineHeight;
  }
  @include typographyFontOffset($offset);
}

/// Links styles
///
/// @param {color} $color
/// @param {color} $hover-color
///
/// @access private
@mixin _typographyLinkStyle($color) {
  a {
    text-decoration: none;
    color: $color;
    cursor: pointer;
  }
}

// Properties for a tag of a given theme
//
// @param {String} $tag - One of heading, subheading, body
@mixin _themePropertiesForTag($theme, $tag) {
  @if _typographyThemeHasTag($theme, $tag) {
    $tags: map-get($typography-themes, $theme);

    @each $property, $value in map-get($tags, $tag) {
      #{$property}: #{$value};
    }
  }
}

/// Typography theme construct
/// Ouputs styles for all typography themes
///
/// @param {String} $theme
@mixin typographyTheme($theme) {
  @include _themePropertiesForTag($theme, body);

  h1, .h1, h2, .h2, h3, .h3 {
    @include _themePropertiesForTag($theme, heading);
  }

  h4, .h4 {
    @include _themePropertiesForTag($theme, subheading);
  }

  p {
    @include _themePropertiesForTag($theme, body);
  }

  a {
    @include _themePropertiesForTag($theme, link);

    &:hover, &:active, &:focus {
      @include _themePropertiesForTag($theme, link);
    }
  }

  .link {
    @include _themePropertiesForTag($theme, link);
    display: inline-block;
    padding-left: spacingGetMagnitude(2.5);

    @include containerRespondTo(tablet) {
      padding-left: spacingGetMagnitude(5);
    }

    &:hover, &:active, &:focus {
      @include _themePropertiesForTag($theme, link);
    }
  }

  .icon-animate {
    display: inline-flex;

    &:hover {
      text-decoration: none;

      .icon-arrow {
        animation-name: digi-slide-round-the-world;
        animation-duration: 500ms;
        animation-fill-mode: forwards;
      }
    }
  }

  .icon-background {
    @include _themePropertiesForTag($theme, icon-background);
    border-radius: 50%;
    display: inline-flex;
    justify-content: center;
    align-items: center;
    overflow: hidden;

    &-large {
      width: spacingGetMagnitude(7);
      height: spacingGetMagnitude(7);
      min-width: spacingGetMagnitude(7);
      min-height: spacingGetMagnitude(7);
      padding: 0;
      margin: 0;
      position: relative;

      @include containerRespondTo(tablet) {
        margin-top: spacingGetMagnitude(.25);
      }

      @include containerRespondTo(desktop) {
        width: spacingGetMagnitude(10);
        height: spacingGetMagnitude(10);
        min-width: spacingGetMagnitude(10);
        min-height: spacingGetMagnitude(10);
        position: static;
        margin-top: 0;
      }
    }
  }

  .icon-text {
    margin-left: spacingGetMagnitude(3);
    margin-bottom: 0;
    display: inline-flex;
    padding: 0;
    position: relative;


    @include containerRespondTo(desktop) {
      margin-top: spacingGetMagnitude(.5);
      margin-left: spacingGetMagnitude(4);
    }
  }

  .icon-arrow {
    @include _themePropertiesForTag($theme, icon-arrow);
  }
}

/// Typography theme construct per breakpoint
/// Outputs styles for all typography themes
/// Scoped to specific breakpoints
///
/// @param {String} $theme
/// @param {String} $breakpoint
@mixin typographyThemePerBreakpoint($theme, $breakpoint) {
  @include mediaAddTo($breakpoint) {
    @include _themePropertiesForTag($theme, body);

    .h1, .h2, .h3, .h4 {
      @include _themePropertiesForTag($theme, heading);
    }

    .h5, .h6 {
      @include _themePropertiesForTag($theme, subheading);
    }

    p {
      @include _themePropertiesForTag($theme, body);
    }

    a {
      @include _themePropertiesForTag($theme, link);

      &:hover, &:active, &:focus {
        @include _themePropertiesForTag($theme, link);
      }
    }

    .icon-animate {
      display: inline-flex;

      &:hover {
        text-decoration: none;

        .icon-arrow {
          animation-name: digi-slide-round-the-world;
          animation-duration: 500ms;
          animation-fill-mode: forwards;
        }
      }
    }

    .icon-arrow {
      @include _themePropertiesForTag($theme, icon-arrow);
    }
  }

  .link {
    @include mediaAddTo($breakpoint) {
      @include _themePropertiesForTag($theme, link);
      display: inline-block;
      padding-left: spacingGetMagnitude(2.5);

      &:hover, &:active, &:focus {
        @include _themePropertiesForTag($theme, link);
      }
    }

    @if ($breakpoint == md) {
      @include containerRespondTo(tablet) {
        padding-left: spacingGetMagnitude(5);
      }
    }
  }

  .icon-background {
    @include mediaAddTo($breakpoint) {
      @include _themePropertiesForTag($theme, icon-background);
      border-radius: 50%;
      display: inline-flex;
      justify-content: center;
      align-items: center;
      overflow: hidden;
    }


    &-large {
      @include mediaAddTo($breakpoint) {
        width: spacingGetMagnitude(7);
        height: spacingGetMagnitude(7);
        min-width: spacingGetMagnitude(7);
        min-height: spacingGetMagnitude(7);
        padding: 0;
        margin: 0;
        position: relative;
      }

      @if ($breakpoint == 'md') {
        @include containerRespondTo(tablet) {
          margin-top: spacingGetMagnitude(.25);
        }
      }

      @if ($breakpoint == 'lg' or $breakpoint == 'xl') {
        @include containerRespondTo(desktop) {
          width: spacingGetMagnitude(10);
          height: spacingGetMagnitude(10);
          min-width: spacingGetMagnitude(10);
          min-height: spacingGetMagnitude(10);
          position: static;
          margin-top: 0;
        }
      }

    }
  }

  .icon-text {
    @include mediaAddTo($breakpoint) {
      @include _themePropertiesForTag($theme, icon-text);
      margin-left: spacingGetMagnitude(3);
      margin-bottom: 0;
      display: inline-flex;
      padding: 0;
      position: relative;
    }

    @if ($breakpoint == 'lg' or $breakpoint == 'xl') {
      @include containerRespondTo(desktop) {
        margin-top: spacingGetMagnitude(.5);
        margin-left: spacingGetMagnitude(4);
      }
    }
  }
}
