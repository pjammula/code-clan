var path = require('path');

var ROOT_DIRECTORY = path.resolve(__dirname, '..');
var SRC_DIRECTORY = path.resolve(ROOT_DIRECTORY, 'src');
var BUILD_DIRECTORY = path.resolve(ROOT_DIRECTORY, 'build');
var DEPLOY_DIRECTORY = path.resolve(ROOT_DIRECTORY, 'dist');
var TOOLS_DIRECTORY = path.resolve(ROOT_DIRECTORY, 'tools');
/*var CERT_DIRECTORY = path.resolve(ROOT_DIRECTORY, './../../../infrastructure/', 'dev');
var AEM_ASSET_VENDOR = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-vendor');
var AEM_ASSET_TROUBLESHOOTING = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-troubleshooting');
var AEM_ASSET_COMMON = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-common');
var AEM_ASSET_BODY = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-main');
var AEM_ASSET_HEAD = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-head');
var AEM_ASSET_BASKET = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-basket');
var AEM_ASSET_SEARCH = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-search');
var AEM_ASSET_COMPARE = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-compare');
var AEM_ASSET_CHECKOUT = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-checkout');
var AEM_ASSET_FREE_TOOLS = path.resolve(ROOT_DIRECTORY, './../../../aem/custom/apps/src/main/content/jcr_root/etc/clientlibs/dyson', 'clientlib-free-tools');
var CRX_ASSET_BODY = 'http://admin:admin@localhost:4502/etc/clientlibs/dyson/clientlib-main';
var CRX_ASSET_HEAD = 'http://admin:admin@localhost:4502/etc/clientlibs/dyson/clientlib-head';*/
var DEV_SERVER = 'https://dev.code-clan.com:3333';

/*module.exports = {
  AEM_ASSET_HEAD: AEM_ASSET_HEAD,
  AEM_ASSET_VENDOR: AEM_ASSET_VENDOR,
  AEM_ASSET_TROUBLESHOOTING: AEM_ASSET_TROUBLESHOOTING,
  AEM_ASSET_COMMON: AEM_ASSET_COMMON,
  AEM_ASSET_BODY: AEM_ASSET_BODY,
  AEM_ASSET_BASKET: AEM_ASSET_BASKET,
  AEM_ASSET_SEARCH: AEM_ASSET_SEARCH,
  AEM_ASSET_COMPARE: AEM_ASSET_COMPARE,
  AEM_ASSET_CHECKOUT: AEM_ASSET_CHECKOUT,
  AEM_ASSET_FREE_TOOLS: AEM_ASSET_FREE_TOOLS,
  BUILD_DIRECTORY: BUILD_DIRECTORY,
  CRX_ASSET_HEAD: CRX_ASSET_HEAD,
  CRX_ASSET_BODY: CRX_ASSET_BODY,
  DEV_SERVER: DEV_SERVER,
  SRC_DIRECTORY: SRC_DIRECTORY,
  DEPLOY_DIRECTORY: DEPLOY_DIRECTORY,
  ROOT_DIRECTORY: ROOT_DIRECTORY,
  CERT_DIRECTORY: CERT_DIRECTORY,
  TOOLS_DIRECTORY: TOOLS_DIRECTORY
};*/

module.exports = {
  DEV_SERVER: DEV_SERVER,
  SRC_DIRECTORY: SRC_DIRECTORY,
  DEPLOY_DIRECTORY: DEPLOY_DIRECTORY,
  ROOT_DIRECTORY: ROOT_DIRECTORY,
  TOOLS_DIRECTORY: TOOLS_DIRECTORY,
  BUILD_DIRECTORY: BUILD_DIRECTORY
};
