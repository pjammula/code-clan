(function (root, factory) {
     if (typeof module !== 'undefined' && module.exports) {
        factory(root, root.jasmine, require('jquery'), require('flight'));
    } else {
        factory(root, root.jasmine, root.jQuery, root.flight);
    }
}((function() {return this; })(), function (window, jasmine, $, flight) {
  jasmine.flight = {};

  window.describeComponent = function (component, specDefinitions) {
    jasmine.getEnv().describeComponent(component, specDefinitions);
  };

  var describeComponentFactory = function (component, specDefinitions) {
    return function () {
      beforeEach(function (done) {
        this.Component = this.component = this.$node = null;

        this.setupComponent = function (fixture, options, resources) {
          if (this.component) {
            this.component.teardown();
            this.$node.remove();
          }

          if (fixture instanceof $ || typeof fixture === 'string') {
            this.$node = $(fixture).addClass('component-root');
          } else {
            this.$node = $('<div class="component-root" />');
            options = fixture;
            fixture = null;
          }

          $('body').append(this.$node);

          options = options === undefined ? {} : options;

          if (resources) {
            var componentWithResources = component.mixin(function () {
              this.before('initialize', function() {
                this.withResourcesLocalRegistry = [];
                resources.forEach(function(resource) {
                  this.provideResource(resource.name, resource.data);
                }, this);
              });
            });

            this.component = (new componentWithResources).initialize(this.$node.find(options.attachTo), options);
          } else {
            this.component = (new component()).initialize(this.$node.find(options.attachTo), options);
          }
        };

        flight.registry.reset();
        this.Component = component;
        done();
      });

      afterEach(function (done) {
        if (this.$node) {
          this.$node.remove();
          this.$node = null;
        }


        if (this.component) {
          this.component = null;
        }

        this.Component = null;
        flight.component.teardownAll();
        done();
      });

      specDefinitions.apply(this);
    };
  };

  jasmine.Env.prototype.describeComponent = function (component, specDefinitions) {
    describe(component, describeComponentFactory(component, specDefinitions));
  };

  window.describeMixin = function (mixin, specDefinitions) {
    jasmine.getEnv().describeMixin(mixin, specDefinitions);
  };

  var describeMixinFactory = function (mixin, specDefinitions) {
    return function () {
      beforeEach(function (done) {
        this.Component = this.component = this.$node = null;

        this.setupComponent = function (fixture, options) {
          if (this.component) {
            this.component.teardown();
            this.$node.remove();
          }

          if (fixture instanceof $ || typeof fixture === 'string') {
            this.$node = $(fixture).addClass('component-root');
          } else {
            this.$node = $('<div class="component-root" />');
            options = fixture;
            fixture = null;
          }

          $('body').append(this.$node);

          options = options === undefined ? {} : options;
          this.Component = flight.component(function Base() {}, mixin);
          this.component = (new this.Component()).initialize(this.$node);
        };

        flight.registry.reset();
        this.Component = flight.component(function () {}, mixin);
        done();
      });

      afterEach(function (done) {
        if (this.$node) {
          this.$node.remove();
          this.$node = null;
        }

        if (this.component) {
          this.component = null;
        }

        this.Component = null;
        flight.component.teardownAll();
        done();
      });

      specDefinitions.apply(this);
    };
  };

  jasmine.Env.prototype.describeMixin = function (mixin, specDefinitions) {
    describe(mixin, describeMixinFactory(mixin, specDefinitions));
  };

  (function (namespace) {
    var eventsData = {
      spiedEvents: {},
      handlers: []
    };

    namespace.events = {
      spyOn: function (selector, eventName) {
        eventsData.spiedEvents[[selector, eventName]] = {
          callCount: 0,
          calls: [],
          mostRecentCall: {},
          name: eventName
        };

        var handler = function (e, data) {
          var call = {
            event: e,
            args: jasmine.util.argsToArray(arguments),
            data: data
          };
          eventsData.spiedEvents[[selector, eventName]].callCount++;
          eventsData.spiedEvents[[selector, eventName]].calls.push(call);
          eventsData.spiedEvents[[selector, eventName]].mostRecentCall = call;
        };

        $(selector).on(eventName, handler);
        eventsData.handlers.push([selector, eventName, handler]);
        return eventsData.spiedEvents[[selector, eventName]];
      },

      eventArgs: function (selector, eventName, expectedArg) {
        var actualArgs = eventsData.spiedEvents[[selector, eventName]].mostRecentCall.args;

        if (!actualArgs) {
          throw 'No event spy found on ' + eventName + '. Try adding a call to spyOnEvent or make sure that the selector the event is triggered on and the selector being spied on are correct.';
        }

        // remove extra event metadata if it is not tested for
        if ((actualArgs.length === 2) && typeof actualArgs[1] === 'object' &&
          expectedArg && !expectedArg.scribeContext && !expectedArg.sourceEventData && !expectedArg.scribeData) {
          actualArgs[1] = $.extend({}, actualArgs[1]);
          delete actualArgs[1].sourceEventData;
          delete actualArgs[1].scribeContext;
          delete actualArgs[1].scribeData;
        }

        return actualArgs;
      },

      wasTriggered: function (selector, event) {
        var spiedEvent = eventsData.spiedEvents[[selector, event]];
        return spiedEvent && spiedEvent.callCount > 0;
      },

      wasTriggeredWith: function (selector, eventName, expectedArg) {
        var actualArgs = jasmine.flight.events.eventArgs(selector, eventName, expectedArg);
        return actualArgs && jasmine.matchersUtil.contains(actualArgs, expectedArg);
      },

      wasTriggeredWithData: function (selector, eventName, expectedArg) {
        var actualArgs = jasmine.flight.events.eventArgs(selector, eventName, expectedArg);
        var valid;

        if (actualArgs) {
          valid = false;
          for (var i = 0; i < actualArgs.length; i++) {
            if (jasmine.flight.validateHash(expectedArg, actualArgs[i])) {
              return true;
            }
          }
          return valid;
        }

        return false;
      },

      cleanUp: function () {
        eventsData.spiedEvents = {};
        // unbind all handlers
        for (var i = 0; i < eventsData.handlers.length; i++) {
          $(eventsData.handlers[i][0]).off(eventsData.handlers[i][1], eventsData.handlers[i][2]);
        }
        eventsData.handlers    = [];
      }
    };

    namespace.validateHash = function (a, b, intersection) {
      var validHash;
      for (var field in a) {
        if ((typeof a[field] === 'object') && (typeof b[field] === 'object')) {
          validHash = a[field] === b[field] || jasmine.flight.validateHash(a[field], b[field]);
        } else if (intersection && (typeof a[field] === 'undefined' || typeof b[field] === 'undefined')) {
          validHash = true;
        } else {
          validHash = (a[field] === b[field]);
        }
        if (!validHash) {
          break;
        }
      }
      return validHash;
    };

    namespace.assertEventTriggeredWithData = function (actual, selector, expectedArg, fuzzyMatch) {
      var eventName = typeof actual === 'string' ? actual : actual.name;
      var wasTriggered = jasmine.flight.events.wasTriggered(selector, eventName);
      var wasTriggeredWithData = false;

      if (wasTriggered) {
        if (fuzzyMatch) {
          wasTriggeredWithData = jasmine.flight.events.wasTriggeredWithData(selector, eventName, expectedArg);
        } else {
          wasTriggeredWithData = jasmine.flight.events.wasTriggeredWith(selector, eventName, expectedArg);
        }
      }

      var result = {
        pass: wasTriggeredWithData
      };

      result.message = (function () {
        var $pp = function (obj) {
          var description;
          var attr;

          if (!(obj instanceof $)) {
            obj = $(obj);
          }

          description = [
            obj.get(0).nodeName
          ];

          attr = obj.get(0).attributes || [];

          for (var x = 0; x < attr.length; x++) {
            description.push(attr[x].name + '="' + attr[x].value + '"');
          }

          return '<' + description.join(' ') + '>';
        };

        if (wasTriggered) {
          var actualArg = jasmine.flight.events.eventArgs(selector, eventName, expectedArg)[1];
          return [
            '<div class="value-mismatch">Expected event ' + eventName + ' to have been triggered on' + selector,
            '<div class="value-mismatch">Expected event ' + eventName + ' not to have been triggered on' + selector
          ];
        } else {
          return [
            'Expected event ' + eventName + ' to have been triggered on ' + $pp(selector),
            'Expected event ' + eventName + ' not to have been triggered on ' + $pp(selector)
          ];
        }
      }());

      return result;

    };
  })(jasmine.flight);

  beforeEach(function () {
    jasmine.addMatchers({
      toHaveBeenTriggeredOn: function () {
        return {
          compare: function (actual, selector) {
            var eventName = typeof actual === 'string' ? actual : actual.name;
            var wasTriggered = jasmine.flight.events.wasTriggered(selector, eventName);
            var result = {
              pass: wasTriggered
            };

            result.message = (function () {
              var $pp = function (obj) {
                var description;
                var attr;

                if (!(obj instanceof jQuery)) {
                  obj = $(obj);
                }

                description = [
                  obj.get(0).nodeName
                ];

                attr = obj.get(0).attributes || [];

                for (var x = 0; x < attr.length; x++) {
                  description.push(attr[x].name + '="' + attr[x].value + '"');
                }

                return '<' + description.join(' ') + '>';
              };

              if (wasTriggered) {
                return [
                  '<div class="value-mismatch">Expected event ' + eventName + ' to have been triggered on' + selector,
                  '<div class="value-mismatch">Expected event ' + eventName + ' not to have been triggered on' + selector
                ];
              } else {
                return [
                  'Expected event ' + eventName + ' to have been triggered on ' + $pp(selector),
                  'Expected event ' + eventName + ' not to have been triggered on ' + $pp(selector)
                ];
              }
            }());

            return result;
          }
        };
      },

      toHaveBeenTriggeredOnAndWith: function () {
        return {
          compare: function (actual, selector, expectedArg, fuzzyMatch) {
            return jasmine.flight.assertEventTriggeredWithData(actual, selector, expectedArg, fuzzyMatch);
          }
        };
      },

      toHaveBeenTriggeredOnAndWithFuzzy: function () {
        return {
          compare: function (actual, selector, expectedArg) {
            return jasmine.flight.assertEventTriggeredWithData(actual, selector, expectedArg, true);
          }
        };
      }
    });
  });

  window.spyOnEvent = function (selector, eventName) {
    jasmine.jQuery.events.spyOn(selector, eventName);
    return jasmine.flight.events.spyOn(selector, eventName);
  };

  afterEach(function () {
    jasmine.flight.events.cleanUp();
  });

}));
