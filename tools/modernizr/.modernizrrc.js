"use strict";

module.exports = {
  "minify": true,
  "options": [
    "setClasses",
    "addTest",
    "prefixes",
    "testStyles",
    "testProp",
    "domPrefixes"
  ],
  "feature-detects": [
    "test/es6/promises",
    "test/storage/localstorage",
    "test/storage/sessionstorage",
    "test/touchevents",
    "test/css/positionsticky"
  ]
};