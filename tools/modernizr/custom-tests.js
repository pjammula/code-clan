(function (Modernizr) {
  const tests = [
    { name: 'svg', value: 'url(#test)' },
    { name: 'circle', value: 'circle(60px at center)' },
  ];

  let t = 0;
  let name;
  let value;
  let prop;

  for (; t < tests.length; t++) {
    name = tests[t].name;
    value = tests[t].value;
    Modernizr.addTest('cssclippath' + name, function () {
      // Try using window.CSS.supports
      if ('CSS' in window && 'supports' in window.CSS) {
        for (let i = 0; i < Modernizr._prefixes.length; i++) {
          prop = Modernizr._prefixes[i] + 'clip-path';
          if (window.CSS.supports(prop, value)) {
            return true;
          }
        }
        return false;
      }
      // Otherwise, use Modernizr.testStyles and examine the property manually
      return Modernizr.testStyles('#modernizr { ' +
        Modernizr._prefixes.join('clip-path:' + value + '; ') + ' }', function (elem) {
        const style = getComputedStyle(elem);
        let clip = style.clipPath;

        if (!clip || clip === 'none') {
          clip = false;
          for (let i = 0; i < Modernizr._domPrefixes.length; i++) {
            const test = Modernizr._domPrefixes[i] + 'ClipPath';
            if (style[test] !== 'undefined' && style[test]) {
              clip = true;
              break;
            }
          }
        }

        return Modernizr.testProp('clipPath') && clip;
      });
    });
  }
})(window.Modernizr);
