/*eslint-disable*/
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var path = require('path');
var constants = require('../constants');

module.exports = {
  entry: {
    head: [
      './tools/modernizr/.modernizrrc.js',
      './tools/modernizr/custom-tests.js',
      './src/scripts/head/promise.js',
      './src/scripts/head/responsive_image_placeholder.js',
      './src/scripts/head/image_loader.js',
    ],
    vendor: [
      'custom-event',
      'flight-handlebars-view',
      'flight-with-child-components',
      'flight-with-resources',
      'flight-with-state',
      'flightjs',
      'handlebars',
      'intersection-observer',
      'isomorphic-fetch',
      'jquery',
      'moment',
      'morphdom',
      'psl',
      'parsleyjs',
      'svg-use-it',
      'uuid',
    ],
    common: [
      './src/scripts/globals/accordion/accordion.js',
      './src/scripts/globals/animations/scroll/scrollTo.js',
      './src/scripts/globals/fetch/fetch.js',
      './src/scripts/globals/fetch/format-endpoint.js',
      './src/scripts/mixins/withClickOutside.js',
      './src/scripts/mixins/withDevice.js',
      './src/scripts/mixins/withForm.js',
      './src/scripts/mixins/withHistory.js',
      './src/scripts/mixins/withStorage.js',
      './src/scripts/mixins/withTemplateHelpers.js',
    ],
    main: [
      './src/scripts/main.js',
      './src/styles/style-loader.js',
    ],
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(bower_components|node_modules)/,
      use: [{
        loader: 'babel-loader'
      }],
    }, {
      test: /\.png$/,
      use: [{
        loader: 'url-loader',
        options: {
          limit: 1000,
          minetype: 'image/png',
          name: '../images/sprite/[name].[ext]',
        }
      }],
    }, {
      test: /\.woff$/,
      use: [{
        loader: 'file-loader',
        options: {
          name: '../fonts/[name].[ext]',
        }
      }]
    }, {
      test: /\.scss$/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          { loader: 'css-loader', options: { autoprefixer: false, importLoaders: 2 } },
          { loader: 'postcss-loader', options: { config: { path: 'postcss.config.js' } } },
          { loader: 'sass-loader' },
        ]
      })
    }, {
      test: /\.modernizrrc\.js$/,
      use: [{loader: 'modernizr-loader' }],
    }],
  },
  output: {
    libraryTarget: 'umd',
    library: 'main',
    path: path.resolve(constants.DEPLOY_DIRECTORY, 'scripts'),
    filename: '[name].js',
  },
  resolve: {
    extensions: [
      '.js',
    ],
    alias: {
      flight: 'flightjs',
      handlebars: 'handlebars/dist/handlebars.min.js',
      globals: path.resolve(constants.SRC_DIRECTORY, 'scripts/globals'),
      components: path.resolve(constants.SRC_DIRECTORY, 'scripts/components'),
      mixins: path.resolve(constants.SRC_DIRECTORY, 'scripts/mixins'),
    },
  },
  resolveLoader: {
    modules: [
      'node_modules',
      path.resolve(constants.TOOLS_DIRECTORY, 'modernizr')
    ],
  },
  node: {
    fs: 'empty',
  },
};
