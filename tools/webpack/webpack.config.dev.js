/*eslint-disable*/
var webpack = require('webpack');
var baseConfig = require('./webpack.config.hot.js');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var WebpackShellPlugin = require('webpack-shell-plugin');
var fs = require('fs');
var path = require('path');
var constants = require('../constants')

var config = Object.create(baseConfig);
config.devtool = 'eval-source-map';
config.output.publicPath = '/dist/';
config.plugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
  }),
  new ExtractTextPlugin({
    filename: 'main.css',
    allChunks: true,
  }),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development'),
  }),
];
config.devServer = {
  port: 3102,
  inline: true,
  hot: true,
  disableHostCheck: true,
  stats: "errors-only",
  https: true,
};
config.watchOptions = {
  aggregateTimeout: 300,
  poll: 2000
};

module.exports = config;
