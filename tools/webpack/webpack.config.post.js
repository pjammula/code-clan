/*eslint-disable*/
var CopyWebpackPlugin = require('copy-webpack-plugin');
var constants = require('../constants');

module.exports = {
  entry: './src/scripts/main.js',
  output: {
    path: constants.DEPLOY_DIRECTORY + '/scripts',
    filename: 'main.js',
  },
  plugins: [
    new CopyWebpackPlugin([{
      from: constants.DEPLOY_DIRECTORY + '/scripts/head.js',
      to: 'deploy/scripts',
    }, {
      from: constants.DEPLOY_DIRECTORY + '/scripts/vendor.js',
      to: 'deploy/scripts',
    }, {
      from: constants.DEPLOY_DIRECTORY + '/scripts/common.js',
      to: 'deploy/scripts',
    }, {
      from: constants.DEPLOY_DIRECTORY + '/styles/main.css',
      to: 'deploy/styles',
    }, {
      from: constants.DEPLOY_DIRECTORY + '/icons',
      to: 'deploy/icons',
    }, {
      from: constants.DEPLOY_DIRECTORY + '/fonts',
      to: 'deploy/fonts',
    }, {
      from: constants.SRC_DIRECTORY + '/images/sprite/sprite.png',
      to: 'deploy/images/sprite',
    }]),
  ],
};
