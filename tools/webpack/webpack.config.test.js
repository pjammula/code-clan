/*eslint-disable*/
var webpack = require('webpack');
var baseConfig = require('./webpack.config.base.js');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var SpritesmithPlugin = require('webpack-spritesmith');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var ImageminPlugin = require('imagemin-webpack-plugin').default;
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var LodashModuleReplacementPlugin = require('lodash-webpack-plugin');

var config = Object.create(baseConfig);

config.devtool = false;
config.plugins = [
  new webpack.ProvidePlugin({
    $: 'jquery',
    jQuery: 'jquery',
  }),
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
  new ExtractTextPlugin({
    filename: '../styles/main.css',
    allChunks: true,
  }),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development'),
  }),
  // new SpritesmithPlugin({
  //   src: {
  //     cwd: './src/images',
  //     glob: '*.png',
  //   },
  //   target: {
  //     image: './src/images/sprite/sprite.png',
  //     css: './src/images/sprite/sprite.scss',
  //   },
  //   apiOptions: {
  //     cssImageRef: '../images/sprite/sprite.png',
  //   },
  //   spritesmithOptions: {
  //     algorithm: 'top-down',
  //   },
  // }),
  // new ImageminPlugin({
  //   svgo: null,
  //   externalImages: {
  //     sources: ['src/images/sprite/sprite.png']
  //   }
  // }),
  // new webpack.optimize.CommonsChunkPlugin({
  //   names: ['common', 'vendor', 'head'],
  //   minChunks: Infinity,
  // }),
  // new BundleAnalyzerPlugin({
  //   analyzerMode: 'static',
  //   openAnalyzer: false,
  //   reportFilename: '../../reports/bundling/js-bundle-report.html'
  // }),
  new LodashModuleReplacementPlugin({
    'shorthands': true,
    'collections': true,
    'flattening': true,
    'paths': true,
  }),
  // new UglifyJSPlugin(),
];

module.exports = config;
